// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class InterstitialAd$$ViewBinder<T extends phantlab.ntertain.app.activities.InterstitialAd> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558598, "field 'ad'");
    target.ad = finder.castView(view, 2131558598, "field 'ad'");
    view = finder.findRequiredView(source, 2131558599, "method 'onClose'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClose();
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.ad = null;
  }
}
