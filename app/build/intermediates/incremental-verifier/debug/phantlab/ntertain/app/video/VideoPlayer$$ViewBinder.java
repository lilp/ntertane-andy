// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.video;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class VideoPlayer$$ViewBinder<T extends phantlab.ntertain.app.video.VideoPlayer> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558694, "field 'player'");
    target.player = finder.castView(view, 2131558694, "field 'player'");
    view = finder.findRequiredView(source, 2131558541, "field 'play' and method 'onPlay'");
    target.play = finder.castView(view, 2131558541, "field 'play'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onPlay();
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.player = null;
    target.play = null;
  }
}
