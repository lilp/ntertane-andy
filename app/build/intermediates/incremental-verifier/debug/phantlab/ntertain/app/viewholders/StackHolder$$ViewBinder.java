// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.viewholders;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class StackHolder$$ViewBinder<T extends phantlab.ntertain.app.viewholders.StackHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558688, "field 'title'");
    target.title = finder.castView(view, 2131558688, "field 'title'");
  }

  @Override public void unbind(T target) {
    target.title = null;
  }
}
