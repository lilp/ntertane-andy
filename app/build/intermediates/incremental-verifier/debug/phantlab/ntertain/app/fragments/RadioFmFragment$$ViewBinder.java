// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RadioFmFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.RadioFmFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558672, "field 'channel'");
    target.channel = finder.castView(view, 2131558672, "field 'channel'");
    view = finder.findRequiredView(source, 2131558537, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
    view = finder.findRequiredView(source, 2131558670, "field 'panel'");
    target.panel = finder.castView(view, 2131558670, "field 'panel'");
    view = finder.findRequiredView(source, 2131558541, "field 'play' and method 'onPlay'");
    target.play = finder.castView(view, 2131558541, "field 'play'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onPlay();
        }
      });
  }

  @Override public void unbind(T target) {
    target.list = null;
    target.channel = null;
    target.thumbnail = null;
    target.panel = null;
    target.play = null;
  }
}
