// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class TopTracksActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.TopTracksActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558616, "field 'progress'");
    target.progress = finder.castView(view, 2131558616, "field 'progress'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.list = null;
    target.progress = null;
  }
}
