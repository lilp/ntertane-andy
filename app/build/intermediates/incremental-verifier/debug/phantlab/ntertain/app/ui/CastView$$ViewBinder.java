// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.ui;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CastView$$ViewBinder<T extends phantlab.ntertain.app.ui.CastView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558537, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
    view = finder.findRequiredView(source, 2131558561, "field 'mActorTextView'");
    target.mActorTextView = finder.castView(view, 2131558561, "field 'mActorTextView'");
  }

  @Override public void unbind(T target) {
    target.thumbnail = null;
    target.mActorTextView = null;
  }
}
