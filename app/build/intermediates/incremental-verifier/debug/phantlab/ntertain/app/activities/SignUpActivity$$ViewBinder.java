// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class SignUpActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.SignUpActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558534, "field 'account' and method 'onSignUpStatus'");
    target.account = finder.castView(view, 2131558534, "field 'account'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onSignUpStatus();
        }
      });
    view = finder.findRequiredView(source, 2131558684, "field 'signup' and method 'onSignUp'");
    target.signup = finder.castView(view, 2131558684, "field 'signup'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onSignUp();
        }
      });
    target.edits = Finder.listOf(
        finder.<android.widget.EditText>findRequiredView(source, 2131558620, "field 'edits'"),
        finder.<android.widget.EditText>findRequiredView(source, 2131558681, "field 'edits'"),
        finder.<android.widget.EditText>findRequiredView(source, 2131558686, "field 'edits'")
    );
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.account = null;
    target.signup = null;
    target.edits = null;
  }
}
