// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class MyMusicActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.MyMusicActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558611, "field 'pager'");
    target.pager = finder.castView(view, 2131558611, "field 'pager'");
    view = finder.findRequiredView(source, 2131558607, "field 'tabs'");
    target.tabs = finder.castView(view, 2131558607, "field 'tabs'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.pager = null;
    target.tabs = null;
  }
}
