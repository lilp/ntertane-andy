// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class SignUpMailActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.SignUpMailActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558534, "field 'account'");
    target.account = finder.castView(view, 2131558534, "field 'account'");
    view = finder.findRequiredView(source, 2131558583, "method 'mailSingup'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.mailSingup();
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.account = null;
  }
}
