// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class SignInActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.SignInActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558682, "field 'login' and method 'onLogin'");
    target.login = finder.castView(view, 2131558682, "field 'login'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onLogin();
        }
      });
    view = finder.findRequiredView(source, 2131558534, "method 'onSignUp'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onSignUp();
        }
      });
    target.edits = Finder.listOf(
        finder.<android.widget.EditText>findRequiredView(source, 2131558620, "field 'edits'"),
        finder.<android.widget.EditText>findRequiredView(source, 2131558681, "field 'edits'")
    );
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.login = null;
    target.edits = null;
  }
}
