// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PlaylistAdapter$TextHolder$$ViewBinder<T extends phantlab.ntertain.app.adapters.PlaylistAdapter.TextHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 16908310, "field 'title'");
    target.title = finder.castView(view, 16908310, "field 'title'");
  }

  @Override public void unbind(T target) {
    target.title = null;
  }
}
