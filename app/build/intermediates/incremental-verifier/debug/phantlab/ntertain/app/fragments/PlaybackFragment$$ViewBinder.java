// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PlaybackFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.PlaybackFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558553, "field 'blur'");
    target.blur = finder.castView(view, 2131558553, "field 'blur'");
    view = finder.findRequiredView(source, 2131558541, "field 'play' and method 'onPlay'");
    target.play = finder.castView(view, 2131558541, "field 'play'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onPlay();
        }
      });
    view = finder.findRequiredView(source, 2131558559, "method 'toggle'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.toggle();
        }
      });
    target.titles = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 16908310, "field 'titles'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558632, "field 'titles'")
    );
  }

  @Override public void unbind(T target) {
    target.blur = null;
    target.play = null;
    target.titles = null;
  }
}
