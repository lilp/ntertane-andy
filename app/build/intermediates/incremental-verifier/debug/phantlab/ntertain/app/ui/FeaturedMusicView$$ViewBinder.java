// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.ui;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class FeaturedMusicView$$ViewBinder<T extends phantlab.ntertain.app.ui.FeaturedMusicView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558612, "field 'image'");
    target.image = finder.castView(view, 2131558612, "field 'image'");
  }

  @Override public void unbind(T target) {
    target.image = null;
  }
}
