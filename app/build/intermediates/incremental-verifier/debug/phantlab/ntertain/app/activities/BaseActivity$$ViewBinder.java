// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class BaseActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.BaseActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558548, "field 'drawer'");
    target.drawer = finder.castView(view, 2131558548, "field 'drawer'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.drawer = null;
  }
}
