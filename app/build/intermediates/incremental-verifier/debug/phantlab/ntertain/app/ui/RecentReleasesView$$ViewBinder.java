// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.ui;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RecentReleasesView$$ViewBinder<T extends phantlab.ntertain.app.ui.RecentReleasesView> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558537, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 16908310, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 16908304, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558540, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.thumbnail = null;
    target.texts = null;
  }
}
