// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PlayerActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.PlayerActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558695, "field 'playerView'");
    target.playerView = finder.castView(view, 2131558695, "field 'playerView'");
  }

  @Override public void unbind(T target) {
    target.playerView = null;
  }
}
