// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class PlayMusicActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.PlayMusicActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558553, "field 'thumbs'");
    target.thumbs = finder.castView(view, 2131558553, "field 'thumbs'");
    view = finder.findRequiredView(source, 2131558662, "field 'seek'");
    target.seek = finder.castView(view, 2131558662, "field 'seek'");
    view = finder.findRequiredView(source, 2131558611, "field 'pager'");
    target.pager = finder.castView(view, 2131558611, "field 'pager'");
    view = finder.findRequiredView(source, 2131558666, "field 'menu'");
    target.menu = finder.castView(view, 2131558666, "field 'menu'");
    view = finder.findRequiredView(source, 2131558665, "field 'overlay'");
    target.overlay = view;
    view = finder.findRequiredView(source, 2131558541, "method 'onPlay'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onPlay();
        }
      });
    view = finder.findRequiredView(source, 2131558637, "method 'next'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.next();
        }
      });
    view = finder.findRequiredView(source, 2131558636, "method 'previous'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.previous();
        }
      });
    view = finder.findRequiredView(source, 2131558546, "method 'shuffle'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.shuffle();
        }
      });
    view = finder.findRequiredView(source, 2131558667, "method 'onRepeat'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onRepeat();
        }
      });
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558661, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558543, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558663, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558664, "field 'texts'")
    );
    target.actions = Finder.listOf(
        finder.<android.widget.ImageButton>findRequiredView(source, 2131558541, "field 'actions'"),
        finder.<android.widget.ImageButton>findRequiredView(source, 2131558667, "field 'actions'")
    );
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.thumbs = null;
    target.seek = null;
    target.pager = null;
    target.menu = null;
    target.overlay = null;
    target.texts = null;
    target.actions = null;
  }
}
