// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NewsAdapter$NewsItemHolder$$ViewBinder<T extends phantlab.ntertain.app.adapters.NewsAdapter.NewsItemHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558629, "field 'newSource'");
    target.newSource = finder.castView(view, 2131558629, "field 'newSource'");
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558626, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 16908310, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 16908304, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.newSource = null;
    target.texts = null;
  }
}
