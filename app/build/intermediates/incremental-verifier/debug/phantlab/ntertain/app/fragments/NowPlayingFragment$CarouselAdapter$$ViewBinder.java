// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NowPlayingFragment$CarouselAdapter$$ViewBinder<T extends phantlab.ntertain.app.fragments.NowPlayingFragment.CarouselAdapter> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558537, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
  }

  @Override public void unbind(T target) {
    target.thumbnail = null;
  }
}
