// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MoviesDiscussion$$ViewBinder<T extends phantlab.ntertain.app.fragments.MoviesDiscussion> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558610, "field 'no_comments'");
    target.no_comments = finder.castView(view, 2131558610, "field 'no_comments'");
    view = finder.findRequiredView(source, 2131558608, "field 'comment'");
    target.comment = finder.castView(view, 2131558608, "field 'comment'");
    view = finder.findRequiredView(source, 2131558600, "field 'progress'");
    target.progress = finder.castView(view, 2131558600, "field 'progress'");
    view = finder.findRequiredView(source, 2131558609, "method 'onSend'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onSend();
        }
      });
  }

  @Override public void unbind(T target) {
    target.list = null;
    target.no_comments = null;
    target.comment = null;
    target.progress = null;
  }
}
