// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CallertuneAdapter$ChildHolder$$ViewBinder<T extends phantlab.ntertain.app.adapters.CallertuneAdapter.ChildHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558557, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558558, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558556, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.texts = null;
  }
}
