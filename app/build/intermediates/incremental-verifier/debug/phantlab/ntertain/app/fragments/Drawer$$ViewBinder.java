// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class Drawer$$ViewBinder<T extends phantlab.ntertain.app.fragments.Drawer> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558581, "field 'indicator'");
    target.indicator = view;
    view = finder.findRequiredView(source, 2131558580, "field 'thumb'");
    target.thumb = finder.castView(view, 2131558580, "field 'thumb'");
    view = finder.findRequiredView(source, 2131558561, "field 'name'");
    target.name = finder.castView(view, 2131558561, "field 'name'");
  }

  @Override public void unbind(T target) {
    target.list = null;
    target.indicator = null;
    target.thumb = null;
    target.name = null;
  }
}
