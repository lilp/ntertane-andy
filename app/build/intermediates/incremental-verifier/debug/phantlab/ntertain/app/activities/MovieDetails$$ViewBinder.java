// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class MovieDetails$$ViewBinder<T extends phantlab.ntertain.app.activities.MovieDetails> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558607, "field 'tab'");
    target.tab = finder.castView(view, 2131558607, "field 'tab'");
    view = finder.findRequiredView(source, 2131558611, "field 'pager'");
    target.pager = finder.castView(view, 2131558611, "field 'pager'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.tab = null;
    target.pager = null;
  }
}
