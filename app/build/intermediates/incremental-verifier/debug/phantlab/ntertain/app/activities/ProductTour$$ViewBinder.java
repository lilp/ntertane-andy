// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class ProductTour$$ViewBinder<T extends phantlab.ntertain.app.activities.ProductTour> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558611, "field 'pager'");
    target.pager = finder.castView(view, 2131558611, "field 'pager'");
    view = finder.findRequiredView(source, 2131558683, "field 'reveal'");
    target.reveal = finder.castView(view, 2131558683, "field 'reveal'");
    view = finder.findRequiredView(source, 2131558578, "field 'pagination'");
    target.pagination = finder.castView(view, 2131558578, "field 'pagination'");
    view = finder.findRequiredView(source, 2131558684, "method 'signup'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.signup();
        }
      });
    view = finder.findRequiredView(source, 2131558685, "method 'signin'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.signin();
        }
      });
    target.buttons = Finder.listOf(
        finder.<android.widget.Button>findRequiredView(source, 2131558684, "field 'buttons'"),
        finder.<android.widget.Button>findRequiredView(source, 2131558685, "field 'buttons'")
    );
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.pager = null;
    target.reveal = null;
    target.pagination = null;
    target.buttons = null;
  }
}
