// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class SearchActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.SearchActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558675, "field 'placeholder'");
    target.placeholder = finder.castView(view, 2131558675, "field 'placeholder'");
    view = finder.findRequiredView(source, 2131558676, "field 'loader'");
    target.loader = finder.castView(view, 2131558676, "field 'loader'");
    view = finder.findRequiredView(source, 2131558674, "field 'search_results'");
    target.search_results = finder.castView(view, 2131558674, "field 'search_results'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.placeholder = null;
    target.loader = null;
    target.search_results = null;
  }
}
