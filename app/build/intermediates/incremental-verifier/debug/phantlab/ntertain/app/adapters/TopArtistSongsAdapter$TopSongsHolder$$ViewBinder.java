// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TopArtistSongsAdapter$TopSongsHolder$$ViewBinder<T extends phantlab.ntertain.app.adapters.TopArtistSongsAdapter.TopSongsHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findOptionalView(source, 2131558537, null);
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
    view = finder.findOptionalView(source, 2131558540, null);
    target.playback = finder.castView(view, 2131558540, "field 'playback'");
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findOptionalView(source, 16908310, "field 'texts'"),
        finder.<android.widget.TextView>findOptionalView(source, 16908304, "field 'texts'"),
        finder.<android.widget.TextView>findOptionalView(source, 2131558673, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.thumbnail = null;
    target.playback = null;
    target.texts = null;
  }
}
