// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MusicFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.MusicFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558596, "field 'header'");
    target.header = finder.castView(view, 2131558596, "field 'header'");
    view = finder.findRequiredView(source, 2131558615, "field 'recent'");
    target.recent = finder.castView(view, 2131558615, "field 'recent'");
    view = finder.findRequiredView(source, 2131558616, "field 'recent_progress'");
    target.recent_progress = finder.castView(view, 2131558616, "field 'recent_progress'");
    view = finder.findRequiredView(source, 2131558614, "method 'onHeaderClick'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onHeaderClick();
        }
      });
  }

  @Override public void unbind(T target) {
    target.header = null;
    target.recent = null;
    target.recent_progress = null;
  }
}
