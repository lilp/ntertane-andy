// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MyPlaylistFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.MyPlaylistFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'playlist'");
    target.playlist = finder.castView(view, 2131558674, "field 'playlist'");
    view = finder.findRequiredView(source, 2131558670, "field 'panel'");
    target.panel = finder.castView(view, 2131558670, "field 'panel'");
    view = finder.findRequiredView(source, 2131558665, "field 'overlay'");
    target.overlay = view;
    view = finder.findRequiredView(source, 16908310, "field 'title'");
    target.title = finder.castView(view, 16908310, "field 'title'");
    view = finder.findRequiredView(source, 2131558633, "method 'onThumbnail'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onThumbnail();
        }
      });
    target.thumbnails = Finder.listOf(
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558617, "field 'thumbnails'"),
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558671, "field 'thumbnails'")
    );
  }

  @Override public void unbind(T target) {
    target.playlist = null;
    target.panel = null;
    target.overlay = null;
    target.title = null;
    target.thumbnails = null;
  }
}
