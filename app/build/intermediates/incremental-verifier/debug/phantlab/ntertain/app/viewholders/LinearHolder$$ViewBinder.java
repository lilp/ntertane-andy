// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.viewholders;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LinearHolder$$ViewBinder<T extends phantlab.ntertain.app.viewholders.LinearHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558495, "field 'icon'");
    target.icon = finder.castView(view, 2131558495, "field 'icon'");
    view = finder.findRequiredView(source, 16908310, "field 'title'");
    target.title = finder.castView(view, 16908310, "field 'title'");
  }

  @Override public void unbind(T target) {
    target.icon = null;
    target.title = null;
  }
}
