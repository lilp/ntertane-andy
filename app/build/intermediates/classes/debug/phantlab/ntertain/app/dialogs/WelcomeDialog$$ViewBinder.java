// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.dialogs;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class WelcomeDialog$$ViewBinder<T extends phantlab.ntertain.app.dialogs.WelcomeDialog> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558633, "method 'onClose'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onClose();
        }
      });
  }

  @Override public void unbind(T target) {
  }
}
