// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomePagerAdapter$$ViewBinder<T extends phantlab.ntertain.app.adapters.HomePagerAdapter> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558597, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558543, "field 'texts'")
    );
    target.thumbnails = Finder.listOf(
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558553, "field 'thumbnails'"),
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558537, "field 'thumbnails'")
    );
  }

  @Override public void unbind(T target) {
    target.texts = null;
    target.thumbnails = null;
  }
}
