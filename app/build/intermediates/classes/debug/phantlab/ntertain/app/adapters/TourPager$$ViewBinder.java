// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TourPager$$ViewBinder<T extends phantlab.ntertain.app.adapters.TourPager> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558690, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558690, "field 'thumbnail'");
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558626, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558691, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.thumbnail = null;
    target.texts = null;
  }
}
