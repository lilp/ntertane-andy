// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MySongsFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.MySongsFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558546, "method 'onShuffle'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onShuffle();
        }
      });
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558618, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558546, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.list = null;
    target.texts = null;
  }
}
