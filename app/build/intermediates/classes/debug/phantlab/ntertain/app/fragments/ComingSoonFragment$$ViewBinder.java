// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ComingSoonFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.ComingSoonFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558611, "field 'pager'");
    target.pager = finder.castView(view, 2131558611, "field 'pager'");
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    target.progress = Finder.listOf(
        finder.<phantlab.ntertain.app.ui.Progress>findRequiredView(source, 2131558651, "field 'progress'"),
        finder.<phantlab.ntertain.app.ui.Progress>findRequiredView(source, 2131558652, "field 'progress'")
    );
  }

  @Override public void unbind(T target) {
    target.pager = null;
    target.list = null;
    target.progress = null;
  }
}
