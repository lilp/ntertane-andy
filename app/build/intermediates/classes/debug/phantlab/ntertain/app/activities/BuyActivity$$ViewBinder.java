// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class BuyActivity$$ViewBinder<T extends phantlab.ntertain.app.activities.BuyActivity> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558553, "field 'blur'");
    target.blur = finder.castView(view, 2131558553, "field 'blur'");
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.blur = null;
  }
}
