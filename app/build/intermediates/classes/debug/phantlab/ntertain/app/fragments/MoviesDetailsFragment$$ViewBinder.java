// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MoviesDetailsFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.MoviesDetailsFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558537, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
    view = finder.findRequiredView(source, 2131558605, "field 'cast'");
    target.cast = finder.castView(view, 2131558605, "field 'cast'");
    view = finder.findRequiredView(source, 2131558602, "method 'watchTrailerClick'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.watchTrailerClick();
        }
      });
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 16908310, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 16908304, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558604, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558606, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558603, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.thumbnail = null;
    target.cast = null;
    target.texts = null;
  }
}
