// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.ui;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class Screen$$ViewBinder<T extends phantlab.ntertain.app.ui.Screen> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findOptionalView(source, 2131558622, null);
    target.toolbar = finder.castView(view, 2131558622, "field 'toolbar'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
  }
}
