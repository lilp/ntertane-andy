// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ArtistTracksFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.ArtistTracksFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558547, "field 'progress'");
    target.progress = finder.castView(view, 2131558547, "field 'progress'");
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558543, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558544, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558545, "field 'texts'")
    );
    target.images = Finder.listOf(
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558617, "field 'images'"),
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558537, "field 'images'")
    );
  }

  @Override public void unbind(T target) {
    target.list = null;
    target.progress = null;
    target.texts = null;
    target.images = null;
  }
}
