// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NewsActivity$$ViewBinder<T extends phantlab.ntertain.app.fragments.NewsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558611, "field 'pager'");
    target.pager = finder.castView(view, 2131558611, "field 'pager'");
    view = finder.findRequiredView(source, 2131558627, "field 'progress'");
    target.progress = finder.castView(view, 2131558627, "field 'progress'");
  }

  @Override public void unbind(T target) {
    target.list = null;
    target.pager = null;
    target.progress = null;
  }
}
