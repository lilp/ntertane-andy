// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class NewsDetails$$ViewBinder<T extends phantlab.ntertain.app.activities.NewsDetails> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558624, "field 'mWebview'");
    target.mWebview = finder.castView(view, 2131558624, "field 'mWebview'");
    view = finder.findRequiredView(source, 2131558625, "field 'progress'");
    target.progress = finder.castView(view, 2131558625, "field 'progress'");
    target.mNewsTitleView = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558496, "field 'mNewsTitleView'"),
        finder.<android.widget.TextView>findRequiredView(source, 16908310, "field 'mNewsTitleView'"),
        finder.<android.widget.TextView>findRequiredView(source, 16908304, "field 'mNewsTitleView'")
    );
    target.mNewsThumb = Finder.listOf(
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558579, "field 'mNewsThumb'"),
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558629, "field 'mNewsThumb'")
    );
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.mWebview = null;
    target.progress = null;
    target.mNewsTitleView = null;
    target.mNewsThumb = null;
  }
}
