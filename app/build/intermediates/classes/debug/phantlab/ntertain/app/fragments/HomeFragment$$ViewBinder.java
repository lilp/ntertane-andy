// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.HomeFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558594, "field 'news'");
    target.news = finder.castView(view, 2131558594, "field 'news'");
    view = finder.findRequiredView(source, 2131558588, "field 'music'");
    target.music = finder.castView(view, 2131558588, "field 'music'");
    view = finder.findRequiredView(source, 2131558584, "field 'pager'");
    target.pager = finder.castView(view, 2131558584, "field 'pager'");
    view = finder.findRequiredView(source, 2131558591, "field 'featured_movies'");
    target.featured_movies = finder.castView(view, 2131558591, "field 'featured_movies'");
    view = finder.findRequiredView(source, 2131558578, "field 'pagination'");
    target.pagination = finder.castView(view, 2131558578, "field 'pagination'");
    target.texts = Finder.listOf(
        finder.<android.widget.LinearLayout>findRequiredView(source, 2131558587, "field 'texts'"),
        finder.<android.widget.LinearLayout>findRequiredView(source, 2131558590, "field 'texts'"),
        finder.<android.widget.LinearLayout>findRequiredView(source, 2131558593, "field 'texts'")
    );
    target.loading = Finder.listOf(
        finder.<phantlab.ntertain.app.ui.Progress>findRequiredView(source, 2131558592, "field 'loading'"),
        finder.<phantlab.ntertain.app.ui.Progress>findRequiredView(source, 2131558595, "field 'loading'"),
        finder.<phantlab.ntertain.app.ui.Progress>findRequiredView(source, 2131558589, "field 'loading'"),
        finder.<phantlab.ntertain.app.ui.Progress>findRequiredView(source, 2131558585, "field 'loading'")
    );
  }

  @Override public void unbind(T target) {
    target.news = null;
    target.music = null;
    target.pager = null;
    target.featured_movies = null;
    target.pagination = null;
    target.texts = null;
    target.loading = null;
  }
}
