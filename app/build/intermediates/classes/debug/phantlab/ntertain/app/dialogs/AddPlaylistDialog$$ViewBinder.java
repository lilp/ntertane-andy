// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.dialogs;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AddPlaylistDialog$$ViewBinder<T extends phantlab.ntertain.app.dialogs.AddPlaylistDialog> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558535, "field 'playlist'");
    target.playlist = finder.castView(view, 2131558535, "field 'playlist'");
    view = finder.findRequiredView(source, 2131558536, "field 'no_playlist'");
    target.no_playlist = finder.castView(view, 2131558536, "field 'no_playlist'");
    view = finder.findRequiredView(source, 2131558552, "method 'onCancel'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onCancel();
        }
      });
  }

  @Override public void unbind(T target) {
    target.playlist = null;
    target.no_playlist = null;
  }
}
