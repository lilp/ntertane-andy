// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PlayMusicActivity$ThumbnailPagerAdapter$$ViewBinder<T extends phantlab.ntertain.app.activities.PlayMusicActivity.ThumbnailPagerAdapter> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558537, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
  }

  @Override public void unbind(T target) {
    target.thumbnail = null;
  }
}
