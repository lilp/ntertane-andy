// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.fragments;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RadioFragment$$ViewBinder<T extends phantlab.ntertain.app.fragments.RadioFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558607, "field 'tab'");
    target.tab = finder.castView(view, 2131558607, "field 'tab'");
    view = finder.findRequiredView(source, 2131558611, "field 'pager'");
    target.pager = finder.castView(view, 2131558611, "field 'pager'");
  }

  @Override public void unbind(T target) {
    target.tab = null;
    target.pager = null;
  }
}
