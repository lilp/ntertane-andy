// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.ui;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class FeaturedHomeMusic$$ViewBinder<T extends phantlab.ntertain.app.ui.FeaturedHomeMusic> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 16908310, "field 'title'");
    target.title = finder.castView(view, 16908310, "field 'title'");
    view = finder.findRequiredView(source, 2131558537, "field 'thumbnail'");
    target.thumbnail = finder.castView(view, 2131558537, "field 'thumbnail'");
  }

  @Override public void unbind(T target) {
    target.title = null;
    target.thumbnail = null;
  }
}
