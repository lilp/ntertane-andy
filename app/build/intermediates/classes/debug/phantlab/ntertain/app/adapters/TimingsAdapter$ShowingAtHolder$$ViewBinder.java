// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TimingsAdapter$ShowingAtHolder$$ViewBinder<T extends phantlab.ntertain.app.adapters.TimingsAdapter.ShowingAtHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558680, "field 'timings'");
    target.timings = finder.castView(view, 2131558680, "field 'timings'");
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 2131558677, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558678, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558679, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    target.timings = null;
    target.texts = null;
  }
}
