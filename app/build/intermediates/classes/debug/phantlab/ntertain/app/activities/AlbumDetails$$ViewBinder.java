// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class AlbumDetails$$ViewBinder<T extends phantlab.ntertain.app.activities.AlbumDetails> extends phantlab.ntertain.app.ui.Screen$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131558537, "field 'thumb'");
    target.thumb = finder.castView(view, 2131558537, "field 'thumb'");
    view = finder.findRequiredView(source, 2131558674, "field 'list'");
    target.list = finder.castView(view, 2131558674, "field 'list'");
    view = finder.findRequiredView(source, 2131558542, "field 'progress'");
    target.progress = finder.castView(view, 2131558542, "field 'progress'");
    target.texts = Finder.listOf(
        finder.<android.widget.TextView>findRequiredView(source, 16908310, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558540, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558538, "field 'texts'"),
        finder.<android.widget.TextView>findRequiredView(source, 2131558539, "field 'texts'")
    );
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.thumb = null;
    target.list = null;
    target.progress = null;
    target.texts = null;
  }
}
