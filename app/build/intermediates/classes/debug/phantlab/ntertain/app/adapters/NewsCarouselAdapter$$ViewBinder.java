// Generated code from Butter Knife. Do not modify!
package phantlab.ntertain.app.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NewsCarouselAdapter$$ViewBinder<T extends phantlab.ntertain.app.adapters.NewsCarouselAdapter> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558628, "field 'title'");
    target.title = finder.castView(view, 2131558628, "field 'title'");
    target.thumbnails = Finder.listOf(
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558537, "field 'thumbnails'"),
        finder.<com.facebook.drawee.view.SimpleDraweeView>findRequiredView(source, 2131558629, "field 'thumbnails'")
    );
  }

  @Override public void unbind(T target) {
    target.title = null;
    target.thumbnails = null;
  }
}
