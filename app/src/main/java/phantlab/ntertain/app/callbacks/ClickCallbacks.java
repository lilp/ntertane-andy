package phantlab.ntertain.app.callbacks;

/**
 * Created by Ddev on 11/22/2015.
 */
public class ClickCallbacks {
    public interface ClickCallback {
        void onClick(int position);
    }

    public interface OverflowCallback {
        void onOverflowClick(int position);
    }

    public interface DataCallback {
        void onClick(int position, Object object);
    }
}
