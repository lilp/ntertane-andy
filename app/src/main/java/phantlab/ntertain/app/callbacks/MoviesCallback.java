package phantlab.ntertain.app.callbacks;

import java.util.List;

import phantlab.ntertain.app.movies.Movie;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.movies.Shows;

/**
 * Created by Ddev on 11/27/2015.
 */
public class MoviesCallback {
    public interface Result {
        void onSuccess(List<Movie> result);
    }

    public interface StringResult {
        void onSuccess(List<MovieMeta> result);
    }

    public interface ThumbResult {
        void onSuccess(String result);
    }

    public interface ShowsResult {
        void onSuccess(List<Shows> shows);
    }

    public interface CastResult {
        void onSuccess(List<String> result);
    }
}
