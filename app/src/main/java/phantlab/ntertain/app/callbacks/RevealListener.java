package phantlab.ntertain.app.callbacks;

/**
 * Created by Ddev on 11/21/2015.
 */
public interface RevealListener {
    void onRevealed();
}
