package phantlab.ntertain.app.callbacks;

/**
 * Created by Ddev on 9/16/2015.
 */
public interface AsyncTaskCallbacks {
    void onPostExecute(String URI);
}
