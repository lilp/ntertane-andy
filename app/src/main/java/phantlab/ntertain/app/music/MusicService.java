package phantlab.ntertain.app.music;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.RemoteViews;

import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;

import java.util.List;
import java.util.Random;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.BaseActivity;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.bus.BusProvider;
import phantlab.ntertain.app.bus.PlayBackStateChangedEvent;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 27/12/15.
 */
public class MusicService extends Service implements FocusListener,
        ExoPlayer.Listener, ViewPager.OnPageChangeListener {

    public static final String ACTION_PLAYPAUSE
            = "phantlab.ntertane.app.ACTION_PLAY_PAUSE";
    public static final String ACTION_PLAY
            = "phantlab.ntertane.app.ACTION_PLAY";
    public static final String ACTION_PAUSE
            = "phantlab.ntertane.app.ACTION_PAUSE";
    public static final String ACTION_NEXT
            = "phantlab.ntertane.app.ACTION_NEXT";
    public static final String ACTION_PREV
            = "phantlab.ntertane.app.ACTION_PREVIOUS";
    public static final String ACTION_EXIT
            = "phantlab.ntertane.app.ACTION_EXIT";
    public static final String ACTION_FAVOURITE
            = "phantlab.ntertane.app.ACTION_FAVOURITE";
    private final static String TAG = MusicService.class.getSimpleName();
    private final static int NOTIFICATION_ID = 1009;
    NtertainPlayer player;
    AudioFocusManager manager;
    MediaSessionCompat mMediaSessionCompat;
    Notification notification;
    NotificationManager mNotificationManger;
    NotificationCompat.Builder builder;
    ExoPlayer.Listener listener;
    AudioFocus mAudioFocus = AudioFocus.NoFocusNoDuck;
    RemoteViews small_view, bigNotification;
    Tracks track;
    private int COVER_WIDTH = 1000, offset;

    private MediaControllerCompat.TransportControls mTransportControls;
    private boolean isNotificationShowing = false,
            isRepeating = false;
    private List<Tracks> tracks;
    private ViewPager pager;
    private MediaSessionCompat.Callback mMediaSessionCallback = new MediaSessionCompat.Callback() {
        @Override
        public boolean onMediaButtonEvent(Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                pause();
            } else if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
                KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
                if (keyEvent == null) {
                    return false;
                }
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.KEYCODE_HEADSETHOOK:
                    case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                        play(null);
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PLAY:
                        play(null);
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PAUSE:
                        pause();
                        break;
                    case KeyEvent.KEYCODE_MEDIA_STOP:
                        stop();
                        break;
                    case KeyEvent.KEYCODE_MEDIA_NEXT:
                        next();
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                        previous();
                        break;
                }
            }
            return super.onMediaButtonEvent(intent);
        }
    };

    @Override
    public void onGainFocus() {

    }

    @Override
    public void onLostAudioFocus(boolean mCanDuck) {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MusicServiceBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action;
        if (intent != null && intent.getAction() != null) {
            action = intent.getAction();
            switch (action) {
                case ACTION_PLAYPAUSE:
                    PlayBackStateChangedEvent.State state;
                    if (player.getControl().isPlaying()) {
                        state = PlayBackStateChangedEvent.State.Paused;
                        player.pause();
                        setPaused(bigNotification);
                        setPaused(small_view);
                    } else {
                        state = PlayBackStateChangedEvent.State.Playing;
                        resume();
                        setPlaying(bigNotification);
                        setPlaying(small_view);
                    }
                    if (isNotificationShowing)
                        mNotificationManger.notify(NOTIFICATION_ID, notification);
                    BusProvider.getInstance().post(new PlayBackStateChangedEvent(state, track));
                    break;
                case ACTION_NEXT:
                    next();
                    break;
                case ACTION_PREV:
                    previous();
                    break;
                case ACTION_EXIT:
                    pause();
                    clearNotification();
                    break;
            }

            if (action.equals(ACTION_NEXT) || action.equals(ACTION_PREV))
                showNotification();
        }
        return START_STICKY;
    }

    public void play(Tracks track) {
        if (track.isPlayable()) {
            getFocus();
            this.track = track;
            player.stream(Uri.parse(track.getUrl().replaceAll(" ", "%20")));
            updateLockscreenControls();
            BusProvider.getInstance().post(new PlayBackStateChangedEvent(PlayBackStateChangedEvent.State.NewSongPlaying, track));
        }
    }

    public void next() {
        if (offset < (tracks.size() - 1)) offset += 1;
        else offset = 0;
        onOffsetChanged();
        play(tracks.get(offset));
    }

    public void previous() {
        if (offset > 0) offset -= 1;
        else offset = tracks.size() - 1;
        onOffsetChanged();
        play(tracks.get(offset));
    }

    private void onOffsetChanged() {
        if (pager != null)
            pager.setCurrentItem(offset, true);
    }

    public void playFromList(List<Tracks> tracks, int offset) {
        this.tracks = tracks;
        this.offset = offset;
        play(tracks.get(offset));
    }

    private boolean hasMoreSongs() {
        if (tracks != null)
            return ((tracks.size() - 1) > offset);
        return false;
    }

    public List<Tracks> getPlayingTracks() {
        return tracks;
    }

    private void updateLockscreenControls() {
        Log.d(TAG, "updateLockscreenControls()");

        if (mMediaSessionCompat == null) {
            ComponentName componentName = new ComponentName(this, MediaButtonReceiver.class);
            Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
            intent.setComponent(componentName);
            mMediaSessionCompat = new MediaSessionCompat(
                    getApplicationContext(), TAG, componentName, null);
            mMediaSessionCompat.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                    MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            intent = new Intent(MusicService.this, BaseActivity.class);
            intent.setAction("SHOW_FRAGEMNT_ONSTART");
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(MusicService.this, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mMediaSessionCompat.setSessionActivity(pendingIntent);
            mMediaSessionCompat.setCallback(mMediaSessionCallback);
            mTransportControls = mMediaSessionCompat.getController().getTransportControls();
        }

        updatePlayBackState();
        mMediaSessionCompat.setActive(true);
//        updateMetaData(null);
        updateAlbumArt();
    }

    private void updateMetaData(@Nullable Bitmap bitmap) {
        MediaMetadataCompat.Builder builder = new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ARTIST,
                        track.getArtists()[0])
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST,
                        track.getArtists()[0])
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE,
                        track.getTrack())
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION,
                        1000)
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap == null ?
                        BitmapFactory.decodeResource(getResources(), R.drawable.weezy) :
                        bitmap);
        mMediaSessionCompat.setMetadata(builder.build());
    }

    public void updateAlbumArt() {
        if (isNotificationShowing) {
            ThumbnailController.with(null).from(getApplicationContext())
                    .uri(Uri.parse(track.getThumbnail()))
                    .dimens(Utils.convertDpToPixel(COVER_WIDTH), Utils.convertDpToPixel(COVER_WIDTH))
                    .dataSource(new BaseBitmapDataSubscriber() {
                        @Override
                        protected void onNewResultImpl(@Nullable Bitmap bitmap) {
                            if (bitmap != null)
                                Log.i("$bitmap", String.format("received bitmap with height %d & with %d & dpValue = %d", bitmap.getHeight(), bitmap.getWidth(), Utils.convertDpToPixel(300)));
//                            bigNotification.setImageViewBitmap(R.id.album_art, bitmap);
                            small_view.setImageViewBitmap(R.id.album_art, bitmap);
//                            mNotificationManger.notify(NOTIFICATION_ID, notification);
//                            updateMetaData(bitmap);
                        }

                        @Override
                        protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {

                        }
                    }).getBitmap();
        }
    }

    private void updatePlayBackState() {
        long actions = PlaybackStateCompat.ACTION_PLAY |
                PlaybackStateCompat.ACTION_PAUSE |
                PlaybackStateCompat.ACTION_STOP |
                PlaybackStateCompat.ACTION_SEEK_TO;

        PlaybackStateCompat playbackStateCompat = new PlaybackStateCompat.Builder()
                .setActions(actions)
                .setState(PlaybackStateCompat.STATE_PLAYING, 1000, 1f)
                .build();
        mMediaSessionCompat.setPlaybackState(playbackStateCompat);
    }

    public void showNotification() {
        builder = new NotificationCompat.Builder(this);

        Intent intent = new Intent(ACTION_PLAYPAUSE, null, MusicService.this,
                MusicService.class);
        PendingIntent play_pause = PendingIntent.getService(MusicService.this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        intent = new Intent(ACTION_EXIT, null, MusicService.this,
                MusicService.class);
        PendingIntent close = PendingIntent.getService(MusicService.this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        intent = new Intent(ACTION_NEXT, null, MusicService.this,
                MusicService.class);
        PendingIntent next = PendingIntent.getService(MusicService.this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        intent = new Intent(ACTION_PREV, null, MusicService.this,
                MusicService.class);
        PendingIntent previous = PendingIntent.getService(MusicService.this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        small_view = new RemoteViews(getPackageName(), R.layout.notification_small);
        small_view.setInt(R.id.close, "setColorFilter", Color.parseColor("#ffffff"));
        small_view.setOnClickPendingIntent(R.id.play, play_pause);
        small_view.setOnClickPendingIntent(R.id.next, next);
        small_view.setOnClickPendingIntent(R.id.close, close);

//        setTexts(small_view);

        builder.setSmallIcon(R.drawable.ic_notification).setContentTitle(track.getArtists()[0])
                .setContentText(track.getTrack()).setOngoing(true).setPriority(
                NotificationCompat.PRIORITY_MAX);
        builder.setContent(small_view);
        builder.setAutoCancel(true);

        Intent contentIntent = new Intent(MusicService.this, BaseActivity.class);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(MusicService.this, 0, contentIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentPendingIntent);

        notification = builder.build();
//        bigNotification = new RemoteViews(getPackageName(), R.layout.notificatation_large);
//        bigNotification.setOnClickPendingIntent(R.id.play, play_pause);
//        bigNotification.setOnClickPendingIntent(R.id.close, close);
//        bigNotification.setOnClickPendingIntent(R.id.next, next);
//        bigNotification.setOnClickPendingIntent(R.id.previous, previous);
//
//        setTexts(bigNotification);
//        setImages(bigNotification);
//        bigNotification.setInt(R.id.close, "setColorFilter", Color.parseColor("#646464"));
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
//            notification.bigContentView = bigNotification;
        startForeground(NOTIFICATION_ID, notification);
        isNotificationShowing = true;
        updateAlbumArt();
    }

    private void setImages(RemoteViews view) {
        view.setImageViewResource(R.id.play, R.drawable.ic_pause_white_24dp);
        view.setImageViewResource(R.id.favourite_action, R.drawable.ic_add_white_24dp);
    }

    public void clearNotification() {
        Log.i("$clearNotification", "clearing notifications & notificationManager is " + (mNotificationManger == null));
        stopForeground(true);
        mNotificationManger.cancel(NOTIFICATION_ID);
        isNotificationShowing = false;
    }

    public boolean isNotificationShowing() {
        return isNotificationShowing;
    }

    private void setTexts(RemoteViews view) {
        view.setTextViewText(android.R.id.title, track.getTrack());
        view.setTextViewText(R.id.album, track.getAlbum());
        view.setTextViewText(R.id.artist, track.getArtists()[0]);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        BusProvider.getInstance().register(this);
        mNotificationManger = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager = new AudioFocusManager(getApplicationContext(), this);
        player = new NtertainPlayer();
        player.init(getApplicationContext());
        player.setListener(this);
    }

    private void getFocus() {
        if (mAudioFocus != AudioFocus.Focused && manager != null && manager.requestFocus())
            mAudioFocus = AudioFocus.Focused;
    }

    public void pause() {
        player.pause();
        BusProvider.getInstance().post(new PlayBackStateChangedEvent(PlayBackStateChangedEvent.State.Paused, track));
    }

    public void seekTo(long ms) {
        player.seekTo(ms);
    }

    public void resume() {
        player.resume();
    }

    private void stop() {
        NtertainPlayer.getPlayer().stop();
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (listener != null)
            listener.onPlayerStateChanged(playWhenReady, playbackState);

        if (playbackState == ExoPlayer.STATE_ENDED)
            if (isRepeating) {
                seekTo(0);
                resume();
            } else {
                offset += 1;
                if (hasMoreSongs())
                    play(tracks.get(offset));
                else {
                    seekTo(0);
                    pause();
                    BusProvider.getInstance().post(new PlayBackStateChangedEvent(PlayBackStateChangedEvent.State.Stop, track));
                }
            }
    }

    @Override
    public void onPlayWhenReadyCommitted() {
        if (listener != null)
            listener.onPlayWhenReadyCommitted();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
        player.release();
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        if (listener != null)
            listener.onPlayerError(error);
    }

    public void setListener(ExoPlayer.Listener listener) {
        this.listener = listener;
    }

    public void setViewPager(ViewPager pager) {
        this.pager = pager;
        pager.addOnPageChangeListener(this);
    }

    public Tracks getTrack() {
        return track;
    }

    public boolean isPlaying() {
        return player.getControl().isPlaying();
    }

    private void setPlaying(RemoteViews view) {
        if (isNotificationShowing && view != null)
            view.setImageViewResource(R.id.play, R.drawable.ic_pause_white_24dp);
    }

    public boolean isRepeating() {
        return isRepeating;
    }

    public void setRepeating(boolean isRepeating) {
        this.isRepeating = isRepeating;
    }

    private void setPaused(RemoteViews view) {
        if (isNotificationShowing && view != null)
            view.setImageViewResource(R.id.play, R.drawable.ic_play_arrow_white_24dp);
    }

    public NtertainPlayer getPlayer() {
        return player;
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        play(tracks.get(position));
        this.offset = position;
    }

    public void shuffle() {
        Random random = new Random();
        int mRandomOffset = random.nextInt(tracks.size() - 1);
        play(tracks.get(mRandomOffset));
        offset = mRandomOffset;
        pager.setCurrentItem(offset, true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    enum AudioFocus {
        NoFocusNoDuck,
        NoFocusCanDuck,
        Focused
    }

    public class MusicServiceBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }
}