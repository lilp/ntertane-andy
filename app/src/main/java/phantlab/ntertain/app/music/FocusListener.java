package phantlab.ntertain.app.music;

/**
 * Created by kartikeyasingh on 28/12/15.
 */
public interface FocusListener {
    void onGainFocus();
    void onLostAudioFocus(boolean mCanDuck);
}
