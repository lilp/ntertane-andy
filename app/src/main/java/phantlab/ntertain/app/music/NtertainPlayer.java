package phantlab.ntertain.app.music;

import android.content.Context;
import android.net.Uri;
import android.widget.MediaController;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.PlayerControl;

/**
 * Created by kartikeyasingh on 17/12/15.
 */
public class NtertainPlayer {
    public static final String AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
    private static ExoPlayer player;
    private final int RENDERER_COUNT = 1;
    private MediaController.MediaPlayerControl control;
    private Context context;
    private DataSource source;

    public static ExoPlayer getPlayer() {
        return player;
    }

    public void init(Context context) {
        this.context = context;
        player = ExoPlayer.Factory.newInstance(RENDERER_COUNT);
        control = new PlayerControl(player);
    }

    public void setListener(ExoPlayer.Listener listener) {
        player.addListener(listener);
    }

    public void stream(Uri uri) {
        source = new DefaultUriDataSource(context, AGENT);

        ExtractorSampleSource extractor = new ExtractorSampleSource(
                uri, source, new DefaultAllocator(8000), 1000 * 1000);

        MediaCodecAudioTrackRenderer mAudioRenderer = new MediaCodecAudioTrackRenderer(extractor);

        if (control.isPlaying()) {
            player.stop();
            player.seekTo(0);
        }
        player.prepare(mAudioRenderer);
        player.setPlayWhenReady(true);
    }

    public MediaController.MediaPlayerControl getControl() {
        return control;
    }

    public void seekTo(long ms) {
        player.seekTo(ms);
    }

    public void pause() {
        control.pause();
    }

    public void resume() {
        control.start();
    }

    public int getPosition() {
        return control.getCurrentPosition();
    }

    public void release() {
        player.stop();
        player.release();
    }
}
