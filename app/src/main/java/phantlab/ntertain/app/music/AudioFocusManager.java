package phantlab.ntertain.app.music;

import android.content.Context;
import android.media.AudioManager;

/**
 * Created by kartikeyasingh on 28/12/15.
 */
public class AudioFocusManager implements AudioManager.OnAudioFocusChangeListener {
    FocusListener listener;
    AudioManager audioManager;

    public AudioFocusManager(Context context, FocusListener listener) {
        this.listener = listener;
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    public boolean requestFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    public boolean abandonFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == audioManager.abandonAudioFocus(this);
    }

    @Override
    public void onAudioFocusChange(int focus) {
        if (listener == null)
            return;

        switch (focus) {
            case AudioManager.AUDIOFOCUS_GAIN:
                listener.onGainFocus();
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                listener.onLostAudioFocus(false);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                listener.onLostAudioFocus(true);
                break;
        }
    }
}
