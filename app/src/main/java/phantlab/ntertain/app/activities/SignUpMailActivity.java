package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.widget.Button;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 12/12/15.
 */
public class SignUpMailActivity extends Screen {
    @Bind(R.id.account_status)
    Button account;

    @Override
    protected void onCreate(Bundle bundle) {
        onCreate();
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        setContentView(R.layout.facebook_signup);
    }

    @OnClick(R.id.mail_signup)
    public void mailSingup(){
        Utils.StartActivity(SignUpMailActivity.this, SignUpActivity.class);
    }

    @Override
    public void setup() {
        account.setText(R.string.have_account);
    }
}
