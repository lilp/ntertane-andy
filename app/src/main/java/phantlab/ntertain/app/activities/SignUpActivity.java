package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.UserEndpoint;
import phantlab.ntertain.app.api.model.UserCallback;
import phantlab.ntertain.app.application.Ntertain;
import phantlab.ntertain.app.application.UserUtils;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.utils.Validation;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 11/21/2015.
 */
public class SignUpActivity extends Screen {
    private final String HAVE_ACCOUNT = "Already have an account ? Sign in";
    @Bind(R.id.account_status)
    Button account;
    @Bind({R.id.username, R.id.password, R.id.email})
    List<EditText> edits;
    @Bind(R.id.signup)
    Button signup;

    UserEndpoint user;
    Validation mValidation;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate();
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        user = new UserEndpoint();
        setContentView(R.layout.signup_mail);
        mValidation = Validation.getInstance();
        mValidation.add(edits.get(0), "Username", Validation.Type.EMPTY);
        mValidation.add(edits.get(1), "Password", Validation.Type.EMPTY);
        mValidation.add(edits.get(2), "Email", Validation.Type.EMAIL);
    }

    @Override
    public void setup() {
        account.setText(HAVE_ACCOUNT);
    }

    @OnClick(R.id.account_status)
    public void onSignUpStatus() {
        Utils.StartActivity(this, SignInActivity.class);
    }

    @OnClick(R.id.signup)
    public void onSignUp() {
        if (mValidation.validate()) {
            String username = edits.get(0).getText().toString(),
                    password = edits.get(1).getText().toString(),
                    email = edits.get(2).getText().toString();
            signup.setText("Signing up ...");
            user.register(username, email, username, password, new Callback<UserCallback>() {
                @Override
                public void success(UserCallback cb, Response response) {
                    signup.setText(R.string.signup);
                    if (cb.isSuccessFull()) {
                        UserUtils.saveUser(((Ntertain) getApplication()), cb.getUser().setToken(cb.getToken()));
                        Utils.StartActivity(SignUpActivity.this, BaseActivity.class);
                        finish();
                    } else
                        Utils.showToast("Unable to register. ".concat(cb.getMessage()));
                }

                @Override
                public void failure(RetrofitError error) {
                    signup.setText(R.string.signup);
                    Utils.showToast("Something went wrong while signing you up.");
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mValidation.release();
    }
}