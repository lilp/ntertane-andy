package phantlab.ntertain.app.activities;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.SearchAdapter;
import phantlab.ntertain.app.api.Search;
import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.api.model.Artist;
import phantlab.ntertain.app.api.model.ResultItem;
import phantlab.ntertain.app.api.model.ResultType;
import phantlab.ntertain.app.api.model.SearchCallback;
import phantlab.ntertain.app.api.model.SearchResult;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 11/12/15.
 */
public class SearchActivity extends Screen {

    @Bind(R.id.search_placeholder)
    LinearLayout placeholder;

    @Bind(R.id.loading)
    Progress loader;

    @Bind(R.id.list)
    RecyclerView search_results;

    private Search search;
    private SearchAdapter mSearchAdapter;
    private int counter;

    @Override
    public void onCreate(Bundle bundle) {
        onCreate();
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        setContentView(R.layout.search);
        loader.setVisibility(View.GONE);
        search = new Search();
    }

    @Override
    public void setup() {
        disableIcon();
        title.setText("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        search_results.setLayoutManager(new LinearLayoutManager(this));
        mSearchAdapter = new SearchAdapter(this);
        search_results.setAdapter(mSearchAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_widget, menu);
        setSearchView(menu);
        menu.findItem(R.id.search).expandActionView();
        return true;
    }


    private void setSearchView(Menu menu) {
        final SearchManager mSearchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView mSearchView = (SearchView) menu.findItem(R.id.search).getActionView();

        LinearLayout ll = (LinearLayout) mSearchView.getChildAt(0),
                ll2 = (LinearLayout) ll.getChildAt(2),
                ll3 = (LinearLayout) ll2.getChildAt(1);

        final SearchView.SearchAutoComplete search = (SearchView.SearchAutoComplete) ll3.getChildAt(0);
        search.setHintTextColor(Color.parseColor("#c5c5c5"));
        search.setHint("Davido");
        search.setTextColor(getResources().getColor(R.color.buttons_yellow));
        search.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        mSearchView.setSearchableInfo(
                mSearchManager.getSearchableInfo(getComponentName()));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mSearchView.clearFocus();
                return true;
            }

            private void handleSearch(final String query) {
                placeholder.setVisibility(View.GONE);

                loader.start();
                loader.setVisibility(View.VISIBLE);

                SearchActivity.this.search.search(query, new Callback<SearchCallback>() {
                    @Override
                    public void success(SearchCallback search, Response response) {
                        loader.stop();
                        loader.setVisibility(View.GONE);

                        SearchResult mSearchResult = search.getResult();
                        List<ResultItem> result = new ArrayList<>();
                        List<Artist> artists = mSearchResult.getArtists();
                        List<Albums> albums = mSearchResult.getAlbums();
                        List<Tracks> songs = mSearchResult.getTracks();

                        result.add(new ResultItem(null).setHeader(ResultType.ARTIST.name()));

                        for (Artist artist : artists)
                        result.add(new ResultItem(ResultType.ARTIST).setArtistItem(artist));

                        result.add(new ResultItem(null).setHeader("SONGS"));

                        for (Tracks track : songs)
                            result.add(new ResultItem(ResultType.TRACK).setTrackItem(track));

                        result.add(new ResultItem(null).setHeader("ALBUMS"));

                        for (Albums album : albums)
                            result.add(new ResultItem(ResultType.ALBUM).setAlbumItem(album));

                        search_results.setVisibility(View.VISIBLE);
                        mSearchAdapter.setData(result);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        loader.stop();
                        loader.setVisibility(View.GONE);
                        placeholder.setVisibility(View.VISIBLE);
                        mSearchAdapter.setData(null);
                    }
                });
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (counter == 2) {
                    if (query.length() > 0)
                        handleSearch(query);
                    counter = 1;
                }
                counter += 1;
                return true;
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
