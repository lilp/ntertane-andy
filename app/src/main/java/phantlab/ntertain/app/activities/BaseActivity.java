package phantlab.ntertain.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.Geocode;
import phantlab.ntertain.app.api.Radio;
import phantlab.ntertain.app.api.model.DistanceCallback;
import phantlab.ntertain.app.api.model.GeocodeCallback;
import phantlab.ntertain.app.api.model.GeocodeResults;
import phantlab.ntertain.app.api.model.RadioCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.application.UserUtils;
import phantlab.ntertain.app.databases.MyMusicDatabase;
import phantlab.ntertain.app.dialogs.AddPlaylistDialog;
import phantlab.ntertain.app.dialogs.DownloadMusicDialog;
import phantlab.ntertain.app.fragments.Drawer;
import phantlab.ntertain.app.fragments.HomeFragment;
import phantlab.ntertain.app.fragments.MoviesFragment;
import phantlab.ntertain.app.fragments.MusicFragment;
import phantlab.ntertain.app.fragments.MyCallerTunez;
import phantlab.ntertain.app.fragments.NewsActivity;
import phantlab.ntertain.app.fragments.RadioFragment;
import phantlab.ntertain.app.fragments.SendSongFragment;
import phantlab.ntertain.app.movies.NtertainLocation;
import phantlab.ntertain.app.music.MusicService;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.GPSHelper;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.video.Youtube;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 12/3/2015.
 */
public class BaseActivity extends Screen implements Drawer.DrawerCallbacks {

    @Bind(R.id.drawer)
    DrawerLayout drawer;
    LocationManager mLocationManager;

    MenuItem search;
    Fragment mCurrentFragment;
    MusicService mService;
    private boolean isBound = false, isPaused;
    private GoogleApiClient mGoogleApiClient;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = ((MusicService.MusicServiceBinder) iBinder).getService();
            Log.i("ServiceConnection", "onConnect & mService == " + (mService == null));
            isBound = true;
            if (mService.isNotificationShowing())
                mService.clearNotification();
            setIsBoundActivity(true);
            mStreamingService = mService;
            onChangeContent(new HomeFragment(), R.string.home);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i("ServiceConnection", "onDisconnect");
            mService = null;
            mStreamingService = mService;
            isBound = false;
        }
    };

    SharedPreferences preferences;


    public MusicService getStreamingService() {
        return mService;
    }

    public boolean isBound() {
        return isBound;
    }

    @Override
    public void onCreate(Bundle bundle) {
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        super.onCreate();
        setContentView(R.layout.base);
        setupDrawer();



        preferences = getSharedPreferences(UserUtils.NTERTANE_PREFS,
                Context.MODE_PRIVATE);
        LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
            return;
        }

        /*locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.i("$location-lat", String.valueOf(location.getLatitude()));
                Log.i("$location-lng", String.valueOf(location.getLongitude()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });*/

//        new MyMusicDatabase(this).increment_track(2);
//        new Radio().getRadioForArtist(3541666, new Callback<RadioCallback>() {
//            @Override
//            public void success(RadioCallback cb, Response response) {
//                Log.i("$callback", String.valueOf(cb.getRadio().size()));
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });

    }

    public SharedPreferences getPreferences() {
        return preferences;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NtertainLocation.REQUEST_CODE) {
            NtertainLocation mLocation = new NtertainLocation(this,preferences);
            if (mLocation.enableLocation())
                mLocation.getLastKnownLocation();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, MusicService.class), mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mService.isPlaying())
            mService.showNotification();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isBound && mService != null && mService.isNotificationShowing())
            mService.clearNotification();
        isPaused = false;
    }

    private void setupDrawer() {
        Drawer fragment = (Drawer) getSupportFragmentManager().findFragmentById(R.id.drawer_content);
        fragment.setUp(this, drawer, toolbar);
        fragment.setCallback(this);
    }

    private void onChangeContent(Fragment fragment, int res) {
        drawer.closeDrawer(GravityCompat.START);

        if (mCurrentFragment != null)
            getSupportFragmentManager().beginTransaction().remove(mCurrentFragment).commit();
        mCurrentFragment = fragment;
        getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
        title.setText(res);
    }

    @Override
    public void setup() {
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        search = menu.getItem(0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Utils.StartActivity(this, SearchActivity.class, null);
                break;
        }
        return false;
    }

    @Override
    public void onHome() {
        search.setVisible(true);
        onChangeContent(new HomeFragment(), R.string.home);


/*        new Geocode().geocode("289/242,MotiNagar,Lucknow", new Callback<GeocodeCallback>() {
            @Override
            public void success(GeocodeCallback result, Response response) {
                GeocodeResults.Location location = result.getGeocodeResults().get(0).getGeometry().getLocation();
//                Log.i("$location", );
                Toast.makeText(BaseActivity.this,String.format(Locale.ENGLISH, "latitude = %d & longitude = %d", location.getLat(),
                        location.getLng()),Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });*/

    }

    @Override
    public void onMusic() {
        search.setVisible(true);
        onChangeContent(new MusicFragment(), R.string.music);
    }

    @Override
    public void onNews() {
        search.setVisible(false);
        onChangeContent(new NewsActivity(), R.string.news_title);
    }

    @Override
    public void onMovies() {
        onChangeContent(new MoviesFragment(), R.string.movies);
    }

    @Override
    public void onRadio() {
        search.setVisible(false);
//        new CreatePlaylistDialog().show(getSupportFragmentManager(), "#create-playlist");
        onChangeContent(new RadioFragment(), R.string.radio);
    }

    @Override
    public void onCallertunez() {
        search.setVisible(false);
        onChangeContent(new MyCallerTunez(), R.string.my_callertunez);
    }

    @Override
    public void onMyMusic() {
        search.setVisible(false);
        startActivity(new Intent(this, MyMusicActivity.class));
    }

    @Override
    public void onSettings() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
    }

    @Override
    public void onSendSong() {
        onChangeContent(new SendSongFragment(), R.string.send_song);
    }
}
