package phantlab.ntertain.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.TourPager;
import phantlab.ntertain.app.application.Ntertain;
import phantlab.ntertain.app.callbacks.RevealListener;
import phantlab.ntertain.app.ui.PaginationView;
import phantlab.ntertain.app.ui.RevealBackgroundView;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;

public class ProductTour extends Screen {
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.reveal)
    RevealBackgroundView reveal;
    @Bind({R.id.signup, R.id.signin})
    List<Button> buttons;
    @Bind(R.id.pagination)
    PaginationView pagination;

    private Animation fade;

    @Override
    protected void onCreate(Bundle bundle) {
        onCreate();
        super.onCreate(bundle);
        if (((Ntertain) getApplication()).isSignedIn()) {
            startActivity(new Intent(ProductTour.this, InterstitialAd.class));
            finish();
            return;
        }
        setContentView(R.layout.tour_base);
    }

    @OnClick(R.id.signup)
    public void signup() {
        Utils.StartActivity(ProductTour.this, SignUpMailActivity.class);
    }

    @OnClick(R.id.signin)
    public void signin() {
        Utils.StartActivity(ProductTour.this, SignInActivity.class);
        finish();
    }

    private void reveal(View view, final RevealListener listener) {
        int[] tap = new int[2];
        view.getLocationOnScreen(tap);
        reveal.setVisibility(View.VISIBLE);
        reveal.startFromLocation(tap, new RevealBackgroundView.OnStateChangeListener() {
            @Override
            public void onStateChange(int state, final RevealBackgroundView reveal) {
                if (state == RevealBackgroundView.STATE_FINISHED) {
                    reveal.setToFinishedFrame();
                    fade.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {/***/}

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            reveal.setVisibility(View.GONE);
                            listener.onRevealed();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {/***/}
                    });
                    reveal.startAnimation(fade);
                }
            }
        });
    }

    @Override
    public void setup() {
        reveal.setFillPaintColor(getResources().getColor(R.color.colorAccent));
        pager.setAdapter(new TourPager(this));
        fade = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        pagination.setViewPager(pager);
        pagination.sync(0);
    }
}
