package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.Recent;
import phantlab.ntertain.app.api.TopTracks;
import phantlab.ntertain.app.api.model.RecentCallback;
import phantlab.ntertain.app.api.model.TrackCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.viewholders.LoadingHolder;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 22/12/15.
 */
public class TopTracksActivity extends Screen implements Paginate.Callbacks {

    public final static String KEY_RECENT = "recent";
    private final int ITEM_COUNT = 10;
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.recent_progress)
    Progress progress;
    TopArtistSongsAdapter adapter;
    TopTracks top;
    Object lock;
    private int start = 0;
    private boolean mIsLoading, isRecent;
    private LoadingListItemCreator creator = new LoadingListItemCreator() {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
            return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.loading, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (!mIsLoading)
                holder.itemView.setVisibility(View.GONE);
            else {
                holder.itemView.setVisibility(View.VISIBLE);
                ((Progress) holder.itemView.findViewById(R.id.progress)).start();
            }
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        Utils.setUpTransitions(getWindow());
        onCreate();
        super.onCreate(bundle);
        top = new TopTracks();
        setValues();
        setContentView(R.layout.recent_top_layout);
    }

    private void setValues() {
        Bundle data = getIntent().getExtras().getBundle("#data");
        if (data != null)
            isRecent = data.getBoolean(KEY_RECENT, false);
    }

    @Override
    public void setup() {
        title.setText(isRecent ? R.string.recent_tracks : R.string.top_tracks);
        setSupportActionBar(toolbar);
        disableIcon();
        setBackEnabled();
        adapter = new TopArtistSongsAdapter(this);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        if (!isRecent) {
            progress.setVisibility(View.GONE);
            adapter.setData(null, true);
            Paginate.with(list, TopTracksActivity.this).setLoadingListItemCreator(creator)
                    .setLoadingTriggerThreshold(10).build();
        } else {
            new Recent().getRecent(new Callback<RecentCallback>() {
                @Override
                public void success(RecentCallback recent, Response response) {
                    progress.setVisibility(View.GONE);
                    List<Tracks> recent_tracks = recent.getRecent();
                    Collections.shuffle(recent_tracks);
                    adapter.setData(recent_tracks, true);
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    @Override
    public void onLoadMore() {
        mIsLoading = true;
        if (lock == null) {
            lock = new Object();
            top.getTracks(ITEM_COUNT, start, new Callback<TrackCallback>() {
                @Override
                public void success(TrackCallback tracks, Response response) {
                    if (tracks.isSuccessFull()) {
                        List<Tracks> data = tracks.getData();
                        Collections.shuffle(data);
                        for (Tracks track : data)
                            adapter.addItem(track);
                        start += 10;
                    }

                    mIsLoading = false;
                    lock = null;
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    @Override
    public boolean isLoading() {
        return mIsLoading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return false;
    }
}
