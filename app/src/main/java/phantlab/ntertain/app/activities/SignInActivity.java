package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.UserEndpoint;
import phantlab.ntertain.app.api.model.UserCallback;
import phantlab.ntertain.app.application.Ntertain;
import phantlab.ntertain.app.application.UserUtils;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.utils.Validation;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 11/21/2015.
 */
public class SignInActivity extends Screen {
    @Bind({R.id.username, R.id.password})
    List<EditText> edits;
    @Bind(R.id.login)
    Button login;

    UserEndpoint user;

    Validation mValidation;

    @Override
    public void onCreate(Bundle bundle) {
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        onCreate();
        setContentView(R.layout.sign_in);
        user = new UserEndpoint();
        mValidation = Validation.getInstance();
        mValidation.add(edits.get(0), "Username", Validation.Type.EMPTY);
        mValidation.add(edits.get(1), "Password", Validation.Type.EMPTY);
    }

    @OnClick(R.id.login)
    public void onLogin() {
        if (mValidation.validate()) {
            login.setText("Logging in...");

            String username = edits.get(0).getText().toString(),
                    email = edits.get(1).getText().toString();


            user.login(username, email, new Callback<UserCallback>() {
                @Override
                public void success(UserCallback cb, Response response) {
                    login.setText(R.string.login);
                    if (cb.isSuccessFull()) {
                        UserUtils.saveUser(((Ntertain) getApplication()), cb.getUser().setToken(cb.getToken()));
                        Utils.StartActivity(SignInActivity.this, BaseActivity.class);
                        finish();
                    } else
                        Utils.showToast("Unable to login. ".concat(cb.getMessage()));

                }

                @Override
                public void failure(RetrofitError error) {
                    login.setText(R.string.login);
                    Utils.showToast("Something went wrong while signing you in.");
                }
            });
        }
    }

    @Override
    public void setup() {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mValidation.release();
    }

    @OnClick(R.id.account_status)
    public void onSignUp() {
        Utils.StartActivity(this, SignUpActivity.class);
    }
}
