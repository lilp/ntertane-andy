package phantlab.ntertain.app.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.Album;
import phantlab.ntertain.app.api.model.AlbumCallback;
import phantlab.ntertain.app.api.model.AlbumItem;
import phantlab.ntertain.app.api.model.Artist;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 17/12/15.
 */
public class AlbumDetails extends Screen {

    public final static String KEY_ALBUM_ID = "album_id";
    public final static String KEY_ALBUM = "album";
    public final static String KEY_ARTIST = "artist";
    public final static String KEY_TRACK_COUNT = "track_count";
    public final static String KEY_THUMBNAIL = "thumbnail";

    @Bind(R.id.thumbnail)
    SimpleDraweeView thumb;

    @Bind({android.R.id.title, R.id.playback, R.id.year, R.id.track_count})
    List<TextView> texts;

    @Bind(R.id.list)
    RecyclerView list;

    @Bind(R.id.album_loader)
    Progress progress;

    TopArtistSongsAdapter adapter;

    private String album_name, artist, thumbnail;
    private boolean isFromTrack;
    private int album_id, track_count;


    @Override
    public void onCreate(Bundle bundle) {
        onCreate();
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        setValues();
        setContentView(R.layout.album_details);
    }

    private void setValues() {
        Bundle data = getIntent().getExtras().getBundle("#data");
        if (data != null) {
            album_id = data.getInt(KEY_ALBUM_ID);
            album_name = data.getString(KEY_ALBUM);
            artist = data.getString(KEY_ARTIST);
            thumbnail = data.getString(KEY_THUMBNAIL);
            track_count = data.getInt(KEY_TRACK_COUNT);
            isFromTrack = data.getBoolean(ArtistActivity.KEY_IS_FROM_TRACK);
        }
    }

    @Override
    public void setup() {
        title.setText(album_name);
        disableIcon();
        setSupportActionBar(toolbar);
        setBackEnabled();
        setUpAlbumDetails();
    }

    private void setUpAlbumDetails() {
        adapter = new TopArtistSongsAdapter(this);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);
        setData();
        Album album = new Album();
        if (isFromTrack)
            album.getAlbumByName(album_name, new Callback<AlbumCallback>() {
                @Override
                public void success(AlbumCallback result, Response response) {
                    progress.setVisibility(View.GONE);
                    adapter.setData(result.getAlbum().getTracks(), false);
                    setUpValues(result.getAlbum());
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        else album.getAlbum(album_id, new Callback<AlbumCallback>() {
            @Override
            public void success(AlbumCallback result, Response response) {
                progress.setVisibility(View.GONE);
                adapter.setData(result.getAlbum().getTracks(), false);
            }

            @Override
            public void failure(RetrofitError error) {
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void setUpValues(AlbumItem album) {
        thumbnail = album.getCover();
        track_count = album.getTrackCount();
        setData();
    }

    public void setData(){
        texts.get(0).setText(album_name.concat(" - " + artist));
        texts.get(1).setText("1 hr 24m");
        texts.get(2).setText("2015");
        texts.get(3).setText(String.format("%d tracks", track_count));

        if (thumbnail != null)
            ThumbnailController.with(thumb).from(this).uri(Uri.parse(thumbnail)).blur(15);
    }
}
