package phantlab.ntertain.app.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.ui.processors.BlurPostprocessor;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class BuyActivity extends Screen {
    @Bind(R.id.blur)
    SimpleDraweeView blur;

    @Override
    public void onCreate(Bundle bundle) {
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        onCreate();
        setContentView(R.layout.buy_song);
    }

    @Override
    public void setup() {
        title.setText("Tupac back ft rick ross...");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        setImage();
    }

    private void setImage() {
        Uri uri = Uri.parse("http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png");
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setPostprocessor(new BlurPostprocessor(this, 25))
                .build();

        PipelineDraweeController controller = (PipelineDraweeController)
                Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(blur.getController())
                        .build();
        blur.setController(controller);
    }

//    Uri uri = Uri.parse("http://media.liveauctiongroup.net/i/12937/13079198_1.jpg");
//    int width  = Utils.convertDpToPixel(400),
//            height = Utils.convertDpToPixel(200);
//
//    new ThumbnailController().with(blur)
//    .dimens(width, height)
//    .uri(uri)
//    .load();
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            NavUtils.navigateUpFromSameTask(this);
        return true;
    }
}
