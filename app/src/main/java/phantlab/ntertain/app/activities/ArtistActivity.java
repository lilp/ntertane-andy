package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.Artists;
import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.api.model.ArtistItem;
import phantlab.ntertain.app.api.model.ArtistItemCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.fragments.ArtistAlbumsFragment;
import phantlab.ntertain.app.fragments.ArtistSongsFragment;
import phantlab.ntertain.app.fragments.ArtistTracksFragment;
import phantlab.ntertain.app.fragments.ComingSoonMovFragment;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class ArtistActivity extends Screen {

    public final static String KEY_ARTIST = "artist";
    public final static String KEY_ARTIST_ID = "artist_id";
    public final static String KEY_ALBUM_COUNT = "albums_count";
    public final static String KEY_SONG_COUNT = "song_count";
    public final static String KEY_THUMBNAIL = "thumbnail";
    public final static String KEY_IS_FROM_TRACK = "isSenderTrack";
    @Bind(R.id.tab_layout)
    TabLayout tab;
    @Bind(R.id.pager)
    ViewPager pager;
    private String artist, thumbnail;
    private boolean isFromTrack;

    private int album_count, tracks_count, id;

    private List<Albums> albums = new ArrayList<>();
    private List<Tracks> tracks = new ArrayList<>();
    private ArtistItem artist_info;
    private List<Fragment> fragments = new ArrayList<>();

    @Override
    public void onCreate(Bundle bundle) {
        onCreate();
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        setValues();
        setContentView(R.layout.artist_base);

        fragments.add(new ArtistTracksFragment());
        fragments.add(new ArtistSongsFragment());
        fragments.add(new ArtistAlbumsFragment());
        fragments.add(new ComingSoonMovFragment());

        if (isFromTrack)
            new Artists().getArtistByName(artist, new Callback<ArtistItemCallback>() {
                @Override
                public void success(ArtistItemCallback callback, Response response) {
                    albums = callback.getAlbums();
                    tracks = callback.getTracks();
                    artist_info = callback.getArtist();
                    ((ArtistTracksFragment) fragments.get(0)).notifyCount();
                    ((ArtistSongsFragment) fragments.get(1)).notifyData();
                }

                @Override
                public void failure(RetrofitError error) {/***/}
            });
        else new Artists().getArtist(id, new Callback<ArtistItemCallback>() {
            @Override
            public void success(ArtistItemCallback callback, Response response) {
                albums = callback.getAlbums();
                tracks = callback.getTracks();
                ((ArtistSongsFragment) fragments.get(1)).notifyData();
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public List<Tracks> getTracks() {
        return tracks;
    }

    public List<Albums> getAlbums() {
        return albums;
    }

    public ArtistItem getArtistInfo() {
        return artist_info;
    }

    private void setValues() {
        Bundle data = getIntent().getExtras().getBundle("#data");
        if (data != null) {
            artist = data.getString(KEY_ARTIST);
            thumbnail = data.getString(KEY_THUMBNAIL);
            id = data.getInt(KEY_ARTIST_ID);
            album_count = data.getInt(KEY_ALBUM_COUNT);
            tracks_count = data.getInt(KEY_SONG_COUNT);
            isFromTrack = data.getBoolean(KEY_IS_FROM_TRACK, false);
        }
    }

    @Override
    public void setup() {
        title.setText(artist);
        disableIcon();
        setSupportActionBar(toolbar);
        setBackEnabled();

        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Bundle data = new Bundle();
                data.putString(KEY_ARTIST, artist);
                if (position == 0) {
                    data.putString(KEY_THUMBNAIL, thumbnail);
                    data.putInt(KEY_ALBUM_COUNT, album_count);
                    data.putInt(KEY_SONG_COUNT, tracks_count);
                    data.putBoolean(KEY_IS_FROM_TRACK, isFromTrack);
                    data.putInt(KEY_ARTIST_ID, id);
                    fragments.get(0).setArguments(data);
                } else if (position == 2)
                    fragments.get(2).setArguments(data);
                return fragments.get(position);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                if (position == 0)
                    return "TOP TRACKS";
                else if (position == 1)
                    return "SONGS";
                return "ALBUMS";
            }

            @Override
            public int getCount() {
                return 3;
            }
        });


        tab.setupWithViewPager(pager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.artist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
