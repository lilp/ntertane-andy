package phantlab.ntertain.app.activities;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.AsyncTaskCallbacks;
import phantlab.ntertain.app.news.News;
import phantlab.ntertain.app.news.NewsUtils;
import phantlab.ntertain.app.news.dompuller.DomPuller;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/29/2015.
 */
public class NewsDetails extends Screen {
    public static final String TITLE = "#title";
    public static final String LINK = "#link";
    public static final String TYPE = "#type";
    public static final String TIME = "#timestamp";
    public static final String ENCODED = "#encoded";
    @Bind(R.id.web)
    WebView mWebview;
    @Bind(R.id.web_progress)
    Progress progress;
    @Bind({R.id.title, android.R.id.title, android.R.id.summary})
    List<TextView> mNewsTitleView;
    @Bind({R.id.thumb, R.id.news_thumb})
    List<SimpleDraweeView> mNewsThumb;
    Handler handler = new Handler();
    Bundle data;
    private String mNewsTitle, link, timestamp, thumbnail,
            fallback = "https://d13yacurqjgara.cloudfront.net/users/230779/screenshots/1383933/dribble_1_1x.jpg";
    private News type;
    private int width = Utils.convertDpToPixel(28),
            height = Utils.convertDpToPixel(170),
            full_width = Utils.convertDpToPixel(400);

    @Override
    public void setup() {
        data = getIntent().getExtras().getBundle("#data");
        setUpVals();
        setUpWebView();
        title.setText(R.string.news_title);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        setDetails();
        DomPuller puller = new DomPuller(this, link, type).setName(mNewsTitle.trim());
        puller.setCallbacks(new AsyncTaskCallbacks() {
            @Override
            public void onPostExecute(final String uri) {
                Looper.prepare();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mWebview.loadUrl(uri);
                    }
                });
            }
        });
        new Thread(puller).start();
    }

    private void setDetails() {
        mNewsTitleView.get(0).setText(mNewsTitle);
        mNewsTitleView.get(1).setText(type.name());
        mNewsTitleView.get(2).setText(timestamp);

        ThumbnailController.with(mNewsThumb.get(1)).uri(Uri.parse(NewsUtils.get().get(type)))
                .dimens(width, width)
                .load();
    }

    private void setUpVals() {
        if (data != null) {
            mNewsTitle = data.getString(TITLE);
            link = data.getString(LINK);
            timestamp = data.getString(TIME);
            type = (News) data.getSerializable(TYPE);

            String encoded = data.getString(ENCODED);

            if (encoded != null) {
                Element image = Jsoup.parse(encoded).select("img").first();
                if (image != null)
                    thumbnail = image.attr("src");
            }

            ThumbnailController.with(mNewsThumb.get(0))
                    .uri(Uri.parse(thumbnail != null ? thumbnail : fallback))
                    .dimens(full_width, height)
                    .load();
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        onCreate();
        setContentView(R.layout.news_details);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_details, menu);
        return true;
    }

    private void setUpWebView() {
       /* if (Build.VERSION.SDK_INT >= 19)
            mWebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        else
            mWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
*/
        mWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(NewsDetails.this, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (progress.getVisibility() == View.GONE)
                    progress.setVisibility(View.VISIBLE);
                progress.start();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progress.stop();
                progress.setVisibility(View.GONE);
            }
        });
    }
}
