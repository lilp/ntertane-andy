package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.dialogs.CreatePlaylistDialog;
import phantlab.ntertain.app.dialogs.DialogCallback;
import phantlab.ntertain.app.fragments.MyAlbumFragment;
import phantlab.ntertain.app.fragments.MyArtistFragment;
import phantlab.ntertain.app.fragments.MyPlaylistFragment;
import phantlab.ntertain.app.fragments.MySongsFragment;
import phantlab.ntertain.app.ui.Screen;

/**
 * Created by kartikeyasingh on 19/12/15.
 **/
public class MyMusicActivity extends Screen {

    @Bind(R.id.pager)
    ViewPager pager;

    @Bind(R.id.tab_layout)
    TabLayout tabs;

    MenuItem item;

    private int playlist_id = -1;

    public void setPlaylistId(int playlist_id) {
        this.playlist_id = playlist_id;
    }

    private List<Fragment> fragments;


    @Override
    public void onCreate(Bundle bundle) {
        onCreate();
        super.onCreate(bundle);
        setContentView(R.layout.my_music);
        fragments = new ArrayList<>();
        fragments.add(new MyPlaylistFragment());
        fragments.add(new MySongsFragment());
        fragments.add(new MyAlbumFragment());
        fragments.add(new MyArtistFragment());
    }

    @Override
    public void setup() {
        title.setText(R.string.my_music);
        setSupportActionBar(toolbar);
        setBackEnabled();

        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            String[] titles = {"PLAYLIST", "SONGS", "ALBUM", "ARTIST"};

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }

            @Override
            public int getCount() {
                return titles.length;
            }
        });

        tabs.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position > 0)
                    item.setVisible(false);
                else item.setVisible(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymusic, menu);
        item = menu.getItem(0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_playlist:
                showDialog();
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void showDialog() {
        final CreatePlaylistDialog dialog = new CreatePlaylistDialog();
        dialog.setCallback(new DialogCallback() {
            @Override
            public void onPositiveClick() {
                dialog.dismiss();
                ((MyPlaylistFragment) fragments.get(0)).refresh();
            }
        }).show(getSupportFragmentManager(), "$playlist");
    }

    @Override
    public void onBackPressed() {
        if (playlist_id != -1) {
            ((MyPlaylistFragment) fragments.get(0)).refresh();
            playlist_id = -1;
        } else super.onBackPressed();
    }
}
