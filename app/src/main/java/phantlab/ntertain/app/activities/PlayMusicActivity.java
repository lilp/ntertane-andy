package phantlab.ntertain.app.activities;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.squareup.otto.Subscribe;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.bus.BusProvider;
import phantlab.ntertain.app.bus.PlayBackStateChangedEvent;
import phantlab.ntertain.app.databases.MyMusicDatabase;
import phantlab.ntertain.app.dialogs.NoMusicDialog;
import phantlab.ntertain.app.music.MusicService;
import phantlab.ntertain.app.music.NtertainPlayer;
import phantlab.ntertain.app.ui.NtertainMenu;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 08/12/15.
 */
public class PlayMusicActivity extends Screen implements ExoPlayer.Listener,
        SeekBar.OnSeekBarChangeListener, AudioManager.OnAudioFocusChangeListener {

    public final static String ACTION_VIEW_SONG = "view_song";
    public final static String KEY_ALBUM = "album";
    public final static String KEY_NAME = "track";
    public final static String KEY_ARTIST = "artist";
    public final static String KEY_THUMBNAIL = "thumbnail";
    public final static String KEY_URL = "url";
    private final int DELAY = 1000;
    NtertainPlayer player;
    @Bind(R.id.blur)
    SimpleDraweeView thumbs;
    @Bind({R.id.title_ntertain, R.id.artist_title, R.id.elapsed, R.id.total})
    List<TextView> texts;
    @Bind(R.id.player_seek)
    AppCompatSeekBar seek;
    @Bind({R.id.play, R.id.repeat})
    List<ImageButton> actions;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.menu)
    NtertainMenu menu;

    @Bind(R.id.overlay)
    View overlay;

    MyMusicDatabase db;


    private boolean isView;
    private Handler handler;
    private String track, album_name, artist, thumbnail, url;
    private Runnable elapsedTask = new Runnable() {
        @Override
        public void run() {
            int progress = player.getPosition();
            seek.setProgress(progress);
            texts.get(2).setText(Utils.format(progress));
            handler.postDelayed(this, DELAY);
        }
    };

    private MusicService mPlayService;
    private boolean isBound = false;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mPlayService = ((MusicService.MusicServiceBinder) iBinder).getService();
            if (!isView)
                mPlayService.play(new Tracks(track, thumbnail, album_name, new String[]{artist}, url, 0L, ""));
            mPlayService.setListener(PlayMusicActivity.this);
            actions.get(0).setImageResource(R.drawable.ic_pause);
            actions.get(0).setEnabled(false);
            seek.setOnSeekBarChangeListener(PlayMusicActivity.this);
            player = mPlayService.getPlayer();
            isBound = true;

            if (isView) {
                Tracks current_track = mPlayService.getTrack();
                setValues(current_track);
                int duration = player.getControl().getDuration();
                seek.setMax(duration);
                seek.setProgress(player.getPosition());
                texts.get(3).setText(Utils.format(duration));
                handler.postDelayed(elapsedTask, DELAY);
                if (!mPlayService.isPlaying())
                    actions.get(0).setImageResource(R.drawable.ic_play);
                else
                    actions.get(0).setImageResource(R.drawable.ic_pause);

                if (!actions.get(0).isEnabled())
                    actions.get(0).setEnabled(true);
                ThumbnailPagerAdapter adapter = new ThumbnailPagerAdapter();
                pager.setAdapter(adapter);
                adapter.setData(mPlayService.getPlayingTracks());
                pager.setCurrentItem(mPlayService.getOffset());
                mPlayService.setViewPager(pager);
                setUpPlayer();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i("$onServiceConnecte", "failed");
            mPlayService = null;
            isBound = false;
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getAction() != null)
            if (getIntent().getAction().equals(ACTION_VIEW_SONG))
                overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        if (getIntent().getAction() != null)
            isView = getIntent().getAction().equals(ACTION_VIEW_SONG);
        else isView = false;

        if (!isView)
            Utils.setUpTransitions(getWindow());
        onCreate();
        super.onCreate(bundle);
        BusProvider.getInstance().register(this);
//        setValues(null);
        bindService(new Intent(this, MusicService.class), mConnection, BIND_AUTO_CREATE);
        setContentView(R.layout.play);
        menu.setSender(this);
        menu.setOverlay(overlay);

        db = new MyMusicDatabase(this);
        handler = new Handler();
    }

    @Override
    public void setup() {
        setSupportActionBar(toolbar);
        setBackEnabled();
        if (!isView)
            setUpPlayer();
    }

    private void setUpPlayer() {
        Uri uri = Uri.parse(thumbnail);
        texts.get(0).setText(track);
        texts.get(1).setText(artist);

        texts.get(0).setSelected(true);
        ThumbnailController.with(thumbs).from(this)
                .uri(uri).blur(22);

        menu.setService(mPlayService);

        if (url == null) {
            final NoMusicDialog dialog = new NoMusicDialog();
            dialog.setListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface di, int positions) {
                    dialog.hide();
                    finish();
                }
            }).show(getSupportFragmentManager(), "$no_music");
        } else {
//            player = new NtertainPlayer();
//            player.init(this);
//            player.setListener(this);
//            player.stream(Uri.parse(url.replaceAll(" ", "%20")));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.play_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.favourite_action) {
            db.add_fav(mPlayService.getTrack());
            Toast.makeText(PlayMusicActivity.this, String.format(Locale.ENGLISH,
                    "%s added to favourites", track), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        Log.i("$state", "isPlayWhenReady = " + playWhenReady + " & state= " + playbackState);

        if (playbackState == ExoPlayer.STATE_READY) {
            long duration = NtertainPlayer.getPlayer().getDuration();
            seek.setMax((int) duration);
            handler.postDelayed(elapsedTask, DELAY);
            texts.get(3).setText(Utils.format(duration));
            if (!actions.get(0).isEnabled())
                actions.get(0).setEnabled(true);
        }
    }

    @Override
    public void onPlayWhenReadyCommitted() {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.i("$error", error.getMessage());
    }

    private void setValues(Tracks mTrack) {
        if (!isView) {
            Bundle data = getIntent().getExtras().getBundle("#data");
            if (data != null) {
                track = data.getString(KEY_NAME);
                album_name = data.getString(KEY_ALBUM);
                artist = data.getString(KEY_ARTIST);
                thumbnail = data.getString(KEY_THUMBNAIL);
                url = data.getString(KEY_URL);
            }
        } else {
            if (mTrack != null) {
                menu.setTrack(mTrack);
                thumbnail = mTrack.getThumbnail();
                artist = mTrack.getArtists()[0];
                track = mTrack.getTrack();
                url = mTrack.getUrl();
                album_name = mTrack.getAlbum();
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seek, int position, boolean fromUser) {
        if (fromUser)
            player.seekTo(position);

        if ((seek.getMax() / 2 - position) <= 100)
            mPlayService.pause();

        Log.i("$onProgressChanged", "position = " + position + "  %/2 =  " + seek.getMax() / 2);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seek) {
        handler.removeCallbacks(elapsedTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seek) {
        handler.postDelayed(elapsedTask, DELAY);
    }

    @OnClick(R.id.play)
    public void onPlay() {
        ImageButton play = actions.get(0);
        if (!mPlayService.isPlaying())
            play.setImageResource(R.drawable.ic_pause);
        else
            play.setImageResource(R.drawable.ic_play);
        Intent serviceIntent = new Intent(MusicService.ACTION_PLAYPAUSE, null, this,
                MusicService.class);
        startService(serviceIntent);
    }

    @OnClick(R.id.next)
    public void next() {
        mPlayService.next();
    }

    @OnClick(R.id.previous)
    public void previous() {
        mPlayService.previous();
    }


    @OnClick(R.id.shuffle)
    public void shuffle() {
        mPlayService.shuffle();
    }


    @OnClick(R.id.repeat)
    public void onRepeat() {
        ImageButton repeat = actions.get(1);
        if (!mPlayService.isRepeating()) {
            repeat.setColorFilter(getResources().getColor(R.color.colorAccent));
            mPlayService.setRepeating(true);
        } else {
            repeat.setColorFilter(null);
            mPlayService.setRepeating(false);
        }
    }

    @Subscribe
    public void onPlayBackStateChanged(PlayBackStateChangedEvent event) {
        Tracks track = event.getTrack();
        if (track != null) {
            switch (event.getState()) {
                case NewSongPlaying:
                    setValues(track);
                    setUpPlayer();
                    break;
                case Playing:
                    actions.get(0).setImageResource(R.drawable.ic_pause);
                    break;
                case Stop:
                    actions.get(0).setImageResource(R.drawable.ic_play);
                    break;
                case Paused:
                    actions.get(0).setImageResource(R.drawable.ic_play);
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        startService(new Intent(this, MusicService.class));
        BusProvider.getInstance().unregister(this);
        unbindService(mConnection);
    }

    @Override
    public void onAudioFocusChange(int focus) {
        Log.i("$onAudioFocusChange", "focus = " + focus);
    }

    class ThumbnailPagerAdapter extends PagerAdapter {
        List<Tracks> data;
        LayoutInflater inflater;
        @Bind(R.id.thumbnail)
        SimpleDraweeView thumbnail;

        public ThumbnailPagerAdapter() {
            inflater = LayoutInflater.from(PlayMusicActivity.this);
        }

        public void setData(List<Tracks> data) {
            this.data = data;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return data != null ? data.size() : 0;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(R.layout.play_pager_item, container, false);
            ButterKnife.bind(this, view);
            int width = Utils.convertDpToPixel(225);
            Tracks track = data.get(position);
            ThumbnailController.with(thumbnail).uri(Uri.parse(track.getThumbnail()))
                    .dimens(width, width).load();
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}