package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.fragments.MoviesDiscussion;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 4/10/2016.
 */
public class NewsComment extends Screen {

    String link;

    public final static String KEY_LINK = "$link";

    @Override
    public void onCreate(Bundle bundle) {
        onCreate();
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        setValues();
        setContentView(R.layout.album_details);
    }

    private void setValues() {
        Bundle data = getIntent().getExtras().getBundle("$data");
        if (data != null)
            link = data.getString(KEY_LINK);
    }

    @Override
    public void setup() {
        MoviesDiscussion fragment =
                (MoviesDiscussion) getSupportFragmentManager().findFragmentById(R.id.news_comment);
        fragment.setMeta(new MovieMeta("","","","","",link,"",""));
    }
}
