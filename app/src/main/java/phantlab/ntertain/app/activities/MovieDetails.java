package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.fragments.MoviesDetailsFragment;
import phantlab.ntertain.app.fragments.MoviesDiscussion;
import phantlab.ntertain.app.fragments.ShowingAtFragment;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/28/2015.
 */
public class MovieDetails extends Screen {
    private String movie_title, genre, plot, thumb, video, link, rating, playback;
    public static final String TITLE  = "#title";
    public static final String GENRE  = "#genre";
    public static final String PLOT   = "#plot";
    public static final String THUMB  = "#thumb";
    public static final String VIDEO  = "#video";
    public static final String RATING = "#rating";
    public static final String LINK = "#link";
    public static final String PLAY = "#playback";

    private MovieMeta movie;
    @Bind(R.id.tab_layout)
    TabLayout tab;

    @Bind(R.id.pager)
    ViewPager pager;

    public MovieDetails() {/***/}

    @Override
    public void setup() {
        setSupportActionBar(toolbar);
        title.setText(movie_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        tab.addTab(tab.newTab().setText("Now Playing"));
        tab.addTab(tab.newTab().setText("Comming Soon"));
        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return position == 0 ? new MoviesDetailsFragment().setMeta(movie) :
                        (position == 1 ?  new ShowingAtFragment().setLink(link) :
                                new MoviesDiscussion().setMeta(movie));
            }

            @Override
            public CharSequence getPageTitle(int position) {
                if (position == 0)
                    return "DETAILS";
                else if (position == 1)
                    return "SHOWING AT";
                return "DISCUSSION";
            }

            @Override
            public int getCount() {
                return 3;
            }
        });
        tab.setupWithViewPager(pager);
    }

    @Override
    public void onCreate(Bundle bundle) {
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        onCreate();
        initValues();
        setContentView(R.layout.movies_details);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return true;
    }

    private void initValues() {
        Bundle data = getIntent().getExtras().getBundle("#data");
        if (data != null) {
            movie_title = data.getString(TITLE);
            link = data.getString(LINK);
            genre = data.getString(GENRE);
            video = data.getString(VIDEO);
            plot = data.getString(PLOT);
            thumb = data.getString(THUMB);
            playback = data.getString(PLAY);
            rating = data.getString(RATING);
            movie = new MovieMeta(movie_title, genre, plot, thumb, video, link, playback, rating);
        }
    }
}
