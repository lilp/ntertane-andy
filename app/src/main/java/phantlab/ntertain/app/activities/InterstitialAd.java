package phantlab.ntertain.app.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.Ads;
import phantlab.ntertain.app.api.model.AdCallback;
import phantlab.ntertain.app.api.model.AdItem;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 4/3/2016.
 */
public class InterstitialAd extends Screen {

    @Bind(R.id.ad)
    SimpleDraweeView ad;
    Ads ads = new Ads();

    @Override
    public void onCreate(Bundle bundle) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(bundle);
        setContentView(R.layout.interstitial_ad);
    }

    @Override
    public void setup() {
        ads.getAds(new Callback<AdCallback>() {
            @Override
            public void success(AdCallback cb, Response response) {
                if (!cb.isSuccessful()) {
                    startBaseActivity();
                    return;
                }

                if (cb.getAd().isEmpty()) {
                    startBaseActivity();
                    return;
                }

                ArrayList<AdItem> ads = cb.getAd();
                final AdItem interstitial = ads.get(Utils.randInt(0, ads.size() - 1));
                ad.setImageURI(Uri.parse(interstitial.getThumbnail()));
                ad.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View drawee) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(interstitial.getLink())));
                        startBaseActivity();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                startBaseActivity();
            }
        });
    }

    @OnClick(R.id.close_ad)
    public void onClose() {
        startBaseActivity();
    }


    void startBaseActivity() {
        startActivity(new Intent(InterstitialAd.this, BaseActivity.class));
        finish();
    }
}
