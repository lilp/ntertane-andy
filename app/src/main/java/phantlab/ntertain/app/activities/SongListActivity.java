package phantlab.ntertain.app.activities;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.SongListAdapter;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 06/12/15.
 */
public class SongListActivity extends Screen {
    @Bind(R.id.list)
    RecyclerView list;

    SongListAdapter adapter;

    @Override
    public void onCreate(Bundle bundle) {
        Utils.setUpTransitions(getWindow());
        super.onCreate(bundle);
        onCreate();
        setContentView(R.layout.song_list);
    }

    @Override
    public void setup() {
        title.setText("Category");
        setSupportActionBar(toolbar);
        setBackEnabled();
        adapter = new SongListAdapter(this, false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            NavUtils.navigateUpFromSameTask(this);
        return true;
    }
}
