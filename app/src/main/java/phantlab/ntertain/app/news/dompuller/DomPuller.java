package phantlab.ntertain.app.news.dompuller;

import android.app.Activity;
import android.net.Uri;
import android.os.Environment;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import phantlab.ntertain.app.callbacks.AsyncTaskCallbacks;
import phantlab.ntertain.app.foundation.Cache;
import phantlab.ntertain.app.movies.MoviesEngine;
import phantlab.ntertain.app.news.News;

public class DomPuller implements Runnable {
    String url, name;
    Activity activity;
    News site;

    AsyncTaskCallbacks callbacks;


    public DomPuller(Activity activity, String url, News site) {
        this.url = url;
        this.activity = activity;
        this.site = site;
    }

    public DomPuller setName(String name) {
        this.name = name;
        return this;
    }

    public void setCallbacks(AsyncTaskCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void run() {
        File file = null, dir = new File(Environment.getExternalStorageDirectory(), "News");
//                dir = Utils.getNewsDataDir();
        if (dir != null)
            if (!dir.exists()) {
                boolean ignored = dir.mkdir();
            }

        file = new File(dir, name + ".html");
        String result = Uri.fromFile(file).toString();

        if (file.exists() && file.length() > 0) {
            if (callbacks != null)
                callbacks.onPostExecute(result);
        } else {
            try {
                file.createNewFile();
                Element mBodyElm = Jsoup.connect(url.trim())
                        .header("Content-Encoding", "gzip")
                        .userAgent(MoviesEngine.AGENT)
                        .header("Content-Type", "application/javascript")
                        .get().body();

                String body = null;

                if (site != null) {
                    if (site.equals(News.PUNCHNG)) {
                        Elements elements = mBodyElm.select("div.td-post-content");
                        elements.select("div.td-post-featured-image").remove();
                        elements.select("div.td-a-rec.td-a-rec-id-content_bottom").remove();
                        body = elements.outerHtml();
                    } else if (site.equals(News.PULSE))
                        body = mBodyElm.select("div.article_text").select("div.vspace").outerHtml();
                    else if (site.equals(News.NOBS))
                        body = mBodyElm.select(".post-content-wrap").outerHtml();
                    else if (site.equals(News.NOTJUSTOK)) {
                        Elements elements = mBodyElm.select("div.entry").removeClass("wp-caption");
                        body = elements.outerHtml();
                    } else if (site.equals(News.YNAIJA)){
                        Elements elements = mBodyElm.select("div.td-post-content")
                                .removeClass("td-a-rec");
                        body = elements.outerHtml();
                    } else if (site.equals(News.BELLANAIJA)){
                        Elements elements = mBodyElm.select("div.entry-content")
                                .removeClass("author-box");
                        body = elements.outerHtml();
                    } else if (site.equals(News.INFONG)){
                        Elements elements = mBodyElm.select("div.entry");
                        elements.select("form").remove();
                        elements.select("div#MarketGidScriptRootC28765").remove();
                        elements.select("div.OUTBRAIN").remove();
                        body = elements.outerHtml();
                    } else if (site.equals(News.NAIJALOADED)){
                        Elements elements = mBodyElm.select("div.post-content");
                        elements.select("div[align=center]").remove();
                        elements.select("div").last().remove();
                        elements.select("p[align=left]").remove();
                        body = elements.outerHtml();
                    }

                    FileOutputStream fop = new FileOutputStream(file);
                    byte[] contentInBytes = ("<html>" + "<head>" + "<style>" + new Cache(activity).getCss() +
                            "</style>" + "</head>" + "<body>" + "<div>" + body + "</div>" + "</body>" + "</html>").getBytes();

                    fop.write(contentInBytes);
                    fop.flush();
                    fop.close();
                    if (callbacks != null)
                        callbacks.onPostExecute(result);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}