package phantlab.ntertain.app.news.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import java.util.List;

@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2005/Atom", prefix = "atom"),
        @Namespace(reference = "http://search.yahoo.com/mrss/", prefix = "media"),
        @Namespace(reference = "http://purl.org/rss/1.0/modules/content/", prefix = "content")
})
@Root(strict = false)
public class Channel {
    @ElementList(entry = "link", inline = true, required = false)
    public List<Link> links;

    @ElementList(name = "item", required = true, inline = true)
    public List<Item> itemList;


    public List<Item> getItemList() {
        return itemList;
    }

    @Element
    String title;
    @Element
    String language;

    @Element(name = "ttl", required = false)
    int ttl;

    @Element(name = "pubDate", required = false)
    String pubDate;

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "links=" + links +
                ", itemList=" + itemList +
                ", title='" + title + '\'' +
                ", language='" + language + '\'' +
                ", ttl=" + ttl +
                ", pubDate='" + pubDate + '\'' +
                '}';
    }

    public static class Link {
        @Attribute(required = false)
        public String href;

        @Attribute(required = false)
        public String rel;

        @Attribute(name = "type", required = false)
        public String contentType;


        @Text(required = false)
        public String link;

        @Attribute(required = false)
        public String title;
    }

    @Root(name = "entry", strict = false)
    public static class Entry {
        @Element(name = "title", required = false)
        @Text
        String title;

        @Element(name = "content", required = false)
        @Text
        String content;

        @Element(name = "id", required = false)
        @Text
        String link;

        public String getLink() {
            return link;
        }

        public String getContent() {
            return content;
        }

        public String getTitle() {
            return title;
        }
    }

    @Root(name = "item", strict = false)
    public static class Item {

        @Element(name = "title", required = true)
        String title;
        @Element(name = "link", required = true)
        String link;

        @Element(name = "description", required = false, data = true)
        String description;
        @Element(name = "author", required = false)
        String author;
        @ElementList(name = "category", inline = true, data = true, required = false)
        List<Category> category;
        @Element(name = "guid", required = false)
        String guid;
        @Element(name = "pubDate", required = false)
        String pubDate;
        @Element(name = "source", required = false)
        String source;


        public String getPubDate() {
            return pubDate;
        }

        @Namespace(prefix = "media")
        @ElementList(name = "thumbnail", inline = true, required = false, type = Thumbnail.class)
        List<Thumbnail> thumbnail;

        public String getDescription() {
            return description;
        }

        @Namespace(prefix = "content")
        @Element(name = "encoded", required = false, data = true)
        String encodedContent;

        /*@Namespace(prefix = "media")
        @Element(name = "content", required = false, type = Content.class)
        Content content;

        public Content getContent() {
            return content;
        }*/

        public List<Category> getCategory() {
            return category;
        }

        public String getEncodedContent() {
            return encodedContent;
        }

        public List<Thumbnail> getThumb() {
            return thumbnail;
        }

        public String getTitle() {
            return title;
        }

        public String getLink() {
            return link;
        }
    }

    public static class Category {
        @Text(data = true, required = false)
        String data;
        @Attribute(name = "domain", required = false)
        String domain;

        public Category setData(String data) {
            this.data = data;
            return this;
        }

        public String getData() {
            return data;
        }
    }

    /* @Root(strict = false)
     public static class Content {
         @Attribute(name = "url", required = false)
         String url;

         @Attribute(name = "type", required = false)
         String type;

         @Attribute(name = "width", required = false)
         int width;

         @Attribute(name = "isDefault", required = false)
         boolean isDefault;

         @Attribute(name = "height", required = false)
         int height;

         @Attribute(name = "medium", required = false)
         String medium;

         @Namespace(prefix = "media")
         @Element(required = false, name = "title")
         Title title;

         public String getUrl() {
             return url;
         }
     }
 */
    public static class Title {
        @Attribute(name = "type", required = false)
        String type;
    }

    public static class Thumbnail {
        public Thumbnail() {
        }

        @Attribute(name = "height", required = false)
        int height;

        @Attribute(name = "width", required = false)
        int width;

        @Attribute(name = "url", required = false)
        String url;

        @Attribute(name = "src", required = false)
        String src;

        public int getWidth() {
            return width;
        }

        public String getSrc() {
            return src;
        }

        public String getUrl() {
            return url;
        }
    }
}