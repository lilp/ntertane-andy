package phantlab.ntertain.app.news;

import retrofit.RestAdapter;
import retrofit.converter.SimpleXMLConverter;

public class Factory {

    public static class RetrofitBuilder {
        public RestAdapter Build(String endpoint) {
            return new RestAdapter.Builder()
                    .setEndpoint(endpoint)
                    .setLogLevel(RestAdapter.LogLevel.BASIC)
                    .setConverter(new SimpleXMLConverter())
                    .build();
        }
    }
}
