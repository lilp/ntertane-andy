package phantlab.ntertain.app.news.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(strict = false)
public class RSS {

    @Attribute(required = false)
    String version;

    @Element(required = false)
    Channel channel;

    @ElementList(name = "entries", required = false, inline = true)
    public List<Channel.Entry> entries;

    public List<Channel.Entry> getEntries() {
        return entries;
    }

    @Element(name = "title", required = false)
    String title;
    @Attribute(required = false)
    String lang;

    public Channel getChannel() {
        return channel;
    }

    @Override
    public String toString() {
        return "RSS{" +
                "version='" + version + '\'' +
                ", channel=" + channel +
                '}';
    }
}