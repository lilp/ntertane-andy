package phantlab.ntertain.app.news;


import android.app.Activity;

import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.List;

import phantlab.ntertain.app.news.dompuller.DomPuller;
import phantlab.ntertain.app.news.dompuller.Executor;
import phantlab.ntertain.app.news.models.Channel;
import phantlab.ntertain.app.news.models.RSS;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.GET;

public class NewsFeed {

    private final static String PUNCHNG = "http://punchng.com";
    private final static String NAIJLOADED = "http://naijaloaded.com.ng";
    private final static String RECODE = "http://www.recode.net";
    private final static String SIXTY_NOBS = "http://www.360nobs.com";
    private final static String NOTJUSTOK = "http://notjustok.com";
    private final static String INFONG = "http://www.informationng.com";
    private final static String BELLA = "http://www.bellanaija.com";
    private final static String NEWSAFRICA = "http://newsafrica.com";
    private final static String YNAIJA = "http://ynaija.com";
    private final static String PULSE = "http://pulse.ng";

    public NewsFeed() { /***/}

    public NewsFeed(final Activity sender, final News type, final NewsResult cb,
                    final boolean is_featured) {

        String Endpoint = "";

        switch (type) {
            case PUNCHNG:
                Endpoint = PUNCHNG;
                break;
            case NAIJALOADED:
                Endpoint = NAIJLOADED;
                break;
            case NOBS:
                Endpoint = SIXTY_NOBS;
                break;
            case NOTJUSTOK:
                Endpoint = NOTJUSTOK;
                break;
            case BELLANAIJA:
                Endpoint = BELLA;
                break;
            case NEWSAFRICA:
                Endpoint = NEWSAFRICA;
                break;
            case YNAIJA:
                Endpoint = YNAIJA;
                break;
            case INFONG:
                Endpoint = INFONG;
                break;
            case PULSE:
                Endpoint = PULSE;
                break;
        }

        FeedService mNewsService = new Factory.RetrofitBuilder().Build(Endpoint).create(FeedService.class);
        Callback<RSS> callback = new Callback<RSS>() {
            @Override
            public void success(RSS tech, Response response) {
                add(sender, tech, type, cb, is_featured);
            }

            @Override
            public void failure(RetrofitError error) {
                cb.onError(error);
            }
        };
        if (type == News.PULSE)
            mNewsService.getRss(callback);
        else
            mNewsService.getFeed(callback);

    }

    public void getFeaturedNews(final FeaturedResult result) {
        List<News> news = new ArrayList<>();
        news.add(News.PULSE);
//        news.add(News.NAIJALOADED);
        final List<NewsItem> res = new ArrayList<>();
        for (final News child : news)
            new NewsFeed(null, child, new NewsResult() {
                @Override
                public void onSuccess(List<NewsItem> items) {
                    for (NewsItem item : items) {
                        try {
                            String src = Jsoup.parse(item.getThumbnail()).select("img")
                                    .first().attr("src");
                            String subtitle = Jsoup.parse(item.getThumbnail()).select("p").first().text();
                            item.setSubtitle(subtitle.replace("&#039;","'"));
                            item.setThumbnail(src);
                            res.add(item);
                        } catch (Exception e) {e.printStackTrace();}
                    }

//                    if (child.equals(News.NAIJALOADED))
                        result.onSuccess(res);
                }

                @Override
                public void onError(RetrofitError error) {
                    result.onSuccess(res);
                }
            }, true);
    }

    private void add(Activity sender, RSS feed, News type, NewsResult cb, boolean is_featured) {
        List<Channel.Item> items = feed.getChannel().getItemList();
        List<NewsItem> result = new ArrayList<>();
        if (items != null) {
            for (int i = 0; i < (is_featured ? 4 : items.size()); i++) {
                Channel.Item item = items.get(i);
                NewsItem mNewsItem = new NewsItem(item.getTitle(), item.getEncodedContent(), item.getPubDate(), item.getLink(), type);
                if (!is_featured) {
                    DomPuller puller = new DomPuller(sender, item.getLink(), type).setName(item.getTitle().trim());
                    Executor.getInstance().submit(puller);
                }
                result.add(mNewsItem);
            }
        }
        cb.onSuccess(result);
    }

    public interface NewsResult {
        void onSuccess(List<NewsItem> result);

        void onError(RetrofitError error);
    }

    public interface FeaturedResult {
        void onSuccess(List<NewsItem> result);
    }

    private interface FeedService {
        @GET("/feed/")
        void getFeed(Callback<RSS> response);

        @GET("/index.xml")
        void getPolygonFeed(Callback<RSS> response);

        @GET("/tag/feed/rss/")
        void getIon9ne(Callback<RSS> response);

        @GET("/rss")
        void getRss(Callback<RSS> response);

    }
}
