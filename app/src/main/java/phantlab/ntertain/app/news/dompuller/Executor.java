package phantlab.ntertain.app.news.dompuller;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class Executor extends ThreadPoolExecutor {
    private static Executor executor;

    public Executor() {
        super(5, 500, 100, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(15));
    }

    public static Executor getInstance() {
        if (executor == null)
            executor = new Executor();
        return executor;
    }
}
