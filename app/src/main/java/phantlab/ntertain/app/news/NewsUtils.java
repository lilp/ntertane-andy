package phantlab.ntertain.app.news;

import java.util.HashMap;

/**
 * Created by Ddev on 11/29/2015.
 */
public class NewsUtils {
    private static HashMap<News,String> thumbnails = null;

    public static HashMap<News,String> get(){
        if (thumbnails == null){
            thumbnails = new HashMap<>();
            thumbnails.put(News.PUNCHNG, "http://punchng.news/wp-content/uploads/2015/10/logo.jpg");
            thumbnails.put(News.INFONG,  "http://www.informationnigeria.org/wp-content/uploads/2012/12/LOGO231.png");
            thumbnails.put(News.NAIJALOADED, "https://pbs.twimg.com/media/BiGVBlYCYAAGhSB.png");
            thumbnails.put(News.PULSE, "http://static.pulse.ng/resources/20151124-ng/ver1-0/img/icon-128-precomposed.png");
            thumbnails.put(News.YNAIJA, "http://static.ynaija.com/wp-content/uploads/2015/09/ynaija_icon.png");
            thumbnails.put(News.NEWSAFRICA, "http://static.ynaija.com/wp-content/uploads/2015/09/ynaija_icon.png");
            thumbnails.put(News.NOBS, "http://static.ynaija.com/wp-content/uploads/2015/09/ynaija_icon.png");
            thumbnails.put(News.NOTJUSTOK, "http://static.ynaija.com/wp-content/uploads/2015/09/ynaija_icon.png");
            thumbnails.put(News.BELLANAIJA, "http://static.ynaija.com/wp-content/uploads/2015/09/ynaija_icon.png");
        }
        return thumbnails;
    }
}
