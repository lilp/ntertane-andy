package phantlab.ntertain.app.news;

/**
 * Created by Ddev on 11/29/2015.
 */
public class NewsItem {
    String title, thumbnail, timestamp, link, subtitle;
    News source;


    public NewsItem(String title, String thumbnail, String timestamp, String link,  News source) {
        this.title = title;
        this.thumbnail = thumbnail;
        this.timestamp = timestamp;
        this.source = source;
        this.link   = link;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getLink() {
        return link;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public News getSource() {
        return source;
    }

    public String getTitle() {
        return title;
    }

    public String getTimestamp() {
        return timestamp;
    }

}
