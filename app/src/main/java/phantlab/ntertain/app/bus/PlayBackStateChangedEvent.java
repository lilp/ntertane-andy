package phantlab.ntertain.app.bus;

import phantlab.ntertain.app.api.model.Tracks;

/**
 * Created by kartikeyasingh on 28/12/15.
 */
public class PlayBackStateChangedEvent {

    State state;
    Tracks track;
    public PlayBackStateChangedEvent(State state, Tracks track) {
        this.state = state;
        this.track = track;
    }

    public Tracks getTrack() {
        return track;
    }

    public State getState() {
        return state;
    }

    public enum State {
        NewSongPlaying,
        Playing,
        Paused,
        Stop
    }
}
