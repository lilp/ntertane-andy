package phantlab.ntertain.app.bus;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by kartikeyasingh on 28/12/15.
 */
public class BusProvider {
    private static final Bus bus = new Bus(ThreadEnforcer.ANY);

    private BusProvider() {/**/}

    public static Bus getInstance() {
        return bus;
    }
}
