package phantlab.ntertain.app.foundation;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ddev on 9/16/2015.
 */
public class Cache {
    private static String css;
    Context context;
    private static Cache instance;

    public Cache(Context context) {
        this.context = context;
    }

    public String getCss() {
        if (css == null)
            init();
        return css;
    }

    public static Cache getInstance() {
        return instance;
    }

    private void init() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BufferedReader reader = null;
                    reader = new BufferedReader(
                            new InputStreamReader(context.getAssets().open("css/foundation.min.css")));
                    css = reader.readLine();
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }).start();
    }
}
