package phantlab.ntertain.app.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class SearchHeaderHolder extends RecyclerView.ViewHolder {
    @Bind(android.R.id.title)
    public TextView title;

    public SearchHeaderHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
