package phantlab.ntertain.app.viewholders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.okhttp.internal.Util;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.utils.Utils;


/**
 * Created by Ddev on 11/22/2015.
 */
public class LinearHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @Bind(R.id.icon)
    public ImageView icon;
    @Bind(android.R.id.title)
    public TextView title;
    View indicator;
    private ClickCallbacks.ClickCallback click;

    public LinearHolder(View view, ClickCallbacks.ClickCallback click) {
        super(view);
        this.click = click;
        ButterKnife.bind(this, view);
        view.setOnClickListener(this);
    }

    public LinearHolder setIndicator(View indicator) {
        this.indicator = indicator;
        return this;
    }

    @Override
    public void onClick(View item) {
        if (indicator != null) {
            int[] location = new int[2],
                    indicator_location = new int[2];
            item.getLocationOnScreen(location);
            indicator.getLocationOnScreen(indicator_location);

            final int height = (int) (Utils.convertDpToPixel(56) * 1.5),
                    translation = (location[1] - height);
            TranslateAnimation animation = new TranslateAnimation(0f, 0f, indicator_location[1], translation);
            animation.setDuration(500);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {/***/}

                @Override
                public void onAnimationEnd(Animation animation) {
                    indicator.setTranslationY(translation);
                    click.onClick(getLayoutPosition());
                }

                @Override
                public void onAnimationRepeat(Animation animation) {/***/}
            });

            indicator.startAnimation(animation);

        }
    }
}
