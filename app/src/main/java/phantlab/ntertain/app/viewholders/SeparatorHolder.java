package phantlab.ntertain.app.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Ddev on 12/3/2015.
 */
public class SeparatorHolder extends RecyclerView.ViewHolder {
    public SeparatorHolder(View view) {
        super(view);
    }
}
