package phantlab.ntertain.app.viewholders;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.ClickCallbacks;

public class SongHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @Bind(R.id.thumbnail)
    public SimpleDraweeView thumbnail;

    @Nullable
    @Bind(R.id.options)
    public ImageButton options;

    @Nullable
    @Bind({android.R.id.title, android.R.id.summary, R.id.playback})
    public List<TextView> texts;

    ClickCallbacks.ClickCallback cb;

    public SongHolder(View view, ClickCallbacks.ClickCallback cb,
                      final ClickCallbacks.OverflowCallback mOverflowCallback) {
        super(view);
        ButterKnife.bind(this, view);
        this.cb = cb;
        view.setOnClickListener(this);

        if (options != null)
            options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOverflowCallback.onOverflowClick(getLayoutPosition());
                }
            });
    }

    @Override
    public void onClick(View view) {
        cb.onClick(getLayoutPosition());
    }
}