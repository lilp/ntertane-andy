package phantlab.ntertain.app.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.ClickCallbacks;

/**
 * Created by kartikeyasingh on 06/12/15.
 */
public class StackHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ClickCallbacks.ClickCallback callback;
    @Bind(R.id.category)
    public TextView title;

    public StackHolder(View view, ClickCallbacks.ClickCallback callback) {
        super(view);
        this.callback = callback;
        view.setOnClickListener(this);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onClick(View view) {
        callback.onClick(getLayoutPosition());
    }
}
