package phantlab.ntertain.app.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import phantlab.ntertain.app.movies.Movie;

/**
 * Created by Ddev on 11/29/2015.
 */
public class MovieDatabase extends SQLiteOpenHelper {
    public static final String KEY_TITLE = "title";
    public static final String KEY_THUMBNAIL = "thumbnail";
    public static final String KEY_PLOT = "plot";
    public static final String KEY_VIDEO = "video";
    public static final String DATABASE_NAME = MovieDatabase.class.getSimpleName();
    public static final String TABLE_NAME = "Movies";

    public MovieDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String QUERY = "CREATE TABLE IF NOT EXIST"  + TABLE_NAME;
        db.execSQL(QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int mOldVersion, int mNewVersion) {
        String QUERY = "";
        db.execSQL(QUERY);
    }

    public void addMovie(Movie movie){
        ContentValues values  = new ContentValues();
        values.put(KEY_TITLE, movie.getTitle());
    }
}
