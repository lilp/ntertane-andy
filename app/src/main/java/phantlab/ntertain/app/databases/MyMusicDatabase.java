package phantlab.ntertain.app.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.models.Playlist;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public class MyMusicDatabase extends SQLiteOpenHelper {
    public static final String DATABASE = "Ntertane_database";
    //    public static final int VERSION = 2;
    public static final String KEY_ID = "id";
    public static final String KEY_ALBUM_ID = "album_id";
    public static final String TABLE_PLAYLIST = "playlists";
    public static final String TABLE_SONGS = "songs";
    public static final String TABLE_ALBUMS = "albums";
    public static final String TABLE_ALBUM_ITEM = "album_item";
    public static final String TABLE_PLAYLIST_ITEM = "playlist_item";
    public static final String KEY_YT = "yt";
    public static final String KEY_PLAYLIST_ID = "pid";
    public static final String KEY_DURATION = "duration";
    public static final String KEY_PLAYLIST_NAME = "playlist_name";
    public static final String KEY_THUMBNAIL = "thumbnail";
    public static final String KEY_TRACK_COUNT = "track_count";
    public static final String KEY_ARTIST = "artist";
    public static final String KEY_URI = "uri";
    public static final String KEY_ALBUM = "album";
    public static final String KEY_NAME = "name";

    public MyMusicDatabase(Context context) {
        super(context, DATABASE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PLAYLIST_TABLE = "create table " + TABLE_PLAYLIST + "(" +
                KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_PLAYLIST_NAME + " TEXT," +
                KEY_TRACK_COUNT + " INTEGER" + ");";

        db.execSQL(CREATE_PLAYLIST_TABLE);

        String CREATE_MY_SONGS_TABLE = "create table " + TABLE_SONGS + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_NAME + " TEXT," +
                KEY_ALBUM + " TEXT," +
                KEY_ARTIST + " TEXT," +
                KEY_URI + " TEXT," +
                KEY_DURATION + " INTEGER," +
                KEY_THUMBNAIL + " TEXT," +
                KEY_YT + " TEXT);";

        db.execSQL(CREATE_MY_SONGS_TABLE);

        String CREATE_MY_ALBUMS_TABLE = "create table " + TABLE_ALBUMS + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_NAME + " TEXT," +
                KEY_ALBUM_ID + " INTEGER," +
                KEY_ARTIST + " TEXT," +
                KEY_URI + " TEXT," +
                KEY_THUMBNAIL + " TEXT" + ");";


        db.execSQL(CREATE_MY_ALBUMS_TABLE);

        String CREATE_ALBUM_ITEMS = "create table " + TABLE_ALBUM_ITEM + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_ALBUM_ID + " INTEGER," +
                KEY_NAME + " TEXT," +
                KEY_ARTIST + " TEXT," +
                KEY_URI + " TEXT," +
                KEY_THUMBNAIL + " TEXT" + ");";

        db.execSQL(CREATE_ALBUM_ITEMS);

        String CREATE_PLAYLIST_ITEMS = "create table " + TABLE_PLAYLIST_ITEM + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_PLAYLIST_ID + " INTEGER," +
                KEY_NAME + " TEXT," +
                KEY_ALBUM + " TEXT," +
                KEY_ARTIST + " TEXT," +
                KEY_URI + " TEXT," +
                KEY_DURATION + " INTEGER," +
                KEY_THUMBNAIL + " TEXT," +
                KEY_YT + " TEXT);";

        db.execSQL(CREATE_PLAYLIST_ITEMS);


    }

    public void createPlaylist(Playlist playlist) {
        ContentValues values = new ContentValues();
        values.put(KEY_PLAYLIST_NAME, playlist.getName());
        values.put(KEY_TRACK_COUNT, 0);
        getReadableDatabase().insert(TABLE_PLAYLIST, null, values);
    }

    public void addPlaylistItem(Tracks track, int playlist_id) {
        ContentValues values = getTracksValues(track);
        values.put(KEY_PLAYLIST_ID, playlist_id);
        getReadableDatabase().insert(TABLE_PLAYLIST_ITEM, null, values);
        increment_track(playlist_id);
    }

    public void increment_track(int playlist_id) {
        Log.i("$increment", "playlist_id = " + playlist_id);
        getWritableDatabase().execSQL("UPDATE " + TABLE_PLAYLIST + " SET " + KEY_TRACK_COUNT + "=" +
                KEY_TRACK_COUNT + "+1" + " WHERE " + KEY_ID + "=" + playlist_id);
        close();
    }

    public void createAlbum(Albums album) {
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, album.getName());
        values.put(KEY_ALBUM_ID, album.getId());
        values.put(KEY_ARTIST, album.getArtist());
        values.put(KEY_ARTIST, album.getArtist());
        values.put(KEY_THUMBNAIL, album.getThumbnail());
        getReadableDatabase().insert(TABLE_ALBUMS, null, values);
    }

    ContentValues getTracksValues(Tracks track) {
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, track.getTrack());
        values.put(KEY_ALBUM, track.getAlbum());
        values.put(KEY_ARTIST, track.getArtists()[0]);
        values.put(KEY_DURATION, track.getDuration());
        values.put(KEY_URI, track.getUrl());
        values.put(KEY_THUMBNAIL, track.getThumbnail());
        values.put(KEY_YT, track.getYoutubeId());
        return values;
    }

    public void add_fav(Tracks track) {
        getReadableDatabase().insert(TABLE_SONGS, null, getTracksValues(track));
    }

    public void addAlbumItem(Tracks track) {

    }

    public List<Tracks> getPlaylistItems(int playlist_id) {
        String query = "select * from " + TABLE_PLAYLIST_ITEM + " where " + KEY_PLAYLIST_ID + "="
                + playlist_id;
        List<Tracks> data = new ArrayList<>();

        Cursor playlist = getReadableDatabase().rawQuery(query, null);
        if (playlist.moveToFirst()) {
            do {
                String name = playlist.getString(2),
                        album = playlist.getString(3),
                        uri = playlist.getString(5),
                        thumb = playlist.getString(7),
                        yt = playlist.getString(8);
                String[] artists = new String[]{playlist.getString(4)};
                int duration = playlist.getInt(6);
                data.add(new Tracks(name, thumb, album, artists, uri, duration, yt));
            } while (playlist.moveToNext());
        }
        playlist.close();
        return data;
    }

    public List<Tracks> getSongs() {
        String query = "select * from " + TABLE_SONGS;
        List<Tracks> data = new ArrayList<>();
        Cursor songs = getReadableDatabase().rawQuery(query, null);
        if (songs.moveToFirst()) {
            do {
                String name = songs.getString(1),
                        album = songs.getString(2),
                        uri = songs.getString(4),
                        thumb = songs.getString(6),
                        yt = songs.getString(7);
                String[] artists = new String[]{songs.getString(3)};
                int duration = songs.getInt(5);
                data.add(new Tracks(name, thumb, album, artists, uri, duration, yt));
            } while (songs.moveToNext());
        }
        songs.close();
        return data;
    }


    public List<Playlist> getPlaylist() {
        List<Playlist> data = null;
        try {
            String query = "select * from " + TABLE_PLAYLIST;
            data = new ArrayList<>();

            Cursor playlist = getReadableDatabase().rawQuery(query, null);
            if (playlist.moveToFirst()) {
                do {
                    String name = playlist.getString(1);
                    int id = playlist.getInt(0),
                            track_count = playlist.getInt(2);
                    Log.i("$playlist", "id = " + id + " tracks = " + track_count);
                    data.add(new Playlist(name, id, track_count));
                } while (playlist.moveToNext());
            }
            playlist.close();
        } catch (Exception e) {
        }
        return data;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int from, int to) {
        db.execSQL("DROP TABLE " + TABLE_ALBUMS);
        db.execSQL("DROP TABLE " + TABLE_SONGS);
        db.execSQL("DROP TABLE " + TABLE_PLAYLIST);
        db.execSQL("DROP TABLE " + TABLE_ALBUM_ITEM);
        db.execSQL("DROP TABLE " + TABLE_PLAYLIST_ITEM);
        onCreate(db);
    }

    public void delete_playlist(int id) {
        getReadableDatabase().delete(TABLE_PLAYLIST, "id=?", new String[]{String.valueOf(id)});
        getReadableDatabase().delete(TABLE_PLAYLIST_ITEM, KEY_PLAYLIST_ID + "=?", new String[]{String.valueOf(id)});
    }
}
