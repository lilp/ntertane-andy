package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class TopPlaylistCallback {
    String status;

    List<Tracks> playlist;

    public boolean isSuccessFull(){
        return status.equals("success");
    }

    public List<Tracks> getPlaylist() {
        return playlist;
    }
}
