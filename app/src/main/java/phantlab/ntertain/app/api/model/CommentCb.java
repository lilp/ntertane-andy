package phantlab.ntertain.app.api.model;

/**
 * Created by Ddev on 4/10/2016.
 */
public class CommentCb {
    String status;
    Comment comment;

    public Comment getComment() {
        return comment;
    }

    public boolean getStatus() {
        return status.equals("success");
    }
}
