package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.SearchCallback;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kartikeyasingh on 15/12/15.
 */
public class Search extends BaseEndpoint{

    public interface SearchService {
        @POST("/search")
        @FormUrlEncoded
        void search(@Field("search") String query,
                    Callback<SearchCallback> cb);
    }

    SearchService mSearchService;

    public Search(){
        mSearchService = APIFactory.createService(SearchService.class, BASE_URL, false);
    }

    public void search(String query, Callback<SearchCallback> cb){
        mSearchService.search(query, cb);
    }
}
