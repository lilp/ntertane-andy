package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 11/12/15.
 */
public class UserCallback {
    String status, token, message;
    User user;

    public String getMessage() {
        return message;
    }

    public boolean isSuccessFull() {
        return status.equals("success");
    }

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }
}
