package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class ArtistItemCallback {
    ArtistItem artist;

    List<Albums> albums;
    List<Tracks> tracks;

    public List<Tracks> getTracks() {
        return tracks;
    }

    public List<Albums> getAlbums() {
        return albums;
    }

    public ArtistItem getArtist() {
        return artist;
    }
}
