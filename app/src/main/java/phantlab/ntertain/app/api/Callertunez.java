package phantlab.ntertain.app.api;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.GET;

/**
 * Created by kartikeyasingh on 30/12/15.
 */
public class Callertunez {
    String BaseEndpoint = "http://www.mobilexserver.com:8080/ntertain";
    CallertuneService mService;

    public Callertunez() {
        mService = new RestAdapter.Builder().setEndpoint(BaseEndpoint)
                .setLogLevel(RestAdapter.LogLevel.FULL).build().create(CallertuneService.class);
    }

    interface CallertuneService {
        @GET("/api?action=getAllTunes")
        void getAllCallerTunes(Callback<Response> cb);
    }

    public void getAllTunes(Callback<Response> cb){
        mService.getAllCallerTunes(cb);
    }
}
