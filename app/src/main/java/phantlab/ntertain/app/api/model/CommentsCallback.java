package phantlab.ntertain.app.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Ddev on 4/10/2016.
 */
public class CommentsCallback implements Parcelable {
    String status;
    List<Comment> comments;

    protected CommentsCallback(Parcel in) {
        status   = in.readString();
        comments = in.readArrayList(Comment.class.getClassLoader());

    }

    public static final Creator<CommentsCallback> CREATOR = new Creator<CommentsCallback>() {
        @Override
        public CommentsCallback createFromParcel(Parcel in) {
            return new CommentsCallback(in);
        }

        @Override
        public CommentsCallback[] newArray(int size) {
            return new CommentsCallback[size];
        }
    };

    public List<Comment> getComments() {
        return comments;
    }

    public boolean getStatus() {
        return status.equals("success");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeList(comments);
    }
}
