package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.ArtistCallback;
import phantlab.ntertain.app.api.model.ArtistItemCallback;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class Artists extends BaseEndpoint {
    private interface ArtistService {
        @POST("/artists")
        void getArtists(@Field("length") int limit,
                        @Field("start") int start,
                        Callback<ArtistCallback> cb);

        @POST("/artist")
        @FormUrlEncoded
        void getArtist(@Field("id") int artist,
                       Callback<ArtistItemCallback> cb);

        @POST("/artist")
        @FormUrlEncoded
        void getArtistByName(@Field("name") String name,
                       Callback<ArtistItemCallback> cb);
    }

    ArtistService mArtistService;

    public Artists(){
        mArtistService = APIFactory.createService(ArtistService.class, BASE_URL, false);
    }

    public void getArtist(int id, Callback<ArtistItemCallback> cb){
        mArtistService.getArtist(id, cb);
    }

    public void getArtistByName(String name, Callback<ArtistItemCallback> cb){
        mArtistService.getArtistByName(name, cb);
    }
}
