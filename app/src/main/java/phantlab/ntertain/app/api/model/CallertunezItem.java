package phantlab.ntertain.app.api.model;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kartikeyasingh on 30/12/15.
 */
public class CallertunezItem implements ParentObject{
    public int ID;
    public String Title, GID, Contnt, DownloadDet, Author, thumbnail, preview, path;

    public String getPath() {
        return path;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getID() {
        return ID;
    }

    public String getAuthor() {
        return Author;
    }

    public String getContnt() {
        return Contnt;
    }

    public String getDownloadDet() {
        return DownloadDet;
    }

    public String getGID() {
        return GID;
    }

    public String getTitle() {
        return Title;
    }

    @Override
    public List<Object> getChildObjectList() {
        List<Object> child = new ArrayList<>();
        Child item = new Child();
        item.setItem(this);
        child.add(item);
        return child;
    }

    @Override
    public void setChildObjectList(List<Object> list) {

    }

    class Child {
        CallertunezItem item;

        public void setItem(CallertunezItem item) {
            this.item = item;
        }

        public CallertunezItem getItem() {
            return item;
        }
    }
}
