package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 15/12/15.
 */
public class SearchCallback {
    SearchResult result;
    String status;

    public SearchResult getResult() {
        return result;
    }

    public boolean isSuccessFull(){
        return status.equals("success");
    }
}
