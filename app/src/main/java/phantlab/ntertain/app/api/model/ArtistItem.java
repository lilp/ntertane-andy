package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class ArtistItem {
    String name, image_small;
    int id;


    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return image_small;
    }
}
