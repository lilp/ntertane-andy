package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by Ddev on 1/3/2016.
 */
public class RadioCallback {
    String status;
    List<Tracks> radio;

    public List<Tracks> getRadio() {
        return radio;
    }

    public boolean isSuccessFull(){
        return status.equals("success");
    }
}
