package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class Tracks {
    String name, album_cover, album_name, url, youtube_id;
    String[] artists;
    long duration;

    public String getYoutubeId() {
        return youtube_id;
    }

    public Tracks(String name, String album_cover, String album_name, String[] artists, String url,
                  long duration, String yt) {
        this.name = name;
        this.album_cover = album_cover;
        this.url = url;
        this.album_name = album_name;
        this.artists = artists;
        this.duration = duration;
        this.youtube_id = yt;
    }

    public boolean isPlayable() {
        return url != null;
    }

    public long getDuration() {
        return duration;
    }

    public String getThumbnail() {
        return album_cover;
    }

    public String getAlbum() {
        return album_name;
    }

    public String getTrack() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String[] getArtists() {
        return artists;
    }
}
