package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.AdCallback;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Ddev on 4/2/2016.
 */
public class Ads extends BaseEndpoint {

    public interface AdService{
        @GET("/ads")
        void getAds(Callback<AdCallback> cb);
    }

    AdService service;

    public Ads(){
        service = APIFactory.createService(AdService.class,BASE_URL,true);
    }

    public void getAds(Callback<AdCallback> cb){
        service.getAds(cb);
    }
}
