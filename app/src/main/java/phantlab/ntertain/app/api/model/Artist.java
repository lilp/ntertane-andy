package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class Artist {
    String name, image_small;
    int id, spotify_popularity, albums_number, tracks_number;

    public void setName(String name) {
        this.name = name;
    }

    public int getAlbumCount() {
        return albums_number;
    }

    public int getTrackCount() {
        return tracks_number;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return image_small;
    }

    public int getSpotifyPopularity() {
        return spotify_popularity;
    }
}
