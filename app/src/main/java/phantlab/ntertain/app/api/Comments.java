package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.CommentCb;
import phantlab.ntertain.app.api.model.CommentsCallback;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Ddev on 1/16/2016.
 */
public class Comments extends BaseEndpoint {
    public interface CommentService {
        @POST("/comment")
        @FormUrlEncoded
        void postComment(@Field("content") String comment,
                         @Field("token") String token,
                         Callback<CommentCb> cb);

        @GET("/comment/{token}")
        void getComments(@Path("token") String token,
                         Callback<CommentsCallback> cb);
    }

    CommentService service;

    public Comments() {
        service = APIFactory.createService(CommentService.class, BASE_URL, false);
    }

    public void postComments(String comment, String token, Callback<CommentCb> cb) {
        service.postComment(comment, token, cb);
    }

    public void getComments(String token, Callback<CommentsCallback> cb) {
        service.getComments(token, cb);
    }

}
