package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by Ddev on 4/9/2016.
 */
public class DistanceCallback {
    String status;

    List<Rows> rows;

    public List<Rows> getRows() {
        return rows;
    }

    public boolean success() {
        return status.equals("OK");
    }

    public class Rows {
        List<Elements> elements;

        public List<Elements> getElements() {
            return elements;
        }
    }

    public class Elements {
        Distance distance;
        String status;

        public boolean success() {
            return status.equals("OK");
        }

        public Distance getDistance() {
            return distance;
        }
    }

    public class Distance {
        String text;

        public String toString() {
            return text;
        }
    }

}
