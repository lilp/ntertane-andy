package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.TrackCallback;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class TopTracks extends BaseEndpoint {

    private TopTrackService service = null;

    public TopTracks() {
        service = APIFactory.createService(TopTrackService.class, BASE_URL, false);
    }

    public void getTracks(int length, int start, Callback<TrackCallback> cb) {
        service.get(length, start, cb);
    }

    public void getTopTracks(Callback<TrackCallback> cb) {
        service.get_top(cb);
    }

    private interface TopTrackService {
        @POST("/top/")
        @FormUrlEncoded
        void get(@Field("length") int length,
                 @Field("start") int start,
                 Callback<TrackCallback> cb);

        @GET("/top/")
        void get_top(Callback<TrackCallback> cb);
    }
}
