package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.UserCallback;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kartikeyasingh on 11/12/15.
 */
public class UserEndpoint extends BaseEndpoint {

    private interface UserService {
        @POST("/register")
        @FormUrlEncoded
        void register(@Field("name")  String name,
                      @Field("email") String email,
                      @Field("login") String username,
                      @Field("password") String password,
                      Callback<UserCallback> cb);

        @POST("/users/login")
        @FormUrlEncoded
        void login(@Field("login") String username,
                   @Field("password") String password,
                   Callback<UserCallback> cb);
    }

    UserService mUserService;

    public UserEndpoint(){
        mUserService = APIFactory.createService(UserService.class, BASE_URL, false);
    }

    public void register(String name, String email, String username, String password,
                         Callback<UserCallback> cb){
        mUserService.register(name, email, username, password, cb);
    }

    public void login(String login, String password , Callback<UserCallback> cb) {
        mUserService.login(login, password, cb);
    }
}
