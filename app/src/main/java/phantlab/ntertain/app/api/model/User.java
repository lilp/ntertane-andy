package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 11/12/15.
 */
public class User {
    String name, email, login, token;

    public User setToken(String token) {
        this.token = token;
        return this;
    }

    public String getToken() {
        return token;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }
}
