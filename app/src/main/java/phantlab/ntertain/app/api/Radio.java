package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.RadioCallback;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Ddev on 1/3/2016.
 */
public class Radio extends BaseEndpoint {
    public interface RadioService {
        @POST("/radio")
        @FormUrlEncoded
        void getRadio(@Field("id") long artistId, Callback<RadioCallback> cb);
    }

    RadioService service;

    public Radio() {
        service = APIFactory.createService(RadioService.class, BASE_URL, true);
    }

    public void getRadioForArtist(long id, Callback<RadioCallback> callback) {
        service.getRadio(id, callback);
    }
}
