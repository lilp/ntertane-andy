package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.AlbumCallback;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class Album extends BaseEndpoint {
    private interface AlbumService {
        @POST("/album")
        @FormUrlEncoded
        void getAlbum(@Field("id") int album_id,
                        Callback<AlbumCallback> cb);

        @POST("/album")
        @FormUrlEncoded
        void getAlbumByName(@Field("name") String album,
                        Callback<AlbumCallback> cb);
    }

    AlbumService mArtistService;

    public Album(){
        mArtistService = APIFactory.createService(AlbumService.class, BASE_URL, false);
    }

    public void getAlbum(int album_id, Callback<AlbumCallback> cb){
        mArtistService.getAlbum(album_id, cb);
    }

    public void getAlbumByName(String album, Callback<AlbumCallback> cb){
        mArtistService.getAlbumByName(album, cb);
    }
}
