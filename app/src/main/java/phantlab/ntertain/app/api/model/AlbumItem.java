package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 17/12/15.
 */
public class AlbumItem {
    int id, number_tracks;
    String released_at, artist, cover, name;
    List<Tracks> tracks;


    public List<Tracks> getTracks() {
        return tracks;
    }

    public String getArtist() {
        return artist;
    }

    public int getId() {
        return id;
    }

    public int getTrackCount() {
        return number_tracks;
    }

    public String getCover() {
        return cover;
    }

    public String getName() {
        return name;
    }

    public String getReleasedAt() {
        return released_at;
    }
}
