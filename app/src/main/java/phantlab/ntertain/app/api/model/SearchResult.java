package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 15/12/15.
 */
public class SearchResult {
    List<Albums> albums;
    List<Artist> artists;
    List<Tracks> tracks;

    public List<Albums> getAlbums() {
        return albums;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public List<Tracks> getTracks() {
        return tracks;
    }

}
