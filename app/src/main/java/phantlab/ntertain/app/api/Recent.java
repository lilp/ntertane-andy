package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.RecentBody;
import phantlab.ntertain.app.api.model.RecentCallback;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class Recent extends BaseEndpoint {
    AlbumService mArtistService;

    public Recent() {
        mArtistService = APIFactory.createService(AlbumService.class, BASE_URL, false);
    }

    public void getRecent(Callback<RecentCallback> cb) {
        mArtistService.getRecent(new RecentBody(), cb);
    }

    private interface AlbumService {
        @POST("/recent")
        void getRecent(@Body RecentBody body,
                       Callback<RecentCallback> cb);
    }
}
