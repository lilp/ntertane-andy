package phantlab.ntertain.app.api.model;

/**
 * Created by Ddev on 4/3/2016.
 */
public class AdItem {
    String name, link, picture, audio;

    public String getAudio() {
        return audio;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return picture;
    }
}
