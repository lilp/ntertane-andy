package phantlab.ntertain.app.api;

import phantlab.ntertain.app.application.Ntertain;
import phantlab.ntertain.app.application.UserUtils;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class APIFactory {
    private static Ntertain app;

    public static APIFactory with(Ntertain app) {
        APIFactory.app = app;
        return new APIFactory();
    }

    public static <S> S createService(Class<S> service, String mBaseUrl, boolean intercept) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(mBaseUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL);

        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("ntertane-authentication-token", Ntertain.getUser().getToken());
            }
        });

        RestAdapter adapter = builder.build();
        return adapter.create(service);
    }
}
