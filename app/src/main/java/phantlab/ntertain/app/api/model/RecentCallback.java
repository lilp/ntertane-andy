package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 18/12/15.
 */
public class RecentCallback {
    String status;

    List<Tracks> latests;

    public List<Tracks> getRecent() {
        return latests;
    }

    public boolean isSuccessFull(){
        return status.equals("success");
    }
}
