package phantlab.ntertain.app.api.model;

import java.util.ArrayList;

/**
 * Created by Ddev on 4/3/2016.
 */
public class AdCallback {
    ArrayList<AdItem> ad;
    String status;

    public boolean isSuccessful() {
        return status.equals("success");
    }

    public ArrayList<AdItem> getAd() {
        return ad;
    }
}
