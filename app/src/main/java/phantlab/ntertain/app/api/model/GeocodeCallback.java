package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by Ddev on 4/8/2016.
 */
public class GeocodeCallback {
    List<GeocodeResults> results;
    String status;

    public boolean success() {
        return status.equals("OK");
    }

    public List<GeocodeResults> getGeocodeResults() {
        return results;
    }
}
