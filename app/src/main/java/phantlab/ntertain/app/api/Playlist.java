package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.TopPlaylistCallback;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class Playlist extends BaseEndpoint {
    private interface PlaylistService {
        @POST("/generate/playlist/artist")
        @FormUrlEncoded
        void radio(@Field("name") String artist,
                    Callback<TopPlaylistCallback> cb);
    }

    PlaylistService mPlaylistService;

    public Playlist(){
        mPlaylistService = APIFactory.createService(PlaylistService.class, BASE_URL, false);
    }

    public void startRadio(String name, Callback<TopPlaylistCallback> cb){
        mPlaylistService.radio(name, cb);
    }
}
