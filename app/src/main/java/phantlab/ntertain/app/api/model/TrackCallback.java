package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class TrackCallback {
    String status;
    List<Tracks> playlist;

    public List<Tracks> getData() {
        return playlist;
    }

    public boolean isSuccessFull(){
        return status.equals("success");
    }
}
