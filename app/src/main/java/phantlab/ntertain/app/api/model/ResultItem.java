package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class ResultItem {
    Artist artistItem;
    Tracks trackItem;
    Albums albumItem;
    ResultType type;

    String header;

    public ResultItem(ResultType type){
        this.type = type;
    }

    public ResultItem setHeader(String header) {
        this.header = header;
        return this;
    }

    public String getHeader() {
        return header;
    }

    public Albums getAlbumItem() {
        return albumItem;
    }

    public ResultItem setAlbumItem(Albums albumItem) {
        this.albumItem = albumItem;
        return this;
    }

    public Artist getArtistItem() {
        return artistItem;
    }

    public ResultItem setArtistItem(Artist artistItem) {
        this.artistItem = artistItem;
        return this;
    }

    public Tracks getTrackItem() {
        return trackItem;
    }

    public ResultType getType(){
        return type;
    }

    public ResultItem setTrackItem(Tracks trackItem) {
        this.trackItem = trackItem;
        return this;
    }
}
