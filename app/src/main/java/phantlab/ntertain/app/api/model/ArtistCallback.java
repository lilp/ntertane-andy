package phantlab.ntertain.app.api.model;

import java.util.List;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class ArtistCallback {
    List<Artist> data;
    int recordsTotal;

    public List<Artist> getData() {
        return data;
    }

    public int geTotalRecords() {
        return recordsTotal;
    }
}
