package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 15/12/15.
 */
public class Albums {
    int id, number_tracks, spotify_popularity;
    String name, cover, artist;
    //    DateTime released_at;
    String[] tracks;


    public Albums(int id, int number_tracks, int spotify_popularity, String name,
                  String cover, String artist) {
        this.id = id;
        this.number_tracks = number_tracks;
        this.spotify_popularity = spotify_popularity;
        this.name = name;
        this.cover = cover;
        this.artist = artist;
    }

    public int getTrackCount() {
        return number_tracks;
    }

    public int getSpotifyPopularity() {
        return spotify_popularity;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return cover;
    }

    public String[] getTracks() {
        return tracks;
    }

    public String getArtist() {
        return artist;
    }
}