package phantlab.ntertain.app.api;

import phantlab.ntertain.app.api.model.DistanceCallback;
import phantlab.ntertain.app.api.model.GeocodeCallback;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.GET;

import retrofit.http.Query;

/**
 * Created by Ddev on 4/8/2016.
 */
public class Geocode {
    public static final String mBaseUrl = "https://maps.googleapis.com";

    public interface GeocodeInterface {
        @GET("/maps/api/geocode/json?sensor=true_or_false")
        void geocode(@Query("address") String address,
                     Callback<GeocodeCallback> cb);

        @GET("/maps/api/distancematrix/json?key=AIzaSyBHyj4b1SmoEDmIEzEvJCWAdBxwmzzAdwY")
        void getDistance(@Query("origins") String origins,
                         @Query("destinations") String destinations,
                         Callback<DistanceCallback> cb);


    }

    GeocodeInterface gi;

    public Geocode() {
        gi = new RestAdapter.Builder().setEndpoint(mBaseUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL).build().create(GeocodeInterface.class);
    }

    public void geocode(String address, Callback<GeocodeCallback> cb) {
        gi.geocode(address, cb);
    }

    public void getDistance(String origins, String destination, Callback<DistanceCallback> cb) {
        gi.getDistance(origins, destination, cb);
    }
}
