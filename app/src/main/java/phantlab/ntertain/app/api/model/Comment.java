package phantlab.ntertain.app.api.model;

/**
 * Created by Ddev on 4/10/2016.
 */
public class Comment {
    String username, content;

    public String getContent() {
        return content;
    }

    public String getUsername() {
        return username;
    }
}
