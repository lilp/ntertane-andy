package phantlab.ntertain.app.api.model;

/**
 * Created by Ddev on 4/8/2016.
 */
public class GeocodeResults {
     Geometry geometry;

    public class Geometry {
        Location location;

        public Location getLocation() {
            return location;
        }
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public class Location{
        double lat, lng;

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }
    }
}
