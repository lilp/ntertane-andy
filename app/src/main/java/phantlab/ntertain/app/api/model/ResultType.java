package phantlab.ntertain.app.api.model;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public enum ResultType {
    ARTIST,
    ALBUM,
    TRACK
}
