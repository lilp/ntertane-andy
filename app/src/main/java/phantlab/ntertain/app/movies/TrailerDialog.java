package phantlab.ntertain.app.movies;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import phantlab.ntertain.app.R;

/**
 * Created by Ddev on 11/28/2015.
 */
public class TrailerDialog extends android.support.v4.app.DialogFragment implements
        YouTubePlayer.OnInitializedListener {

    private YouTubePlayerSupportFragment player;
    private String video;
    private ViewGroup mContainer;

    public TrailerDialog setVideo(String video) {
        this.video = video.substring(video.lastIndexOf('/') + 1);
        return this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View trailer = inflater.inflate(R.layout.trailer_content, container, false);
        mContainer = container;
        player = (YouTubePlayerSupportFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        player.initialize(Config.YOUTUBE_API_KEY, this);
        return trailer;
    }

    @Override
    public void onDestroyView() {
        getFragmentManager().findFragmentById(R.id.youtube_fragment).onDestroy();
        super.onDestroyView();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean restored) {
        player.cueVideo(video);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
    }
}
