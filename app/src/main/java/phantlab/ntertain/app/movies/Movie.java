package phantlab.ntertain.app.movies;

/**
 * Created by Ddev on 11/27/2015.
 */
public class Movie {
    String thumbnail, title , link, trailer, cast, genre;

    public void setLink(String link) {
        this.link = link;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }
}
