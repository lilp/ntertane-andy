package phantlab.ntertain.app.movies;

import java.util.List;

/**
 * Created by Ddev on 11/28/2015.
 */
public class Shows {
    String title, address;
    List<String> shows;

    public Shows(String title, String address, List<String> shows) {
        this.title = title;
        this.address = address;
        this.shows = shows;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getShows() {
        return shows;
    }

    public String getAddress() {
        return address;
    }
}
