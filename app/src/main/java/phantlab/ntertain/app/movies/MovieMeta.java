package phantlab.ntertain.app.movies;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ddev on 11/28/2015.
 */
public class MovieMeta {
    List<String> data = new ArrayList<>();
    private String title, genre, plot, thumb, video, link, playback, content_rating;

    public List<String> getData() {
        return data;
    }

    public MovieMeta() {/***/}

    public MovieMeta(String title, String genre, String plot, String thumb, String video, String link, String playback, String content_rating) {
        this.title = title;
        this.genre = genre;
        this.plot = plot;
        this.thumb = thumb;
        this.video = video;
        this.link = link;
        this.content_rating = content_rating;
        this.playback = playback;
    }

    public String getContent_rating() {
        return content_rating;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getGenre() {
        return genre;
    }

    public String getPlayback() {
        return playback;
    }

    public String getPlot() {
        return plot;
    }

    public String getThumb() {
        return thumb;
    }

    public String getVideo() {
        return video;
    }
}
