package phantlab.ntertain.app.movies;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import phantlab.ntertain.app.callbacks.MoviesCallback;

/**
 * Created by Ddev on 11/27/2015.
 */
public class MoviesEngine {
    public final static String AGENT = "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US;   rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";
    private final String BASE_URL = "http://tripican.com/movies/home";
    private final String UPCOMING_MOVIES = "http://www.imdb.com/movies-coming-soon/";
    private Element doc = null;

    public MoviesEngine() {
    }

    public void getFeatured(final MoviesCallback.Result cb) {
        new AsyncTask<Void, Void, List<Movie>>() {
            @Override
            protected List<Movie> doInBackground(Void... params) {
                try {
                    List<Movie> movies = new ArrayList<>();
                    doc = Jsoup.connect(BASE_URL).userAgent(AGENT).get().body();
                    if (doc != null) {
                        for (Element elm : doc.select("div.carousel-inner > div")) {
                            Movie movie = new Movie();
                            Elements elms = elm.getElementsByTag("a");
                            movie.setLink(elms.attr("href"));
                            movie.setTitle(elm.getElementsByClass("carousel-caption").select("h4").text());
                            for (Element child : elms.select("img")) {
                                movie.setThumbnail(child.attr("src"));
                                movies.add(movie);
                            }
                        }
                        return movies;
                    }
                } catch (Exception ignored) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<Movie> movies) {
                cb.onSuccess(movies);
            }
        }.execute();
    }

    public void getNowShowing(final MoviesCallback.StringResult cb) {
        new AsyncTask<Void, Void, List<MovieMeta>>() {
            @Override
            protected List<MovieMeta> doInBackground(Void... params) {
                    try {
                        List<MovieMeta> movies = new ArrayList<>();
                        doc = Jsoup.connect(BASE_URL).userAgent(AGENT)
                                .timeout(10000).get().body();
                        Log.i("#size", String.valueOf(doc.select("div#movieFeatures").first().
                                children().select("div.movies > div").size()));

                        for (Element elm : doc.select("div#movieFeatures").first().children().select("div.movies > div"))
                            for (Element _ : elm.children()) {
                                MovieMeta meta = new MovieMeta();
                                for (Element child : _.getElementsByTag("meta"))
                                    meta.getData().add(child.attr("content"));
                                if (!meta.getData().isEmpty())
                                    movies.add(meta);
                            }
                        return movies;
                    } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<MovieMeta> movies) {
                cb.onSuccess(movies);
            }
        }.execute();
    }

    public void getTimings(String link, final MoviesCallback.ShowsResult cb) {
        new AsyncTask<String, Void, List<Shows>>() {
            @Override
            protected List<Shows> doInBackground(String... params) {
                try {
                    Element body = Jsoup.connect(params[0])
                            .userAgent(AGENT).get().body();

                    List<Shows> result = new ArrayList<>();

                    for (Element elm : body.select("div.cinema")) {
                        Elements theater = elm.select("div.span8");
                        String name = theater.select("h3").first().text(),
                                address = theater.select("div.cinema-address").first().text();
                        List<String> shows = new ArrayList<>();
                        for (Element elms : elm.select("div.showtime"))
                            shows.add(elms.text());
                        result.add(new Shows(name, address, shows));
                    }

                    return result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<Shows> shows) {
                cb.onSuccess(shows);
            }
        }.execute(link);
    }

    public void getThumb(String name, final MoviesCallback.ThumbResult cb) {
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    Element body = Jsoup.connect(params[0]).userAgent(AGENT).get().body();
                    Elements elms = body.select("tbody").first().children();
                    if (elms.size() > 1)
                        return elms.get(1).children().first()
                                .select("a").first().children().attr("src");
                } catch (HttpStatusException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException ignored) {

                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                cb.onSuccess(result);
            }
        }.execute("https://en.wikipedia.org/wiki/" + name);
    }

    public void getComingSoon(final MoviesCallback.StringResult cb) {
        new AsyncTask<Void, Void, List<MovieMeta>>() {
            @Override
            protected List<MovieMeta> doInBackground(Void... voids) {
                try {
                    Log.i("$commingSoon", "comingSoon");
                    Element element = Jsoup.connect(UPCOMING_MOVIES).userAgent(AGENT).get().body();
                    List<MovieMeta> result = new ArrayList<>();
                    Elements movies = element.select("div.list.detail");
//                    Log.i("$movies", "movies = " + movies.get(0).children().size());
                    Elements elms = movies.select("div.list_item");
                    for (Element elm : elms) {
                        Element thumb = elm.select("img").get(0);
                        Element link_elm = elm.select("a[itemprop=url]").get(0);
                        Elements genres = elm.select("span[itemprop=genre]");
                        String genre = "";
                        for (int i = 0; i < genres.size(); i++) {
                            Element genre_elm = genres.get(i);
                            genre += genre_elm.text() + (i == (genres.size() - 1) ? "" : ",");
                        }
                        String name = link_elm.text(),
                                link = link_elm.attr("href"),
                                thumbnail = thumb.attr("src"),
                                plot = elm.select("div[itemprop=description]").text();
//                                playback = elm.select("time[itemprop=duration]").get(0).text();

                        result.add(new MovieMeta(name, genre, plot, thumbnail, "", link, "", ""));
                    }
                    return result;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<MovieMeta> result) {
                cb.onSuccess(result);
            }
        }.execute();
    }

    public void getActors(String link, final MoviesCallback.CastResult cb) {
        new AsyncTask<String, Void, List<String>>() {
            @Override
            protected List<String> doInBackground(String... params) {
                try {
                    List<String> data = new ArrayList<>();
                    Element body = Jsoup.connect(params[0]).userAgent(AGENT).get().body();
                    String actors = body.select("div.moviedetails").first().select("div.detail").get(3).children().get(1).text();
                    for (String actor : actors.split(","))
                        data.add(actor);
                    return data;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<String> strings) {
                cb.onSuccess(strings);
            }
        }.execute(link);
    }
}
