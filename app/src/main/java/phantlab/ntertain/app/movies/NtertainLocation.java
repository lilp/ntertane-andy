package phantlab.ntertain.app.movies;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.List;

import phantlab.ntertain.app.fragments.MoviesFragment;

/**
 * Created by Ddev on 4/9/2016.
 */
public class NtertainLocation {

    Activity sender;
    LocationManager mLocationManager;
    public final static int REQUEST_CODE = 1000;

    SharedPreferences preferences;

    public NtertainLocation(Activity sender, SharedPreferences preferences) {
        this.sender = sender;
        this.preferences = preferences;
        mLocationManager = (LocationManager) sender.getSystemService(Context.LOCATION_SERVICE);
    }

    public boolean enableLocation() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlert();
            return false;
        }
        return true;
    }

    private void buildAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(sender);
        builder.setMessage("Your GPS seems to be disabled, Ntertane needs your location for getting," +
                "nearby cinemas.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        sender.startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_CODE);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                }).create().show();
    }


    public void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(sender, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(sender, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(sender, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        }
        List<String> providers = mLocationManager.getProviders(true);
        Location location = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null)
                continue;
            if (location == null || l.getAccuracy() < location.getAccuracy())
                location = l;
        }

        if (location != null) {
            SharedPreferences.Editor edit = preferences.edit();
            putDouble(edit, MoviesFragment.KEY_LATITUDE, location.getLatitude());
            putDouble(edit, MoviesFragment.KEY_LONGITUDE, location.getLongitude());
            edit.commit();
        }
    }

    public double getLatitude() {
        return getDouble(preferences, MoviesFragment.KEY_LATITUDE, 0.0);
    }

    public double getLongitude() {
        return getDouble(preferences, MoviesFragment.KEY_LONGITUDE, 0.0);
    }

    SharedPreferences.Editor putDouble(final SharedPreferences.Editor edit, final String key, final double value) {
        return edit.putLong(key, Double.doubleToRawLongBits(value));
    }

    public double getDouble(final SharedPreferences prefs, final String key, final double defaultValue) {
        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(defaultValue)));
    }

}
