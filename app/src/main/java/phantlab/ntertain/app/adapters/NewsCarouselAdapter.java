package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.NewsDetails;
import phantlab.ntertain.app.news.NewsItem;
import phantlab.ntertain.app.news.NewsUtils;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/29/2015.
 */
public class NewsCarouselAdapter extends PagerAdapter implements View.OnClickListener {
    List<NewsItem> data;

    @Bind(R.id.news_title)
    TextView title;

    @Bind({R.id.thumbnail, R.id.news_thumb})
    List<SimpleDraweeView> thumbnails;

    Activity context;

    public NewsCarouselAdapter(Activity context) {
        this.context = context;
    }

    public void setData(List<NewsItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data != null ? Math.min(data.size(), 4) : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final NewsItem model = data.get(position);
        View layout = LayoutInflater.from(container.getContext()).inflate(R.layout.news_pager_item, container, false);
        ButterKnife.bind(this, layout);
        layout.setOnClickListener(this);
        int width = Utils.convertDpToPixel(400),
                height = Utils.convertDpToPixel(176),
                width_small = Utils.convertDpToPixel(28);

        title.setText(model.getTitle());
        loadThumb(thumbnails.get(0), height, width, model.getThumbnail());
        loadThumb(thumbnails.get(1), width_small, width_small, NewsUtils.get().get(model.getSource()));
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.StartActivity(context, NewsDetails.class, Utils.getNewsBundle(model));
            }
        });
        container.addView(layout);
        return layout;
    }

    private void loadThumb(SimpleDraweeView view, int height, int width, String uri) {
         ThumbnailController.with(view).dimens(width, height).uri(Uri.parse(uri)).load();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void onClick(View v) {

    }
}
