package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.BaseActivity;
import phantlab.ntertain.app.api.model.CallertunezItem;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 31/12/15.
 */
public class CallertuneAdapter extends ExpandableRecyclerAdapter<CallertuneAdapter.ParentHolder, CallertuneAdapter.ChildHolder> {
    LayoutInflater inflater;
    List<ParentObject> parent;
    int width;
    Activity sender;


    public CallertuneAdapter(Activity context, List<ParentObject> parent) {
        super(context, parent);
        inflater = LayoutInflater.from(context);
        width = Utils.convertDpToPixel(48);
        this.sender = context;
        this.parent = parent;
    }

    @Override
    public ParentHolder onCreateParentViewHolder(ViewGroup VGroup) {
        return new ParentHolder(inflater.inflate(R.layout.callertune_parent, VGroup, false));
    }

    @Override
    public ChildHolder onCreateChildViewHolder(ViewGroup VGroup) {
        return new ChildHolder(inflater.inflate(R.layout.callertune_options, VGroup, false));
    }

    @Override
    public void onBindParentViewHolder(ParentHolder holder, int position, Object data) {
        CallertunezItem item = (CallertunezItem) data;
        holder.texts.get(0).setText(item.getTitle());
        holder.texts.get(1).setText(item.getAuthor());
        ThumbnailController.with(holder.thumbnail).dimens(width, width)
                .uri(Uri.parse(item.getThumbnail())).load();
    }


    @Override
    public void onBindChildViewHolder(ChildHolder holder, int position, Object data) {
    }

    public class ChildHolder extends ChildViewHolder implements View.OnClickListener {
        @Bind({R.id.get_tune, R.id.present, R.id.listen})
        List<TextView> texts;

        public ChildHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            for (TextView text : texts)
                text.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getLayoutPosition() / 2;

            switch (view.getId()) {
                case R.id.get_tune:
                    break;
                case R.id.present:
                    break;
                case R.id.listen:
                    CallertunezItem item = (CallertunezItem) parent.get(position);
                    Tracks track = new Tracks(item.getTitle(), item.getThumbnail(), item.getAuthor(), new String[]{item.getAuthor()}, item.getPreview(), 1000, "");
                    List<Tracks> tracks = new ArrayList<Tracks>();
                    tracks.add(track);
                    ((BaseActivity) sender).getStreamingService().playFromList(tracks, 0);
                    break;
            }
        }
    }

    public class ParentHolder extends ParentViewHolder {
        @Bind(R.id.thumbnail)
        public SimpleDraweeView thumbnail;

        @Bind({android.R.id.title, android.R.id.summary})
        public List<TextView> texts;

        public ParentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
