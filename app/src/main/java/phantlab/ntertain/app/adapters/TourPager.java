package phantlab.ntertain.app.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;

public class TourPager extends PagerAdapter {
    private int[] thumbs = new int[]{R.drawable.ilustration_hello, R.drawable.ilusstration_browse, R.drawable.illustration_radio,
            R.drawable.illustration_gift};

    private String[] titles = new String[]{"Hello", "Browse", "Radio", "Gift A Song"};

    private int[] content = new int[]{R.string.hello_content, R.string.browse_content, R.string.radio_content, R.string.gift_content};

    private LayoutInflater mInflater;
    private Context mContext;

    @Bind(R.id.illustration)
    ImageView thumbnail;

    @Bind({R.id.heading, R.id.heading_content})
    List<TextView> texts;


    public TourPager(Context mContext) {
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View tour_view = mInflater.inflate(R.layout.tour, container, false);
        ButterKnife.bind(this, tour_view);
        thumbnail.setImageResource(thumbs[position]);
        texts.get(0).setText(titles[position]);
        texts.get(1).setText(mContext.getResources().getString(content[position]));
        container.addView(tour_view);
        return tour_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
