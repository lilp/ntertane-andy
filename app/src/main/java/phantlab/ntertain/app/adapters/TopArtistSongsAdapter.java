package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.Playlist;
import phantlab.ntertain.app.api.model.TopPlaylistCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Preconditions;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class TopArtistSongsAdapter extends RecyclerView.Adapter<TopArtistSongsAdapter.TopSongsHolder> {
    Activity sender;

    List<Tracks> data;
    Playlist playlist = new Playlist();
    boolean isThumbEnabled, isDark;

    public TopArtistSongsAdapter(Activity sender) {
        this.sender = sender;
    }

    public void setData(List<Tracks> data, boolean isThumbEnabled) {
        this.data = data;
        this.isThumbEnabled = isThumbEnabled;
        notifyDataSetChanged();
    }

    public void addItem(Tracks item) {
        if (data == null)
            data = new ArrayList<>();
        data.add(item);
        notifyItemInserted(data.size());
    }

    @Override
    public TopSongsHolder onCreateViewHolder(ViewGroup parent, int mViewType) {
        return new TopSongsHolder(LayoutInflater.from(parent.getContext())
                .inflate(isThumbEnabled ? R.layout.song_list_item : isDark ? R.layout.radio_music_item : R.layout.top_tracks_item, parent, false));
    }

    @Override
    public void onBindViewHolder(TopSongsHolder holder, int position) {
        Tracks track = data.get(position);

        holder.texts.get(0).setText(isDark ? track.getArtists()[0] : track.getTrack());
        if (!isDark)
            holder.texts.get(1).setText(track.getArtists()[0]);
        if (!isThumbEnabled)
            holder.texts.get(isDark ? 1 : 2).setText(String.valueOf(position + 1));
        else {
            if (track.getThumbnail() != null) {
                int width = Utils.convertDpToPixel(48);
                ThumbnailController.with(holder.thumbnail).dimens(width, width)
                        .uri(Uri.parse(track.getThumbnail()))
                        .load();
            }
            holder.playback.setText(Utils.format(track.getDuration()));
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? (isDark ? 15 : data.size()) : 0;
    }

    public void setDark() {
        isDark = true;
    }


    public class TopSongsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Nullable
        @Bind({android.R.id.title, android.R.id.summary, R.id.track_number})
        public List<TextView> texts;

        @Nullable
        @Bind(R.id.thumbnail)
        SimpleDraweeView thumbnail;

        @Nullable
        @Bind(R.id.playback)
        TextView playback;

        public TopSongsHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Tracks model = data.get(getLayoutPosition());
            if (isDark) {
                final ProgressDialog dialog = new ProgressDialog(sender);
                dialog.setMessage("Starting artist radio..");
                dialog.show();
                playlist.startRadio(model.getArtists()[0], new Callback<TopPlaylistCallback>() {
                    @Override
                    public void success(TopPlaylistCallback cb, Response response) {
                        Screen.mStreamingService.playFromList(cb.getPlaylist(), 0);
                        dialog.dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(sender, "Something went wrong.\nPlease try again later.", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                try {
                    Preconditions.checkTrack(model);
                    Screen.mStreamingService.playFromList(data, getLayoutPosition());
                } catch (Exception e) {
                    Toast.makeText(sender, "This song is not available", Toast.LENGTH_SHORT).show();
                }
            }
//            Utils.StartActivity(sender, PlayMusicActivity.class, Utils.getTrackData(model));
        }
    }
}