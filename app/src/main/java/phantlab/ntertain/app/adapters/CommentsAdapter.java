package phantlab.ntertain.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.model.Comment;

/**
 * Created by Ddev on 11/29/2015.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsHolder> {
    List<Comment> comments;
    Context context;

    public CommentsAdapter(Context context) {
        this.context = context;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }

    @Override
    public CommentsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.news_comments_items, parent, false));
    }

    @Override
    public void onBindViewHolder(CommentsHolder holder, int position) {
        Comment comment = comments.get(position);
        holder.texts.get(0).setText(comment.getUsername().toUpperCase().charAt(0) + "");
        holder.texts.get(1).setText(comment.getUsername());
        holder.texts.get(2).setText(comment.getContent());
        holder.texts.get(3).setText(DateUtils.getRelativeTimeSpanString(context, comment.getCreatedAt().getTime()));
    }

    @Override
    public int getItemCount() {
        return comments != null ? comments.size() : 0;
    }

    public void add(Comment comment) {
        comments.add(0, comment);
        notifyItemInserted(0);
    }


    public class CommentsHolder extends RecyclerView.ViewHolder {
        @Bind({R.id.thumbnail, R.id.username, R.id.comment, R.id.time})
        List<TextView> texts;

        public CommentsHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
