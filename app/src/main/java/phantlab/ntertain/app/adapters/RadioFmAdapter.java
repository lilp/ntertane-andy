package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.fragments.RadioFmFragment;
import phantlab.ntertain.app.radio.ChannelItem;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 06/12/15.
 */
public class RadioFmAdapter extends RecyclerView.Adapter<RadioFmAdapter.RadioFmHolder> implements ClickCallbacks.ClickCallback {
    Activity sender;
    int width, height;

    List<ChannelItem> data;
    RadioFmFragment fragment;

    public RadioFmAdapter(Activity sender, RadioFmFragment fragment) {
        this.sender = sender;
        this.fragment = fragment;
        width = Utils.convertDpToPixel(100);
        height = Utils.convertDpToPixel(124);
    }


    public void setData(List<ChannelItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public RadioFmHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RadioFmHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.radio_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(RadioFmHolder holder, int position) {
        ChannelItem item = data.get(position);
        ThumbnailController.with(holder.thumbnail).dimens(width, height)
                .uri(Uri.parse(item.getThumbnail())).load();
        holder.title.setText(item.getChannel());
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public void onClick(int position) {

    }

    public class RadioFmHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.thumbnail)
        public SimpleDraweeView thumbnail;
        @Bind(android.R.id.title)
        public TextView title;

        public RadioFmHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
            title.setTypeface(Utils.getFont(sender, false));
        }

        @Override
        public void onClick(View view) {
            fragment.play(data.get(getLayoutPosition()));
        }
    }
}
