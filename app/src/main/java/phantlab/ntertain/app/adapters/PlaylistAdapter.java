package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.databases.MyMusicDatabase;
import phantlab.ntertain.app.models.Playlist;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.viewholders.SongHolder;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public class PlaylistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    LayoutInflater inflater;
    List<Playlist> data;
    ClickCallbacks.OverflowCallback overflowCallback;

    ClickCallbacks.ClickCallback callback;

    MyMusicDatabase db;
    int width = Utils.convertDpToPixel(68);

    public void setCallback(ClickCallbacks.ClickCallback callback) {
        this.callback = callback;
    }

    boolean isDialog;

    public void setDialog() {
        isDialog = true;
    }

    public void setOverflowCallback(ClickCallbacks.OverflowCallback overflowCallback) {
        this.overflowCallback = overflowCallback;
    }

    public void setData(List<Playlist> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    Activity sender;

    public PlaylistAdapter(Activity sender, MyMusicDatabase db) {
        inflater = LayoutInflater.from(sender);
        this.sender = sender;
        this.db = db;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        if (isDialog)
            return new TextHolder(inflater.inflate(R.layout.text_item, parent, false));
        return new SongHolder(inflater.inflate(R.layout.playlist_item, parent, false), new ClickCallbacks.ClickCallback() {
            @Override
            public void onClick(int position) {
                callback.onClick(position);
            }
        }, overflowCallback);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vh, int position) {
        Playlist model = data.get(position);
        if (vh instanceof TextHolder) {
            TextHolder holder = (TextHolder) vh;
            holder.title.setText(model.getName());
        } else {
            SongHolder holder = (SongHolder) vh;
            holder.texts.get(0).setText(model.getName());
            String thumbnail = (String) holder.thumbnail.getTag();

            if (thumbnail == null) {
                List<Tracks> items = db.getPlaylistItems(model.getId());
                if (!items.isEmpty()) {
                    thumbnail = items.get(0).getThumbnail();
                    holder.thumbnail.setTag(thumbnail);
                }
            }

            if (thumbnail != null) {
                ThumbnailController.with(holder.thumbnail).from(sender)
                        .dimens(width, width)
                        .uri(Uri.parse(thumbnail))
                        .load();
            }

            holder.texts.get(1).setText(String.format(Locale.ENGLISH, "%d tracks", model.getTrackCount()));
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public class TextHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(android.R.id.title)
        TextView title;

        public TextHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            callback.onClick(getLayoutPosition());
        }
    }
}
