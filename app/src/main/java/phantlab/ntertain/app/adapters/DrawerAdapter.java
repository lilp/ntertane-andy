package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.fragments.Drawer;
import phantlab.ntertain.app.models.DrawerItems;
import phantlab.ntertain.app.viewholders.LinearHolder;
import phantlab.ntertain.app.viewholders.SeparatorHolder;

public class DrawerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ClickCallbacks.ClickCallback {
    private List<DrawerItems> data;
    private Activity sender;
    private Drawer.DrawerCallbacks callbacks;
    View indicator;

    enum ItemTypes {
        SEPARATOR(0),
        OPTION(1);

        ItemTypes(int position) {/***/}
    }

    public DrawerAdapter(Activity sender, Drawer.DrawerCallbacks callbacks, View indicator) {
        this.sender = sender;
        this.callbacks = callbacks;
        this.indicator = indicator;
    }

    public void setData(List<DrawerItems> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) == null)
            return ItemTypes.SEPARATOR.ordinal();
        return ItemTypes.OPTION.ordinal();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        if (type == 0)
            return new SeparatorHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_divider, parent, false));
        return new LinearHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drawer, parent, false), this).setIndicator(indicator);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mViewHolder, int position) {
        DrawerItems model = data.get(position);
        if (model != null) {
            LinearHolder holder = (LinearHolder) mViewHolder;
            holder.title.setText(model.getTitle());
            holder.icon.setImageResource(model.getIcon());
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public void onClick(int position) {
        switch (position){
            case 0: callbacks.onHome();
                break;
            case 1: callbacks.onMusic();
                break;
            case 3: callbacks.onMovies();
                break;
            case 4: callbacks.onRadio();
                break;
            case 5: callbacks.onNews();
                break;
            case 7: callbacks.onCallertunez();
                break;
            case 8: callbacks.onMyMusic();
                break;
//            case 9: callbacks.onSendSong();
//                break;
            case 10: callbacks.onSettings();
                break;
        }
    }
}
