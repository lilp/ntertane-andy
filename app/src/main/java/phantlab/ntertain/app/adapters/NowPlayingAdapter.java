package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.MovieDetails;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/28/2015.
 */
public class NowPlayingAdapter extends RecyclerView.Adapter<NowPlayingAdapter.NowPlayingHolder> {
    boolean isFeatured;
    AdapterType type;
    private List<MovieMeta> data;
    private Activity sender;

    public NowPlayingAdapter(Activity sender, boolean isFeatured) {
        this.sender = sender;
        this.isFeatured = isFeatured;
    }

    public void setType(AdapterType type) {
        this.type = type;
    }

    public void setData(List<MovieMeta> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public NowPlayingHolder onCreateViewHolder(ViewGroup parent, int mViewType) {
        return new NowPlayingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    @Override
    public int getItemCount() {
        return data != null ? isFeatured ? data.size() > 0 ? Math.min(3, data.size()) : 0 : data.size() : 0;
    }

    @Override
    public void onBindViewHolder(NowPlayingHolder holder, int position) {
        MovieMeta meta = data.get(position);
        List<String> model = meta.getData();
        holder.texts.get(0).setText(type.equals(AdapterType.ComingSoon) ? meta.getTitle() : model.get(0));
        holder.texts.get(1).setText(type.equals(AdapterType.ComingSoon) ? meta.getGenre() : model.get(2));
        ThumbnailController.with(holder.thumbnail)
                .uri(Uri.parse(type.equals(AdapterType.ComingSoon) ? meta.getThumb() : model.get(5)))
                .dimens(150, 200)
                .load();
    }

    public enum AdapterType {
        NowPlaying,
        ComingSoon
    }

    public class NowPlayingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind({android.R.id.title, android.R.id.summary})
        public List<TextView> texts;
        @Bind(R.id.thumbnail)
        public SimpleDraweeView thumbnail;

        public NowPlayingHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View layout) {
            if (!type.equals(AdapterType.ComingSoon)) {
                MovieMeta model = data.get(getLayoutPosition());
                Utils.StartActivity(sender, MovieDetails.class, Utils.getMovieBundle(model));
            }
        }
    }
}
