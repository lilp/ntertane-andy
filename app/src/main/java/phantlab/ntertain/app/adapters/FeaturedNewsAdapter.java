package phantlab.ntertain.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import phantlab.ntertain.app.R;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class FeaturedNewsAdapter extends RecyclerView.Adapter<FeaturedNewsAdapter.TitleHolder> {
    @Override
    public TitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TitleHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.featured_news, parent, false));
    }

    @Override
    public void onBindViewHolder(TitleHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class TitleHolder extends RecyclerView.ViewHolder {
        public TitleHolder(View view) {
            super(view);
        }
    }
}
