package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.NewsDetails;
import phantlab.ntertain.app.news.NewsItem;
import phantlab.ntertain.app.news.NewsUtils;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/29/2015.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsItemHolder> {
    List<NewsItem> data = new ArrayList<>();
    private int width = -1;
    private Activity sender;

    public NewsAdapter(Activity sender) {
        width = Utils.convertDpToPixel(28);
        this.sender = sender;
    }

    public void setData(List<NewsItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public NewsItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.news_items, parent, false));
    }

    @Override
    public void onBindViewHolder(NewsItemHolder holder, int position) {
        NewsItem model = data.get(position);
        String mNewsThumb = NewsUtils.get().get(model.getSource());

        holder.texts.get(0).setText(model.getTitle());
        holder.texts.get(1).setText(model.getSource().name());
        holder.texts.get(2).setText(model.getTimestamp());

        Utils.ellipsis(holder.texts.get(0));

        ThumbnailController.with(holder.newSource)
                .uri(Uri.parse(mNewsThumb))
                .dimens(width, width).load();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addItem(NewsItem item) {
        data.add(item);
        notifyItemInserted(data.size());
    }

    public class NewsItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind({R.id.heading, android.R.id.title, android.R.id.summary})
        List<TextView> texts;


        @Bind(R.id.news_thumb)
        SimpleDraweeView newSource;

        public NewsItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            NewsItem model = data.get(getLayoutPosition());
            Utils.StartActivity(sender, NewsDetails.class, Utils.getNewsBundle(model));
        }
    }
}