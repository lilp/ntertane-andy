package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.AlbumDetails;
import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class TopArtistAlbumsAdapter extends RecyclerView.Adapter<TopArtistAlbumsAdapter.AlbumHolder> {
    Activity sender;

    List<Albums> data;
    String artist;

    public TopArtistAlbumsAdapter(Activity sender, String artist) {
        this.sender = sender;
        this.artist = artist;
    }

    public void setData(List<Albums> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public AlbumHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlbumHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(AlbumHolder holder, int position) {
        Albums album = data.get(position);
        holder.texts.get(0).setText(album.getName());
        holder.texts.get(1).setText(String.format("%d songs", album.getTrackCount()));
        holder.texts.get(2).setText("2016");

        int width = Utils.convertDpToPixel(178),
                height = Utils.convertDpToPixel(148);
        if (album.getThumbnail() != null)
            ThumbnailController.with(holder.thumbnail)
                    .dimens(width, height)
                    .uri(Uri.parse(album.getThumbnail()))
                    .load();
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


    public class AlbumHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind({android.R.id.title, android.R.id.summary, R.id.year})
        public List<TextView> texts;

        @Bind(R.id.thumbnail)
        SimpleDraweeView thumbnail;

        public AlbumHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Albums album = data.get(getLayoutPosition());
            Utils.StartActivity(sender, AlbumDetails.class, Utils.getAlbumData(album, artist, false));
        }
    }
}