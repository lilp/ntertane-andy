package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.AlbumDetails;
import phantlab.ntertain.app.activities.ArtistActivity;
import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.api.model.Artist;
import phantlab.ntertain.app.api.model.ResultItem;
import phantlab.ntertain.app.api.model.ResultType;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.viewholders.SearchHeaderHolder;
import phantlab.ntertain.app.viewholders.SongHolder;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ClickCallbacks.ClickCallback {
    private final int ITEM_HEADER = 0;
    private final int ITEM_ARTIST = 3;
    private final int ITEM_ALBUMS = 4;
    private final int ITEM_TRACKS = 5;

    List<ResultItem> data;
    Activity sender;
    private int forty_eight;

    public SearchAdapter(Activity sender) {
        this.sender = sender;
        forty_eight = Utils.convertDpToPixel(48);
    }


    public void setData(List<ResultItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        ResultItem model = data.get(position);
        if (model.getType() == null)
            return ITEM_HEADER;
        else if (model.getType().equals(ResultType.ALBUM))
            return ITEM_ALBUMS;
        else if (model.getType().equals(ResultType.ARTIST))
            return ITEM_ARTIST;
        else return ITEM_TRACKS;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (type == ITEM_HEADER)
            return new SearchHeaderHolder(inflater.inflate(R.layout.search_result_header, parent, false));
        else
            return new SongHolder(inflater.inflate(R.layout.song_list_item, parent, false), SearchAdapter.this, null);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        ResultItem model = data.get(position);

        if (isHeader(position)) {
            SearchHeaderHolder header = (SearchHeaderHolder) holder;
            header.title.setText(model.getHeader());
        } else if (type == ITEM_TRACKS) {
            SongHolder song = (SongHolder) holder;
            Tracks item = data.get(position).getTrackItem();
            song.texts.get(0).setText(item.getTrack());
            song.texts.get(1).setText(item.getArtists()[0]);
            song.texts.get(2).setText(Utils.format(item.getDuration()));

            if (item.getThumbnail() != null)
                loadThumb(Uri.parse(item.getThumbnail()), song.thumbnail);
        } else if (type == ITEM_ARTIST) {
            SongHolder artist = (SongHolder) holder;
            Artist item = data.get(position).getArtistItem();
            artist.texts.get(0).setText(item.getName());
            artist.texts.get(1).setText(String.format("%d albums - %d songs", item.getAlbumCount(),
                    item.getTrackCount()));
            artist.texts.get(2).setVisibility(View.GONE);
            if (item.getThumbnail() != null)
                loadThumb(Uri.parse(item.getThumbnail()), artist.thumbnail);
        } else {
            SongHolder album = (SongHolder) holder;
            Albums item = data.get(position).getAlbumItem();
            album.texts.get(0).setText(item.getName());
            album.texts.get(1).setText(String.format("%d songs", item.getTrackCount()));
            album.texts.get(2).setVisibility(View.GONE);
            if (item.getThumbnail() != null)
                loadThumb(Uri.parse(item.getThumbnail()), album.thumbnail);
        }
    }

    private void loadThumb(Uri uri, SimpleDraweeView thumbnail) {
        ThumbnailController.with(thumbnail).uri(uri)
                .dimens(forty_eight, forty_eight).load();
    }

    private boolean isHeader(int position) {
        return data.get(position).getType() == null;
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public void onClick(int position) {
        int type = getItemViewType(position);
        ResultItem model = data.get(position);
        if (type == ITEM_ARTIST) {
            Artist artist = model.getArtistItem();
            Utils.StartActivity(sender, ArtistActivity.class, Utils.getArtistData(artist, false));
        } else if (type == ITEM_TRACKS) {
            Tracks track = model.getTrackItem();
//            Utils.StartActivity(sender, PlayMusicActivity.class, Utils.getTrackData(track));
            List<Tracks> tracks = new ArrayList<>();
            tracks.add(track);
            ((Screen) sender).mStreamingService.playFromList(tracks, 0);
        } else if (type == ITEM_ALBUMS) {
            Albums album = model.getAlbumItem();
            Utils.StartActivity(sender, AlbumDetails.class, Utils.getAlbumData(album, album.getArtist(), false));
        }
    }
}
