package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.SongListActivity;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.viewholders.StackHolder;

/**
 * Created by kartikeyasingh on 06/12/15.
 */
public class SendSongAdapter extends RecyclerView.Adapter<StackHolder> implements ClickCallbacks.ClickCallback {
    Activity activity;
    int itemCount = -1;
    String title;

    public SendSongAdapter(Activity activity) {
        this.activity   = activity;
    }

    public SendSongAdapter setItemCount(int itemCount) {
        this.itemCount = itemCount;
        return this;
    }

    public SendSongAdapter setTitle(String title) {
        this.title = title;
        return this;
    }

    @Override
    public StackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StackHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.stack_items, parent, false), this);
    }

    @Override
    public void onBindViewHolder(StackHolder holder, int position) {
        if (title != null)
            holder.title.setText("Playlist Name");
    }

    @Override
    public int getItemCount() {
        return itemCount == -1 ? 8 : itemCount;
    }

    @Override
    public void onClick(int position) {
        Utils.StartActivity(activity, SongListActivity.class);
    }
}
