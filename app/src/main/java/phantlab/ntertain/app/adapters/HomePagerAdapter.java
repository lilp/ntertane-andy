package phantlab.ntertain.app.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.ui.processors.BlurPostprocessor;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class HomePagerAdapter extends PagerAdapter {
    Context context;

    @Bind({R.id.song_name, R.id.artist_title})
    List<TextView> texts;


    @Bind({R.id.blur, R.id.thumbnail})
    List<SimpleDraweeView> thumbnails;

    List<Tracks> tracks;
    View.OnClickListener listener;

    public void setData(List<Tracks> tracks) {
        this.tracks = tracks;
        notifyDataSetChanged();
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public HomePagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return tracks != null ? 3 : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View layout = LayoutInflater.from(context).inflate(R.layout.home_pager_items, container, false);
        ButterKnife.bind(this, layout);
        Tracks track = tracks.get(position);
        texts.get(0).setText(track.getTrack());
        texts.get(1).setText(track.getArtists()[0]);
        setImage(track.getThumbnail());
        layout.setOnClickListener(listener);
        layout.setTag(position);
        container.addView(layout);
        return layout;
    }

    private void setImage(String thumb) {
        Uri uri = Uri.parse(thumb);
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setPostprocessor(new BlurPostprocessor(context, 25))
                .build();

        PipelineDraweeController controller = (PipelineDraweeController)
                Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(thumbnails.get(0).getController())
                        .build();
        thumbnails.get(0).setController(controller);

        int width = Utils.convertDpToPixel(76);

        ThumbnailController.with(thumbnails.get(1))
                .uri(uri).dimens(width, width).load();
    }


    public static class Items {
        String song, artist, song_thumb;

        public Items(String song, String artist, String song_thumb) {
            this.song = song;
            this.artist = artist;
            this.song_thumb = song_thumb;
        }

        public String getArtist() {
            return artist;
        }

        public String getSong() {
            return song;
        }

        public String getSongThumb() {
            return song_thumb;
        }

    }
}
