package phantlab.ntertain.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.models.FeaturedItems;
import phantlab.ntertain.app.viewholders.MusicHolder;
import phantlab.ntertain.app.viewholders.RadioHolder;

/**
 * Created by kartikeyasingh on 03/12/15.
 */
public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<FeaturedItems> data;
    FeaturedType type;

    public HomeAdapter(FeaturedType type) {
        this.type = type;
    }

    public void setData(List<FeaturedItems> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int view) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (type == FeaturedType.RADIO) {
            return new RadioHolder(inflater.inflate(R.layout.item, parent, false));
        } else if (type == FeaturedType.MUSIC)
            return  new MusicHolder(inflater.inflate(R.layout.music_item, parent, false));
        return new MoviesHolder(inflater.inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return data != null ? 3 : 0;
    }

    public enum FeaturedType {
        RADIO(0),
        MUSIC(1),
        MOVIE(2);

        FeaturedType(int ignored) {/**/}
    }

    public class MoviesHolder extends RecyclerView.ViewHolder {
        @Bind({android.R.id.title, android.R.id.summary})
        public List<TextView> texts;
        @Bind(R.id.thumbnail)
        public SimpleDraweeView thumbnail;

        public MoviesHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
