package phantlab.ntertain.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Ddev on 11/29/2015.
 */
public abstract class NtertainAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private List<Class<T>> data;

    public void setData() {
        this.data = data;
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int mViewType) {
        return getHolder(parent, mViewType);
    }

    @Override
    public void onBindViewHolder(T holder, int position) {
        onBindData(holder, position);
    }

    public abstract void onBindData(T holder, int position);

    public abstract T getHolder(ViewGroup parent, int type);

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }
}
