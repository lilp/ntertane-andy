package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.viewholders.SongHolder;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public class MyAlbumsAdapter extends RecyclerView.Adapter<SongHolder> implements ClickCallbacks.ClickCallback {
    LayoutInflater inflater;
    List<Albums> data;

    public void setData(List<Albums> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public MyAlbumsAdapter(Activity sender){
        inflater = LayoutInflater.from(sender);
    }

    @Override
    public SongHolder onCreateViewHolder(ViewGroup parent, int type) {
        return new SongHolder(inflater.inflate(R.layout.playlist_item, parent, false), this, null);
    }

    @Override
    public void onBindViewHolder(SongHolder holder, int position) {
        Albums model = data.get(position);
        holder.texts.get(0).setText(model.getName());
        holder.texts.get(1).setText(String.format("%d tracks", model.getTrackCount()));
    }

    @Override
    public int getItemCount() {
        return data !=null ? data.size() : 0;
    }

    @Override
    public void onClick(int position) {

    }
}
