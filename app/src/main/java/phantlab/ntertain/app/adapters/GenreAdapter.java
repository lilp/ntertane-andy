package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.SongListActivity;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 06/12/15.
 */
public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenreHolder> implements ClickCallbacks.ClickCallback {
    Activity activity;

    private String[] titles = new String[]{"Party", "Focus", "Study", "EDM/Dance",
            "Dinner", "Sleep", "Hip Hop", "Travel", "decades", "Reggae"};

    private int[] icon = new int[]{R.drawable.icon_party, R.drawable.icon_focus, R.drawable.icon_study,
            R.drawable.icon_edm, R.drawable.icon_dinner, R.drawable.icon_sleep, R.drawable.icon_hiphop,
            R.drawable.icon_travel, R.drawable.icon_decades, R.drawable.icon_reggae};

    private int[] bg   = new int[]{R.drawable.party_background, R.drawable.focus_background,
            R.drawable.study_background, R.drawable.edm_background, R.drawable.dinner_background,
            R.drawable.sleep_background, R.drawable.hiphop_background, R.drawable.travel_background,
            R.drawable.decades_background, R.drawable.reggae_background};

    public GenreAdapter(Activity activity) {
        this.activity   = activity;
    }

    @Override
    public GenreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GenreHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.music_genre_mood_items, parent, false));
    }

    @Override
    public void onBindViewHolder(GenreHolder holder, int position) {
        holder.thumbs.get(0).setImageResource(icon[position]);
        holder.thumbs.get(1).setImageResource(bg[position]);
        holder.title.setText(titles[position]);
    }

    @Override
    public int getItemCount() {
        return 10;
    }


    public class GenreHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind({R.id.genre_icon, R.id.thumbnail})
        List<ImageView> thumbs;

        @Bind(android.R.id.title)
        TextView title;

        public GenreHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
    @Override
    public void onClick(int position) {
        Utils.StartActivity(activity, SongListActivity.class);
    }
}
