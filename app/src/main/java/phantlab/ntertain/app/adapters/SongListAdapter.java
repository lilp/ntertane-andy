package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.BuyActivity;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;
import phantlab.ntertain.app.viewholders.SongHolder;

/**
 * Created by kartikeyasingh on 06/12/15.
 */
public class SongListAdapter extends RecyclerView.Adapter<SongHolder> implements ClickCallbacks.ClickCallback {
    Activity sender;

    boolean is_showcase;

    public SongListAdapter(Activity sender, boolean is_showcase) {
        this.sender = sender;
        this.is_showcase = is_showcase;
    }

    @Override
    public SongHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SongHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.song_list_item, parent, false), new ClickCallbacks.ClickCallback() {
            @Override
            public void onClick(int position) {
                Utils.StartActivity(sender, BuyActivity.class);
            }
        }, null);
    }

    @Override
    public void onBindViewHolder(SongHolder holder, int position) {
        int width = Utils.convertDpToPixel(48);

        ThumbnailController.with(holder.thumbnail).dimens(width, width)
                .uri(Uri.parse("http://imb.ulximg.com/image/300x300/artist/1392659304_2a631c7878cbd5d1679d3c1bcae20a5f.jpg/c6c0833ae5b7e5c3f17e90d55b6f5c2e/1392659304_lil_wayne_fear_god_69.jpg"))
                .load();
    }

    @Override
    public int getItemCount() {
        return is_showcase ? 3 : 12;
    }

    @Override
    public void onClick(int position) {

    }


}
