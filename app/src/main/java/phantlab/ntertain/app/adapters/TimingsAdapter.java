package phantlab.ntertain.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.Geocode;
import phantlab.ntertain.app.api.model.DistanceCallback;
import phantlab.ntertain.app.application.UserUtils;
import phantlab.ntertain.app.movies.NtertainLocation;
import phantlab.ntertain.app.movies.Shows;
import phantlab.ntertain.app.ui.NowShowingTimeLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 11/28/2015.
 */
public class TimingsAdapter extends RecyclerView.Adapter<TimingsAdapter.ShowingAtHolder> {
    private List<Shows> data;
    Geocode geocode = new Geocode();
    NtertainLocation mLocation;
    Activity sender;

    public TimingsAdapter(Activity sender) {
        this.sender = sender;
        mLocation = new NtertainLocation(sender, sender.getSharedPreferences(UserUtils.NTERTANE_PREFS,
                Context.MODE_PRIVATE));
    }

    @Override
    public TimingsAdapter.ShowingAtHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShowingAtHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.showing_at_item, parent, false));
    }

    public void setData(List<Shows> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    class ShowingAtHolder extends RecyclerView.ViewHolder {
        @Bind({R.id.cinema_name, R.id.cinema_adress, R.id.distance})
        List<TextView> texts;

        @Bind(R.id.timings)
        NowShowingTimeLayout timings;

        public ShowingAtHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onBindViewHolder(final TimingsAdapter.ShowingAtHolder holder, int position) {
        final Shows model = data.get(position);
        holder.texts.get(0).setText(model.getTitle());
        holder.texts.get(1).setText(model.getAddress());
        holder.timings.setData(model.getShows());

        if (mLocation.getLatitude() != 0.0) {
            TextView distance = holder.texts.get(2);
            String dis = (String) distance.getTag();
            if (dis != null)
                distance.setText(dis);
            else
                bindDistance(distance, "", model.getAddress());

            distance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View distance) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse(String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%s&daddr=%s",
                                    getFormattedLocation(), model.getAddress())));
                    sender.startActivity(intent);
                }
            });
        }
    }

    String getFormattedLocation() {
        return mLocation.getLatitude() + "," + mLocation.getLongitude();
    }

    void bindDistance(final TextView tv, String lat_lng, String address) {
        new Geocode().getDistance(getFormattedLocation(), address, new Callback<DistanceCallback>() {
            @Override
            public void success(DistanceCallback cb, Response response) {
                DistanceCallback.Elements elems = cb.getRows().get(0).getElements().get(0);
                if (elems.success()) {
                    String distance = elems.getDistance().toString();
                    tv.setTag(distance);
                    tv.setText(distance);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.i("$error", error.getMessage());

            }
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }
}
