package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.MyAlbumsAdapter;
import phantlab.ntertain.app.api.model.Albums;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public class MyAlbumFragment extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;


    private MyAlbumsAdapter adapter;


    @Override
    int getLayout() {
        return R.layout.rv;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        list.setLayoutManager(new LinearLayoutManager(sender));

        adapter = new MyAlbumsAdapter(sender);
        list.setAdapter(adapter);

        List<Albums> albums = new ArrayList<>();

        albums.add(new Albums(0, 5, 0, "Dark Sky Paradise", "http://imb.ulximg.com/image/300x300/artist/1392659304_2a631c7878cbd5d1679d3c1bcae20a5f.jpg/c6c0833ae5b7e5c3f17e90d55b6f5c2e/1392659304_lil_wayne_fear_god_69.jpg",
                "Lil weezy"));

        albums.add(new Albums(0, 5, 0, "Sorry for the wait", "http://imb.ulximg.com/image/300x300/artist/1392659304_2a631c7878cbd5d1679d3c1bcae20a5f.jpg/c6c0833ae5b7e5c3f17e90d55b6f5c2e/1392659304_lil_wayne_fear_god_69.jpg",
                "Lil weezy"));

        albums.add(new Albums(0, 5, 0, "Sorry for the wait 2", "http://imb.ulximg.com/image/300x300/artist/1392659304_2a631c7878cbd5d1679d3c1bcae20a5f.jpg/c6c0833ae5b7e5c3f17e90d55b6f5c2e/1392659304_lil_wayne_fear_god_69.jpg",
                "Lil weezy"));

        albums.add(new Albums(0, 5, 0, "FWA", "http://imb.ulximg.com/image/300x300/artist/1392659304_2a631c7878cbd5d1679d3c1bcae20a5f.jpg/c6c0833ae5b7e5c3f17e90d55b6f5c2e/1392659304_lil_wayne_fear_god_69.jpg",
                "Lil weezy"));

        adapter.setData(albums);
    }
}
