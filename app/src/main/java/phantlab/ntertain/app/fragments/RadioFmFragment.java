package phantlab.ntertain.app.fragments;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.RadioFmAdapter;
import phantlab.ntertain.app.bus.BusProvider;
import phantlab.ntertain.app.bus.PlayBackStateChangedEvent;
import phantlab.ntertain.app.dialogs.NoMusicDialog;
import phantlab.ntertain.app.music.NtertainPlayer;
import phantlab.ntertain.app.radio.ChannelItem;
import phantlab.ntertain.app.radio.Channels;
import phantlab.ntertain.app.radio.Radio;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 18/12/15.
 */
public class RadioFmFragment extends BaseFragment implements ExoPlayer.Listener {
    @Bind(R.id.list)
    RecyclerView list;

    @Bind(R.id.channel_name)
    TextView channel;

    @Bind(R.id.thumbnail)
    SimpleDraweeView thumbnail;

    boolean mIsPaused = false;
    @Bind(R.id.panel)
    LinearLayout panel;
    NtertainPlayer player;
    Animation slide, slide_down;
    int width;

    @Bind(R.id.play)
    ImageButton play;

    @OnClick(R.id.play)
    public void onPlay() {
        if (mIsPaused) {
            play.setImageResource(R.drawable.ic_pause_white_24dp);
            mIsPaused = false;
            player.resume();
        } else {
            play.setImageResource(R.drawable.ic_play_arrow_white_24dp);
            mIsPaused = true;
            player.pause();
        }
    }

    @Override
    int getLayout() {
        return R.layout.radio_fm_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        list.setLayoutManager(new GridLayoutManager(sender, 3));
        RadioFmAdapter adapter = new RadioFmAdapter(sender, this);
        list.setAdapter(adapter);
        adapter.setData(getData());
        slide = AnimationUtils.loadAnimation(sender, R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(sender, R.anim.slide_down);
        width = Utils.convertDpToPixel(48);
        play.setEnabled(false);
        channel.setTypeface(Utils.getFont(sender, false));
    }

    public void play(ChannelItem item) {
        if (player == null) {
            player = new NtertainPlayer();
            player.init(sender);
        }
        player.setListener(this);
        player.stream(item.getUri());
        BusProvider.getInstance().post(new PlayBackStateChangedEvent(PlayBackStateChangedEvent.State.Stop, null));
        togglePlayer(item);
    }

    private void togglePlayer(ChannelItem item) {
        if (panel.getVisibility() == View.GONE) {
            slide.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    panel.setVisibility(View.VISIBLE);
                    list.setPadding(0, 0, 0, Utils.convertDpToPixel(70));
                }

                @Override
                public void onAnimationEnd(Animation animation) {/****/}

                @Override
                public void onAnimationRepeat(Animation animation) {/****/}
            });

            slide_down.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {/***/}

                @Override
                public void onAnimationEnd(Animation animation) {
                    list.setPadding(0, 0, 0, 0);
                    panel.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {/***/}
            });
            panel.startAnimation(slide);
        }

        ThumbnailController.with(thumbnail).dimens(width, width)
                .uri(Uri.parse(item.getThumbnail())).load();
        channel.setText(item.getChannel());
    }

    private List<ChannelItem> getData() {
        List<ChannelItem> data = new ArrayList<>();
        data.add(new ChannelItem("102.7 Naija Fm", "http://cdn-radiotime-logos.tunein.com/s134094q.png", Radio.getLiveStreamUri(Channels.NAIJAFM), Channels.NAIJAFM));
        data.add(new ChannelItem("234Radio", "http://cdn-radiotime-logos.tunein.com/s200605q.png", Radio.getLiveStreamUri(Channels.RADIOTWOTHIRTYFOUR), Channels.RADIOTWOTHIRTYFOUR));
        data.add(new ChannelItem("Classic Fm 97.3", "http://cdn-radiotime-logos.tunein.com/s134095q.png", Radio.getLiveStreamUri(Channels.CLASSICFM), Channels.CLASSICFM));
        data.add(new ChannelItem("Cool FM 96.9 Lagos", "http://cdn-radiotime-logos.tunein.com/s16083q.png", Radio.getLiveStreamUri(Channels.COOLFM), Channels.COOLFM));
        data.add(new ChannelItem("Inspiration 92.3 Fm", "http://cdn-radiotime-logos.tunein.com/s165872q.png", Radio.getLiveStreamUri(Channels.INSPIRATION), Channels.INSPIRATION));
        data.add(new ChannelItem("Radio Continental", "http://cdn-radiotime-logos.tunein.com/s181442q.png", Radio.getLiveStreamUri(Channels.RADIOCONTINENTAL), Channels.RADIOCONTINENTAL));
        data.add(new ChannelItem("Rainbow 94.1 Fm", "http://cdn-radiotime-logos.tunein.com/s224186q.png", Radio.getLiveStreamUri(Channels.RAINBOW), Channels.RAINBOW));
        data.add(new ChannelItem("Smooth 98.1 Fm", "http://cdn-radiotime-logos.tunein.com/s135948q.png", Radio.getLiveStreamUri(Channels.SMOOTH), Channels.SMOOTH));
        data.add(new ChannelItem("The Beat", "http://cdn-radiotime-logos.tunein.com/s131677q.png", Radio.getLiveStreamUri(Channels.THEBEAT), Channels.THEBEAT));
        data.add(new ChannelItem("Top Radio", "http://cdn-radiotime-logos.tunein.com/s176363q.png", Radio.getLiveStreamUri(Channels.TOPRADIO), Channels.TOPRADIO));
        data.add(new ChannelItem("Wazobia FM 95.1 Lagos", "http://cdn-radiotime-logos.tunein.com/s109503q.png", Radio.getLiveStreamUri(Channels.WAZOBIALAGOS), Channels.WAZOBIALAGOS));
        data.add(new ChannelItem("HOT FM", "http://cdn-radiotime-logos.tunein.com/s105732q.png", Radio.getLiveStreamUri(Channels.HOTFM), Channels.HOTFM));
        return data;
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_READY) {
            if (!play.isEnabled())
                play.setEnabled(true);
        }
    }

    @Override
    public void onPlayWhenReadyCommitted() {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        final NoMusicDialog dialog = new NoMusicDialog();
        dialog.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface di, int position) {
                dialog.dismiss();
                panel.startAnimation(slide_down);
            }
        }).setMessage("An error occurred while streaming, maybe try another one (Dj Khaled's voice)")
                .show(getChildFragmentManager(), "$no_radio");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null)
            player.release();
    }
}
