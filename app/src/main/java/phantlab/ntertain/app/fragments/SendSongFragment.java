package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.SendSongAdapter;

/**
 * Created by kartikeyasingh on 06/12/15.
 */
public class SendSongFragment extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;

    SendSongAdapter adapter;


    @Override
    int getLayout() {
        return R.layout.send_song_main;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        list.setLayoutManager(new GridLayoutManager(sender ,3));
        adapter = new SendSongAdapter(sender);
        list.setAdapter(adapter);
    }
}
