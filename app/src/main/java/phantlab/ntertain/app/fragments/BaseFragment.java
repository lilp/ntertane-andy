package phantlab.ntertain.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by Ddev on 11/27/2015.
 */
public abstract class BaseFragment extends Fragment {
    public Activity sender;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View layout = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, layout);
        sender = getActivity();
        return layout;
    }

    abstract int getLayout();
}
