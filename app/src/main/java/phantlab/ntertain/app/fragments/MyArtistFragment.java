package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.model.Tracks;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public class MyArtistFragment extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;


    private TopArtistSongsAdapter adapter;

    @Override
    int getLayout() {
        return R.layout.rv;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        list.setLayoutManager(new LinearLayoutManager(sender));

        adapter = new TopArtistSongsAdapter(sender);
        list.setAdapter(adapter);

        List<Tracks> tracks = new ArrayList<>();

        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));
        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));
        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));
        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));
        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));
        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));
        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));
        tracks.add(new Tracks("Lil wayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png",
                "Sftw2", new String[]{"12 Songs"}, null, 20000, ""));


        adapter.setData(tracks, true);
    }
}
