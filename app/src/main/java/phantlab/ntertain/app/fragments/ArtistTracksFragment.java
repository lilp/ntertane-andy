package phantlab.ntertain.app.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.ArtistActivity;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.Playlist;
import phantlab.ntertain.app.api.model.ArtistItem;
import phantlab.ntertain.app.api.model.TopPlaylistCallback;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class ArtistTracksFragment extends BaseFragment {
    @Bind({R.id.artist_title, R.id.song_count, R.id.album_count})
    public List<TextView> texts;
    @Bind({R.id.blur_bg, R.id.thumbnail})
    List<SimpleDraweeView> images;
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.top_progress)
    Progress progress;
    TopArtistSongsAdapter adapter;
    private String artist, thumbnail;
    private int album_count, tracks_count, id;
    private boolean isFromTrack;
    int width = Utils.convertDpToPixel(76);

    @Override
    int getLayout() {
        return R.layout.artists_top_tracks;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        setValues();


        adapter = new TopArtistSongsAdapter(sender);
        list.setLayoutManager(new LinearLayoutManager(sender));
        list.setAdapter(adapter);

        texts.get(0).setText(artist);
        setUpArtist();

        new Playlist().startRadio(artist, new Callback<TopPlaylistCallback>() {
            @Override
            public void success(TopPlaylistCallback result, Response response) {
                if (result.isSuccessFull()) {
                    progress.stop();
                    progress.setVisibility(View.GONE);
                    adapter.setData(result.getPlaylist(), false);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                progress.stop();
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void setUpArtist() {
        texts.get(1).setText((tracks_count == -1 && isFromTrack) ? "Loading..." : String.format("%d Songs", tracks_count));
        texts.get(2).setText((album_count == -1 && isFromTrack) ? "Loading..." : String.format("%d Albums", album_count));

        if (thumbnail != null) {
            ThumbnailController.with(images.get(0)).from(sender)
                    .uri(Uri.parse(thumbnail))
                    .blur(18);

            ThumbnailController.with(images.get(1))
                    .uri(Uri.parse(thumbnail))
                    .dimens(width, width).load();
        }
    }

    private void setValues() {
        Bundle data = getArguments();
        if (data != null) {
            artist = data.getString(ArtistActivity.KEY_ARTIST);
            thumbnail = data.getString(ArtistActivity.KEY_THUMBNAIL);
            id = data.getInt(ArtistActivity.KEY_ARTIST_ID);
            album_count = data.getInt(ArtistActivity.KEY_ALBUM_COUNT, -1);
            tracks_count = data.getInt(ArtistActivity.KEY_SONG_COUNT, -1);
            isFromTrack = data.getBoolean(ArtistActivity.KEY_IS_FROM_TRACK);
        }
    }

    public void notifyCount() {
        tracks_count = ((ArtistActivity) sender).getTracks().size();
        album_count = ((ArtistActivity) sender).getAlbums().size();
        ArtistItem artist = ((ArtistActivity) sender).getArtistInfo();
        id = artist.getId();
        thumbnail = artist.getThumbnail();
        getArguments().putString(ArtistActivity.KEY_THUMBNAIL, thumbnail);
        getArguments().putInt(ArtistActivity.KEY_ARTIST_ID, id);
        getArguments().putInt(ArtistActivity.KEY_SONG_COUNT, tracks_count);
        getArguments().putInt(ArtistActivity.KEY_ALBUM_COUNT, album_count);
        setUpArtist();
    }
}
