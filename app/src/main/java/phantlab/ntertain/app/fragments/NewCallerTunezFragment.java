package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.CallertuneAdapter;
import phantlab.ntertain.app.api.Callertunez;
import phantlab.ntertain.app.api.model.CallertunezItem;
import phantlab.ntertain.app.ui.Progress;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 11/12/15.
 */
public class NewCallerTunezFragment extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.callertune_progress)
    Progress progress;

    @Override
    int getLayout() {
        return R.layout.callertunez_expandable;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        list.setLayoutManager(new LinearLayoutManager(sender));

        new Callertunez().getAllTunes(new Callback<Response>() {
            @Override
            public void success(Response cb, Response response) {
                try {
                    BufferedReader reader = null;
                    StringBuilder sb = new StringBuilder();
                    try {
                        reader = new BufferedReader(new InputStreamReader(cb.getBody().in()));
                        String line;
                        try {
                            while ((line = reader.readLine()) != null) {
                                sb.append(line);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String result = sb.toString();
                    List<ParentObject> data = new ArrayList<>();
                    JSONArray data_array = new JSONArray(result);
                    for (int i = 0; i < data_array.length(); i++) {
                        JSONObject item = data_array.getJSONObject(i);
                        String title = item.getString("Title"),
                                author = item.getString("Author"),
                                content = item.getString("Contnt"),
                                download = item.getString("DownloadDet"),
                                gid = item.getString("GID"),
                                path = item.getString("path");
                        int id = item.getInt("ID");

                        CallertunezItem result_item = new CallertunezItem();
                        result_item.ID = id;
                        result_item.Title = title;
                        result_item.Author = author;
                        result_item.Contnt = content;
                        result_item.DownloadDet = download;
                        result_item.GID = gid;
                        result_item.path = path;
                        result_item.thumbnail = path.concat("/jpg/".concat(gid).concat(".jpg"));
                        result_item.preview = path.concat("/mp3/".concat(gid).concat(".mp3"));
                        data.add(result_item);
                    }

                    progress.setVisibility(View.GONE);
                    CallertuneAdapter adapter = new CallertuneAdapter(sender, data);
                    adapter.setCustomParentAnimationViewId(R.id.toggle);
                    adapter.setParentClickableViewAnimationDefaultDuration();
                    adapter.setParentAndIconExpandOnClick(true);
                    list.setAdapter(adapter);
                    list.setLayoutManager(new LinearLayoutManager(sender));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.i("$cb", error.getMessage());
            }
        });
    }
}
