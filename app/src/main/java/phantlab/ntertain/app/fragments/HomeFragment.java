package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.BaseActivity;
import phantlab.ntertain.app.activities.TopTracksActivity;
import phantlab.ntertain.app.adapters.HomePagerAdapter;
import phantlab.ntertain.app.api.TopTracks;
import phantlab.ntertain.app.api.model.TrackCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.FeaturedHomeMusic;
import phantlab.ntertain.app.ui.FeaturedMoviesView;
import phantlab.ntertain.app.ui.FeaturedNewsView;
import phantlab.ntertain.app.ui.Pager;
import phantlab.ntertain.app.ui.PaginationView;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 12/3/2015.
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener {

    @Bind(R.id.featured_news)
    FeaturedNewsView news;

    @Bind(R.id.home_music)
    FeaturedHomeMusic music;

    @Bind({R.id.featured_music, R.id.featured_movies, R.id.latest_news})
    List<LinearLayout> texts;

    @Bind(R.id.music_pager)
    ViewPager pager;

    @Bind({R.id.movies_loading, R.id.news_progress, R.id.home_music_progress, R.id.home_progress})
    List<Progress> loading;

    @Bind(R.id.featured_movies_view)
    FeaturedMoviesView featured_movies;

    @Bind(R.id.pagination)
    PaginationView pagination;

    HomePagerAdapter pager_adapter;

    @Override
    int getLayout() {
        return R.layout.home;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        //lists.get(0).setLayoutManager(new GridLayoutManager(sender, 3));

        ((TextView) texts.get(0).findViewById(R.id.header_title)).setText(R.string.featured_music);
        ((TextView) texts.get(1).findViewById(R.id.header_title)).setText(R.string.featured_movies);
        ((TextView) texts.get(2).findViewById(R.id.header_title)).setText(R.string.latest_news);


        for (LinearLayout header : texts)
            header.setOnClickListener(this);

        pager_adapter = new HomePagerAdapter(getContext());
        pager.setAdapter(pager_adapter);
        setFeaturedMusic();

        featured_movies.setData(loading.get(0), sender);

        music.setProgress(loading.get(2), sender);

        Log.i("$isStreamingnull", String.valueOf((((BaseActivity) sender).getStreamingService() == null)) + "&isbound=" + ((BaseActivity) sender).isBound());

        news.setData(loading.get(1), sender);
    }

    private void setFeaturedMusic() {
        new TopTracks().getTracks(3, 1, new Callback<TrackCallback>() {
            @Override
            public void success(final TrackCallback cb, Response response) {
                final List<Tracks> data = cb.getData();
                pager_adapter.setData(data);
                pagination.setViewPager(pager);
                pager_adapter.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View click) {
                        ((BaseActivity) sender).getStreamingService().play(data.get((Integer) click.getTag()));
                    }
                });
                loading.get(3).setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.featured_music:
                Utils.StartActivity(sender, TopTracksActivity.class, Utils.getFeaturedMusicData(false));
                break;

            case R.id.featured_movies:
                ((BaseActivity) sender).onMovies();
                break;

            case R.id.latest_news:
                ((BaseActivity) sender).onNews();
                break;
        }
    }
}
