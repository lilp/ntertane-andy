package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.TimingsAdapter;
import phantlab.ntertain.app.callbacks.MoviesCallback;
import phantlab.ntertain.app.movies.MoviesEngine;
import phantlab.ntertain.app.movies.Shows;
import phantlab.ntertain.app.ui.Progress;

/**
 * Created by Ddev on 11/28/2015.
 */
public class ShowingAtFragment extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.progress)
    Progress progress;

    TimingsAdapter adapter;

    private String link;

    public ShowingAtFragment setLink(String link) {
        this.link = link;
        return this;
    }

    @Override
    int getLayout() {
        return R.layout.movies_showing_at;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new TimingsAdapter(getActivity());
        list.setLayoutManager(new LinearLayoutManager(sender));
        list.setAdapter(adapter);

        progress.setVisibility(View.VISIBLE);
        progress.start();

        new MoviesEngine().getTimings(link, new MoviesCallback.ShowsResult() {
            @Override
            public void onSuccess(List<Shows> shows) {
                adapter.setData(shows);
                progress.stop();
                progress.setVisibility(View.GONE);
            }
        });
    }
}
