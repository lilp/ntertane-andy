package phantlab.ntertain.app.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.MyMusicActivity;
import phantlab.ntertain.app.adapters.PlaylistAdapter;
import phantlab.ntertain.app.adapters.SongListAdapter;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.databases.MyMusicDatabase;
import phantlab.ntertain.app.models.Playlist;
import phantlab.ntertain.app.ui.MenuUtils;
import phantlab.ntertain.app.ui.Screen;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.ui.models.Menu;
import phantlab.ntertain.app.utils.Utils;

public class MyPlaylistFragment extends BaseFragment implements ClickCallbacks.ClickCallback {
    @Bind(R.id.list)
    RecyclerView playlist;

    @Bind(R.id.panel)
    LinearLayout panel;

    @Bind({R.id.blur_bg, R.id.thumb_sharp})
    List<SimpleDraweeView> thumbnails;

    @Bind(R.id.overlay)
    View overlay;

    @Bind(android.R.id.title)
    TextView title;

    PlaylistAdapter adapter;

    Animation slide, slide_down;
    int width = Utils.convertDpToPixel(60), pos;

    @Override
    int getLayout() {
        return R.layout.playlist_layout;
    }

    MyMusicDatabase database;
    List<Playlist> data;
    TopArtistSongsAdapter song_adapter;

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        database = new MyMusicDatabase(sender);
        adapter = new PlaylistAdapter(sender, database);
        adapter.setCallback(this);
        song_adapter = new TopArtistSongsAdapter(sender);
        playlist.setLayoutManager(new LinearLayoutManager(sender));
        playlist.setAdapter(adapter);

        refresh();

        slide = AnimationUtils.loadAnimation(sender, R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(sender, R.anim.slide_down);

        slide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                overlay.setVisibility(View.VISIBLE);
                panel.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {/****/}

            @Override
            public void onAnimationRepeat(Animation animation) {/****/}
        });

        slide_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {/***/}

            @Override
            public void onAnimationEnd(Animation animation) {
                overlay.setVisibility(View.GONE);
                panel.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {/***/}
        });

        List<Menu> menu = new ArrayList<>();
        menu.add(new Menu("Play Songs", R.drawable.ic_video));
        menu.add(new Menu("Delete", R.drawable.ic_unfollow));
        MenuUtils.setMenu(getContext(), panel, menu, new ClickCallbacks.ClickCallback() {
            @Override
            public void onClick(int position) {
                int id = data.get(pos).getId();

                switch (position) {
                    case 0:
                        List<Tracks> items = getPlaylistItems(id);
                        if (items.size() > 0)
                            Screen.mStreamingService.playFromList(items, 0);
                        else
                            empty_playlist();
                        break;
                    case 1:
                        delete(id, pos);
                }
            }
        });

        adapter.setOverflowCallback(new ClickCallbacks.OverflowCallback() {
            @Override
            public void onOverflowClick(int position) {
                pos = position;
                List<Tracks> items = getPlaylistItems(data.get(position).getId());
                if (!items.isEmpty()) {
                    ThumbnailController.with(thumbnails.get(0)).from(sender)
                            .uri(Uri.parse(items.get(0).getThumbnail()))
                            .blur(15);

                    ThumbnailController.with(thumbnails.get(1)).from(sender)
                            .uri(Uri.parse(items.get(0).getThumbnail()))
                            .dimens(width, width)
                            .load();
                    title.setText(data.get(position).getName());
                    panel.startAnimation(slide);
                } else empty_playlist();
            }
        });
    }

    private void delete(int id, int position) {
        database.delete_playlist(id);
        adapter.delete(position);
    }

    List<Tracks> getPlaylistItems(int id) {
        return database.getPlaylistItems(id);
    }

    void empty_playlist() {
        Toast.makeText(sender, "Empty Playlist", Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.close)
    public void onThumbnail() {
        panel.startAnimation(slide_down);
    }


    public void refresh() {
        if (playlist.getAdapter() instanceof TopArtistSongsAdapter)
            playlist.setAdapter(adapter);
        data = database.getPlaylist();
        adapter.setData(data);
    }


    @Override
    public void onClick(int position) {
        Log.i("$click", "position = " + position);
        int id = data.get(position).getId();
        List<Tracks> tracks = database.getPlaylistItems(id);
        if (tracks.size() > 0) {
            ((MyMusicActivity) getActivity()).setPlaylistId(id);
            playlist.setAdapter(song_adapter);
            song_adapter.setData(tracks, true);
        } else Toast.makeText(sender, "Playlist is empty!", Toast.LENGTH_SHORT).show();
    }
}
