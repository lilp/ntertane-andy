package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.NewsAdapter;
import phantlab.ntertain.app.adapters.NewsCarouselAdapter;
import phantlab.ntertain.app.news.News;
import phantlab.ntertain.app.news.NewsFeed;
import phantlab.ntertain.app.news.NewsItem;
import phantlab.ntertain.app.ui.Pager;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.SimpleDividerItemDecoration;
import phantlab.ntertain.app.viewholders.LoadingHolder;
import retrofit.RetrofitError;

/**
 * Created by Ddev on 11/29/2015.
 */
public class NewsActivity extends BaseFragment implements Paginate.Callbacks {
    private static int counter = 0;
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.featured_loader)
    Progress progress;

    NewsAdapter adapter = null;
    NewsCarouselAdapter mNewsCarouselAdapter;
    Paginate paginate;
    List<News> news;
    Object lock;
    private boolean mIsLoading;
    private LoadingListItemCreator creator = new LoadingListItemCreator() {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
            return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.loading, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (!mIsLoading)
                holder.itemView.setVisibility(View.GONE);
            else {
                holder.itemView.setVisibility(View.VISIBLE);
                ((Progress) holder.itemView.findViewById(R.id.progress)).start();
            }
        }
    };

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        setUpNews();
        setup();
    }

    private void setUpNews() {
        news = new ArrayList<>();
//        news.add(News.PUNCHNG);
        news.add(News.PULSE);
        news.add(News.NAIJALOADED);
        news.add(News.INFONG);
        news.add(News.NOBS);
        news.add(News.YNAIJA);
        news.add(News.BELLANAIJA);
//        news.add(News.NEWSAFRICA);
        news.add(News.NOTJUSTOK);
    }

    public void setup() {
        adapter = new NewsAdapter(sender);
        list.setLayoutManager(new LinearLayoutManager(sender));
        list.addItemDecoration(new SimpleDividerItemDecoration(sender));
        list.setAdapter(adapter);
        mNewsCarouselAdapter = new NewsCarouselAdapter(sender);
        pager.setAdapter(mNewsCarouselAdapter);

        paginate = Paginate.with(list, NewsActivity.this)
                .setLoadingListItemCreator(creator)
                .setLoadingTriggerThreshold(10)
                .build();

        setUpCarousel();
    }

    private void setUpCarousel() {
        showProgress();
        new NewsFeed().getFeaturedNews(new NewsFeed.FeaturedResult() {
            @Override
            public void onSuccess(List<NewsItem> result) {
                mNewsCarouselAdapter.setData(result);
                Pager.attach(pager, 8000);
                hideProgress();
            }
        });
    }

    private void hideProgress() {
        progress.stop();
        progress.setVisibility(View.GONE);
    }

    private void showProgress() {
        if (progress.getVisibility() == View.GONE)
            progress.setVisibility(View.VISIBLE);
        progress.start();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.search, menu);
//        return true;
//    }


    @Override
    public void onLoadMore() {
        mIsLoading = true;
        if (lock == null) {
            lock = new Object();
            Log.i("#counter", "counter = " + counter + ", news =  " + news.get(counter));
            new NewsFeed(sender, news.get(counter), new NewsFeed.NewsResult() {
                @Override
                public void onSuccess(List<NewsItem> result) {
                    for (NewsItem item : result)
                        adapter.addItem(item);
                    counter = counter + 1;
                    mIsLoading = false;
                    lock = null;
                }

                @Override
                public void onError(RetrofitError error) {
                    error.printStackTrace();
//                    Log.i(NewsActivity.class.getSimpleName(), error.getMessage());
                }
            }, false);
        }
    }

    @Override
    public boolean isLoading() {
        return mIsLoading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return counter == news.size();
    }

    @Override
    int getLayout() {
        return R.layout.news_main;
    }
}
