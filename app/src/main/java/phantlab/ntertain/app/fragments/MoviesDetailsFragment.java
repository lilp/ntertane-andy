package phantlab.ntertain.app.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;
import java.util.UUID;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.MoviesCallback;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.movies.MoviesEngine;
import phantlab.ntertain.app.movies.TrailerDialog;
import phantlab.ntertain.app.ui.CastView;
import phantlab.ntertain.app.ui.ThumbnailController;

/**
 * Created by Ddev on 11/28/2015.
 */
public class MoviesDetailsFragment extends BaseFragment {
    @Bind(R.id.thumbnail)
    SimpleDraweeView thumbnail;

    @Bind({android.R.id.title, android.R.id.summary, R.id.genre, R.id.plot, R.id.content_rating})
    List<TextView> texts;

    @Bind(R.id.cast)
    CastView cast;

    MovieMeta meta;

    public MoviesDetailsFragment() {/****/}

    public MoviesDetailsFragment setMeta(MovieMeta meta) {
        this.meta = meta;
        return this;
    }

    @OnClick(R.id.watch_trailer)
    public void watchTrailerClick() {
        new TrailerDialog().setVideo(meta.getVideo()).show(getFragmentManager(), UUID.randomUUID().toString());
    }

    @Override
    int getLayout() {
        return R.layout.movie_details;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (meta != null) {
            new ThumbnailController().with(thumbnail)
                    .uri(Uri.parse(meta.getThumb()))
                    .dimens(400, 200)
                    .load();

            String genre = meta.getGenre(), time = meta.getPlayback()
                    .replace("P",""), hour, minute;

            String[] genres = genre.split(",");
            if (genres.length > 1)
                genre = genres[0];

            hour = time.substring(0, time.indexOf('H'));
            minute = time.replace("M","").substring(time.indexOf('H') + 1);

            time = hour.concat("h").concat(" ").concat(minute).concat(" ").concat("m");

            texts.get(0).setText(meta.getTitle());
            texts.get(1).setText(time);
            texts.get(2).setText(genre);
            texts.get(3).setText(meta.getPlot());
            texts.get(4).setText(meta.getContent_rating());
            new MoviesEngine().getActors(meta.getLink(), new MoviesCallback.CastResult() {
                @Override
                public void onSuccess(List<String> result) {
                    if (result != null)
                        cast.setActors(result);
                }
            });
        }
    }
}
