package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.databases.MyMusicDatabase;
import phantlab.ntertain.app.ui.Screen;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public class MySongsFragment extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;

    @Bind({R.id.no_songs, R.id.shuffle})
    List<TextView> texts;


    TopArtistSongsAdapter adapter;
    MyMusicDatabase database;


    @Override
    int getLayout() {
        return R.layout.my_song_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        list.setLayoutManager(new LinearLayoutManager(sender));

        adapter = new TopArtistSongsAdapter(sender);
        list.setAdapter(adapter);

        database = new MyMusicDatabase(sender);
        List<Tracks> tracks = database.getSongs();
        if (tracks.isEmpty()) {
            texts.get(0).setVisibility(View.VISIBLE);
            texts.get(1).setVisibility(View.GONE);
        } else
            adapter.setData(tracks, true);
    }

    @OnClick(R.id.shuffle)
    public void onShuffle() {
        List<Tracks> songs = database.getSongs();
        Collections.shuffle(songs);
        Screen.mStreamingService.playFromList(songs, 0);
    }

}
