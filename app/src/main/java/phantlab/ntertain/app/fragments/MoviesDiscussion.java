package phantlab.ntertain.app.fragments;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.CommentsAdapter;
import phantlab.ntertain.app.api.Comments;
import phantlab.ntertain.app.api.model.CommentCb;
import phantlab.ntertain.app.api.model.CommentsCallback;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.SimpleDividerItemDecoration;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 11/29/2015.
 */
public class MoviesDiscussion extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.no_comments)
    TextView no_comments;
    @Bind(R.id.comment_edit)
    EditText comment;

    @Bind(R.id.progress)
    Progress progress;

    CommentsAdapter adapter;
    MovieMeta meta;

    Comments comments;

    @Override
    int getLayout() {
        return R.layout.movies_discussion;
    }

    public MoviesDiscussion() {/****/}

    public MoviesDiscussion setMeta(MovieMeta meta) {
        this.meta = meta;
        return this;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        adapter = new CommentsAdapter(sender);
        list.setLayoutManager(new LinearLayoutManager(sender));
        list.addItemDecoration(new SimpleDividerItemDecoration(sender));
        list.setAdapter(adapter);
        comments = new Comments();
        loadComments();
    }

    private void loadComments() {
        String md5 = Utils.md5(meta.getLink());

        comments.getComments(md5, new Callback<CommentsCallback>() {
            @Override
            public void success(CommentsCallback cb, Response response) {
                adapter.setComments(cb.getComments());
                if (cb.getComments().isEmpty())
                    no_comments.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @OnClick(R.id.send)
    public void onSend() {
        String txt = comment.getText().toString();
        if (txt.trim().length() > 0) {
            final ProgressDialog dialog = new ProgressDialog(sender);
            dialog.setMessage("Please Wait...");
            dialog.show();
            String md5 = Utils.md5(meta.getLink());
            Log.i("$md5", "posting = " + md5);
            comments.postComments(txt, md5, new Callback<CommentCb>() {
                @Override
                public void success(CommentCb cb, Response response) {
                    dialog.dismiss();
                    if (cb.getStatus()) {
                        adapter.add(cb.getComment());
                        no_comments.setVisibility(View.GONE);
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        } else
            Toast.makeText(sender, "Comment can't be empty!", Toast.LENGTH_SHORT).show();
    }
}