package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import butterknife.Bind;
import phantlab.ntertain.app.R;

/**
 * Created by kartikeyasingh on 11/12/15.
 */
public class MyCallerTunez extends BaseFragment {

    @Bind(R.id.tab_layout)
    TabLayout tabs;

    @Bind(R.id.pager)
    ViewPager pager;


    @Override
    int getLayout() {
        return R.layout.simple_tab_layout;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        pager.setAdapter(new CallerTunePagerAdapter(getChildFragmentManager()));
        tabs.setupWithViewPager(pager);

        /*TelephonyManager mTManager = (TelephonyManager) sender.getSystemService(Context.TELEPHONY_SERVICE);
        String operator = mTManager.getNetworkOperator();
        Log.i("$operator", operator.substring(0,3) + "mnc = " + operator.substring(3));*/
    }

    public class CallerTunePagerAdapter extends FragmentPagerAdapter {

        private String[] titles = new String[]{"NEW", "MY TUNEZ"};

        public CallerTunePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return new NewCallerTunezFragment();
            return new MyTunezFragment();
        }
    }
}
