package phantlab.ntertain.app.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.BaseActivity;
import phantlab.ntertain.app.activities.TopTracksActivity;
import phantlab.ntertain.app.api.Recent;
import phantlab.ntertain.app.api.model.RecentCallback;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.RecentReleasesView;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class MusicFragment extends BaseFragment {

    @Bind(R.id.header_title)
    TextView header;

    @Bind(R.id.recent_releases)
    RecentReleasesView recent;

    @OnClick(R.id.header)
    public void onHeaderClick(){
        Utils.StartActivity(sender, TopTracksActivity.class, Utils.getFeaturedMusicData(true));
    }

    @Bind(R.id.recent_progress)
    Progress recent_progress;

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        header.setText("RECENT RELEASE");

        recent.setProgress(recent_progress, ((BaseActivity)sender).getStreamingService());

        new Recent().getRecent(new Callback<RecentCallback>() {
            @Override
            public void success(RecentCallback cb, Response response) {
                if (cb.isSuccessFull())
                    recent.setData(Utils.getFilteredFeaturedMusic(cb.getRecent()));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    int getLayout() {
        return R.layout.music_home;
    }
}
