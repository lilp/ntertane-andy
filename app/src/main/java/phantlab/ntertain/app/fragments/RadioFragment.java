package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import butterknife.Bind;
import phantlab.ntertain.app.R;

/**
 * Created by Ddev on 11/27/2015.
 */
public class RadioFragment extends BaseFragment {
    @Bind(R.id.tab_layout)
    TabLayout tab;

    @Bind(R.id.pager)
    ViewPager pager;

    @Override
    protected int getLayout() {
        return R.layout.radio_layout;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        pager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return position == 0 ? new RadioFmFragment(): new RadioMusic();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return position == 0 ? "Radio FM" : "Radio Music";
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
        tab.setupWithViewPager(pager);
    }
}
