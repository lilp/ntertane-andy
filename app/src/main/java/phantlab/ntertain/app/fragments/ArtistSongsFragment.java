package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.ArtistActivity;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.Screen;

/**
 * Created by kartikeyasingh on 17/12/15.
 */
public class ArtistSongsFragment extends BaseFragment {

    @Bind(R.id.list)
    RecyclerView list;

    TopArtistSongsAdapter adapter;

    @Override
    int getLayout() {
        return R.layout.artists_top_songs;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        adapter = new TopArtistSongsAdapter(sender);
        list.setLayoutManager(new LinearLayoutManager(sender));
        list.setAdapter(adapter);
        notifyData();
    }

    public void notifyData() {
        ArtistActivity parent = (ArtistActivity) getActivity();
        if (parent != null)
            adapter.setData(parent.getTracks(), false);
    }

    @OnClick(R.id.shuffle)
    public void onShuffle() {
        ArtistActivity parent = (ArtistActivity) getActivity();
        List<Tracks> tracks = parent.getTracks();
        if (tracks != null && !tracks.isEmpty()) {
            Collections.shuffle(tracks);
            Screen.mStreamingService.playFromList(tracks, 0);
        } else Toast.makeText(sender, "No tracks to shuffle", Toast.LENGTH_SHORT).show();
    }
}
