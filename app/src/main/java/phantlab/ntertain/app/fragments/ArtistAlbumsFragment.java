package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.ArtistActivity;
import phantlab.ntertain.app.adapters.TopArtistAlbumsAdapter;

/**
 * Created by kartikeyasingh on 16/12/15.
 */
public class ArtistAlbumsFragment extends BaseFragment {
    private String artist, thumbnail;
    private int album_count, tracks_count,id;

    @Bind(R.id.list)
    RecyclerView list;

    TopArtistAlbumsAdapter adapter;

    @Override
    int getLayout() {
        return R.layout.artist_albums;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        setValues();
        adapter = new TopArtistAlbumsAdapter(sender, artist);
        list.setLayoutManager(new GridLayoutManager(sender, 2));
        list.setAdapter(adapter);
        notifyData();
    }

    private void setValues() {
        Bundle data = getArguments();
        if (data != null)
            artist = data.getString(ArtistActivity.KEY_ARTIST);

    }

    public void notifyData(){
        ArtistActivity parent = (ArtistActivity) getActivity();
        assert parent != null;
        adapter.setData(parent.getAlbums());
    }
}
