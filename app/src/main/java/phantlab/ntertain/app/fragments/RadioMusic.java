package phantlab.ntertain.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.exoplayer.extractor.mp4.Track;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.TopArtistSongsAdapter;
import phantlab.ntertain.app.api.TopTracks;
import phantlab.ntertain.app.api.model.TrackCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.SimpleDividerItemDecoration;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ddev on 11/28/2015.
 */
public class RadioMusic extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.progress)
    Progress progress;

    TopArtistSongsAdapter adapter;

    @Override
    int getLayout() {
        return R.layout.list_with_progress;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        list.setLayoutManager(new LinearLayoutManager(sender));
        adapter = new TopArtistSongsAdapter(sender);
        list.setAdapter(adapter);
        list.addItemDecoration(new SimpleDividerItemDecoration(sender));
        adapter.setDark();

        final ArrayList<String> artists = new ArrayList<>();
        new TopTracks().getTopTracks(new Callback<TrackCallback>() {
            @Override
            public void success(TrackCallback cb, Response response) {
                List<Tracks> tracks = new ArrayList<>();
                for (Tracks t : cb.getData()) {
                    String artist = t.getArtists()[0];
                    if (!artists.contains(artist))
                        tracks.add(t);
                    artists.add(artist);
                }
                adapter.setData(tracks, false);
                progress.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
