package phantlab.ntertain.app.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.DrawerAdapter;
import phantlab.ntertain.app.api.model.User;
import phantlab.ntertain.app.application.Ntertain;
import phantlab.ntertain.app.application.UserUtils;
import phantlab.ntertain.app.models.DrawerItems;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 12/3/2015.
 */
public class Drawer extends BaseFragment {
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.indicator)
    View indicator;
    @Bind(R.id.user_thumb)
    SimpleDraweeView thumb;
    @Bind(R.id.name)
    TextView name;

    DrawerAdapter adapter;
    DrawerCallbacks callback;
    ActionBarDrawerToggle toggle;

    public void setCallback(DrawerCallbacks callback) {
        this.callback = callback;

        adapter = new DrawerAdapter(sender, callback, indicator);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter);
        setUpData();
    }

    @Override
    int getLayout() {
        return R.layout.drawer;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        setup(UserUtils.getUser((Ntertain) sender.getApplication()));
    }

    private void setup(User user) {
        int width = Utils.convertDpToPixel(48);
        ThumbnailController.with(thumb)
                .dimens(width, width)
                .uri(Uri.parse("https://d13yacurqjgara.cloudfront.net/users/13307/avatars/small/Mike3.jpg?1382537343"))
                .load();
        name.setText(String.format("Hi %s", user.getName().split(" ")[0]));
    }

    private void setUpData() {
        List<DrawerItems> items = new ArrayList<>();
        items.add(new DrawerItems("Home", R.drawable.ic_home));
        items.add(new DrawerItems("Music", R.drawable.ic_music));
        items.add(null);
        items.add(new DrawerItems("Movies", R.drawable.ic_movies));
        items.add(new DrawerItems("Radio", R.drawable.ic_radio));
        items.add(new DrawerItems("News", R.drawable.ic_news));
        items.add(null);
        items.add(new DrawerItems("My Callertunez", R.drawable.ic_callertunez));
        items.add(new DrawerItems("My Music", R.drawable.ic_mymusic));
//        items.add(new DrawerItems("Send a Song", R.drawable.ic_send_songs));
        items.add(null);
        items.add(new DrawerItems("Settings", R.drawable.ic_settings));

        adapter.setData(items);
    }

    public void setUp(AppCompatActivity activity, DrawerLayout layout, Toolbar toolbar) {

        toggle = new ActionBarDrawerToggle(activity, layout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawer) {
                super.onDrawerOpened(drawer);
                toggle.syncState();
            }

            @Override
            public void onDrawerClosed(View drawer) {
                super.onDrawerClosed(drawer);
                toggle.syncState();

            }
        };

        layout.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();
    }

    public interface DrawerCallbacks {
        void onHome();

        void onMusic();

        void onNews();

        void onMovies();

        void onRadio();

        void onCallertunez();

        void onMyMusic();

        void onSettings();

        void onSendSong();
    }
}
