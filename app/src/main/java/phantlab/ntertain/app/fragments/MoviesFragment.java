package phantlab.ntertain.app.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import butterknife.Bind;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.BaseActivity;
import phantlab.ntertain.app.application.UserUtils;
import phantlab.ntertain.app.movies.NtertainLocation;

/**
 * Created by Ddev on 11/27/2015.
 */
public class MoviesFragment extends BaseFragment {
    @Bind(R.id.tab_layout)
    TabLayout tab;

    @Bind(R.id.pager)
    ViewPager pager;

    public final static String KEY_LATITUDE = "lat";
    public final static String KEY_LONGITUDE = "lng";
    NtertainLocation mLocation;

    @Override
    protected int getLayout() {
        return R.layout.movies_main;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        tab.addTab(tab.newTab().setText("NOW PLAYING"));
        tab.addTab(tab.newTab().setText("COMING SOON"));
        pager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return position == 0 ? new NowPlayingFragment() : new ComingSoonFragment();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return position == 0 ? "NOW PLAYING" : "COMING SOON";
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
        tab.setupWithViewPager(pager);
        setupLocation();
    }

    private void setupLocation() {
        SharedPreferences preferences = ((BaseActivity) getActivity()).getPreferences();
        mLocation = new NtertainLocation(getActivity(), preferences);
        if (mLocation.getDouble(preferences, KEY_LATITUDE, 0.0) == 0.0)
            mLocation.enableLocation();
    }
}
