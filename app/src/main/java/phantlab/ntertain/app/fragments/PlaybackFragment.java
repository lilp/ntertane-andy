package phantlab.ntertain.app.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.PlayMusicActivity;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.bus.BusProvider;
import phantlab.ntertain.app.bus.PlayBackStateChangedEvent;
import phantlab.ntertain.app.music.MusicService;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 28/12/15.
 */
public class PlaybackFragment extends BaseFragment {

    @Bind(R.id.blur)
    SimpleDraweeView blur;

    MusicService mService;

    @Bind({android.R.id.title, R.id.artist})
    List<TextView> titles;

    @Bind(R.id.play)
    ImageButton play;
    View fragment;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = ((MusicService.MusicServiceBinder) iBinder).getService();
            onPlayBackStateChanged(new PlayBackStateChangedEvent(PlayBackStateChangedEvent.State.NewSongPlaying, mService.getTrack()));
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    @OnClick(R.id.play)
    public void onPlay() {
        Intent serviceIntent = new Intent(MusicService.ACTION_PLAYPAUSE, null, sender,
                MusicService.class);
        sender.startService(serviceIntent);
    }

    @OnClick(R.id.toggle)
    public void toggle() {
        Intent intent = new Intent(getActivity(), PlayMusicActivity.class);
        intent.setAction(PlayMusicActivity.ACTION_VIEW_SONG);
        startActivity(intent);
    }

    @Override
    int getLayout() {
        return R.layout.playback;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onPlayBackStateChanged(PlayBackStateChangedEvent event) {
        if (event.getState().equals(PlayBackStateChangedEvent.State.Stop)) {
            fragment.setVisibility(View.GONE);
            Intent serviceIntent = new Intent(MusicService.ACTION_EXIT, null, sender,
                    MusicService.class);
            sender.startService(serviceIntent);
        } else {
            Tracks track = event.getTrack();
            int res;
            if (track != null) {
                switch (event.getState()) {
                    case NewSongPlaying:
                        titles.get(0).setText(track.getTrack());
                        titles.get(1).setText(track.getArtists()[0]);
                        if (mService.isPlaying())
                            res = R.drawable.ic_pause_white_24dp;
                        else res = R.drawable.ic_play_arrow_white_24dp;
                        play.setImageResource(res);

                        if (track.getThumbnail() != null)
                            ThumbnailController.with(blur)
                                    .from(sender).uri(Uri.parse(track.getThumbnail()))
                                    .dimens(Utils.convertDpToPixel(400), Utils.convertDpToPixel(56))
                                    .blur(25);
                        break;
                    case Playing:
                        play.setImageResource(R.drawable.ic_pause_white_24dp);
                        break;
                    case Paused:
                        play.setImageResource(R.drawable.ic_play_arrow_white_24dp);
                        break;
                }
                if (fragment.getVisibility() == View.GONE)
                    fragment.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        fragment = view;
        getActivity().bindService(new Intent(getActivity(), MusicService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
        view.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unbindService(mServiceConnection);
    }
}
