package phantlab.ntertain.app.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.NowPlayingAdapter;
import phantlab.ntertain.app.callbacks.MoviesCallback;
import phantlab.ntertain.app.movies.Movie;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.movies.MoviesEngine;
import phantlab.ntertain.app.ui.Pager;
import phantlab.ntertain.app.ui.Progress;
import phantlab.ntertain.app.ui.ThumbnailController;

/**
 * Created by Ddev on 11/27/2015.
 */
public class ComingSoonFragment extends BaseFragment {
    @Bind(R.id.pager)
    ViewPager pager;

    @Bind(R.id.list)
    RecyclerView list;

    @Bind({R.id.crousel_loader, R.id.now_showing_items_loader})
    List<Progress> progress;
    CarouselAdapter adapter;
    NowPlayingAdapter mListAdapter;

    @Override
    protected int getLayout() {
        return R.layout.now_playing;
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        list.setLayoutManager(new GridLayoutManager(sender, 3));
        adapter = new CarouselAdapter();
        pager.setAdapter(adapter);
        setUpCarousel();
        mListAdapter = new NowPlayingAdapter(sender, false);
        mListAdapter.setType(NowPlayingAdapter.AdapterType.ComingSoon);
        list.setAdapter(mListAdapter);
    }

    private void setUpCarousel() {
        progress.get(0).start();


        progress.get(1).start();
        new MoviesEngine().getComingSoon(new MoviesCallback.StringResult() {
            @Override
            public void onSuccess(List<MovieMeta> result) {
                if (result != null) {
                    progress.get(1).stop();
                    progress.get(1).setVisibility(View.GONE);
                    mListAdapter.setData(result);

                    progress.get(0).stop();
                    progress.get(0).setVisibility(View.GONE);

                    List<Movie> movies = new ArrayList<>();
                    for (int i = 0; i < 3; i++) {
                        MovieMeta meta = result.get(i);
                        Movie movie = new Movie();
                        movie.setLink(meta.getTitle());
                        movie.setThumbnail(meta.getThumb());
                        movie.setTitle(meta.getTitle());
                        movies.add(movie);
                    }
                    adapter.setMovie(movies);
                    Pager.attach(pager, 2500);
                }
            }
        });
    }

    public class CarouselAdapter extends PagerAdapter {
        @Bind(R.id.thumbnail)
        SimpleDraweeView thumbnail;
        private List<Movie> movie;

        public void setMovie(List<Movie> movie) {
            this.movie = movie;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return movie != null ? movie.size() : 0;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View layout = sender.getLayoutInflater().inflate(R.layout.now_playing_item, container, false);
            ButterKnife.bind(this, layout);
            thumbnail.getHierarchy().setProgressBarImage(new ProgressBarDrawable());
            ThumbnailController.with(thumbnail).uri(Uri.parse(movie.get(position).getThumbnail()))
                    .dimens(400, 200).load();
            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

    }
}
