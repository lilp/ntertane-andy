package phantlab.ntertain.app.video;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageButton;

import butterknife.Bind;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.ui.Screen;

/**
 * Created by kartikeyasingh on 29/12/15.
 */
public class VideoPlayer extends Screen {
    @Bind(R.id.surface)
    ExoPlayerView player;

    @Bind(R.id.play)
    ImageButton play;


    @OnClick(R.id.play)
    public void onPlay(){
        if (player.isPlaying()){
            player.pause();
            play.setImageResource(R.drawable.ic_play_arrow_white_24dp);
        } else {
            player.resume();
            play.setImageResource(R.drawable.ic_pause_white_24dp);
        }
    }

    @Override
    protected void onCreate(Bundle bundle) {
        setHorizontalOrientation();
        super.onCreate(bundle);
        setContentView(R.layout.video_player);
    }

    @Override
    public void setup() {
        Uri track = Uri.parse("http://r6---sn-bg07ynes.googlevideo.com/videoplayback?fexp=9408208%2C9412859%2C9413137%2C9416126%2C9416984%2C9418203%2C9420452%2C9422596%2C9423581%2C9423662%2C9424752%2C9425864&ms=au&mt=1451743834&mv=m&dur=378.183&upn=BRUbo90qUAQ&source=youtube&mm=31&mn=sn-bg07ynes&lmt=1389692113901689&signature=CF74E0DD10E19AA92CF6C3D047A27BFC1B30EF07.1D50C12D765B5FC455F9B03B6B07BDDD0CAA1925&ipbits=0&mime=video%2Fmp4&itag=18&sver=3&pl=21&nh=IgpwcjAxLmdydTA2Kg0xNzcuODQuMTYwLjgx&expire=1451765593&initcwndbps=328750&id=o-ALWmEANgjzqUWnqr71jFWDbehjhSMvTvzONAYcFYlxKU&sparams=dur%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnh%2Cpl%2Cratebypass%2Csource%2Cupn%2Cexpire&key=yt6&ip=177.8.170.4&ratebypass=yes&title=Tech+N9ne+-+This+Ring+%28live%29");
        player.play(track);
    }
}
