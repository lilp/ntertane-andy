package phantlab.ntertain.app.video;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import phantlab.ntertain.app.movies.MoviesEngine;

/**
 * Created by Ddev on 1/3/2016.
 */
public class Youtube {
    public void get(){
        /*new AsyncTask<Void, Void, String[]>() {
            @Override
            protected String[] doInBackground(Void... params) {
                try {
                    String htm = Jsoup.connect("https://www.youtube.com/watch?v=7oYGLwCscB0").userAgent(MoviesEngine.AGENT).get().outerHtml();
                    Log.i("$htm", htm);
                    return extractLinks("<script>var ytplayer = ytplayer || {};ytplayer.config = {\"html5\":true,\"assets\":{\"css\":\"\\/\\/s.ytimg.com\\/yts\\/cssbin\\/www-player-webp-vfld-mKGE.css\",\"js\":\"\\/\\/s.ytimg.com\\/yts\\/jsbin\\/player-en_US-vflnrstgx\\/base.js\"},\"sts\":16777,\"args\":{\"watch_ajax_token\":\"QUFFLUhqbGRJS3p5Y0ZYeHdycXNMTkd1UC15SnRkSktSZ3xBQ3Jtc0tuVk95dEpnTG5iT2l4eEdPaVE2VzZrbGc0cHpxdmVlaFpqd1VrMFJGTVNnUWc2ZGhaVDVDRGVKNXI1aU9NOWZKVW44eGhZR2RnT0hUY1YtSE1ZWmxoQ2RPWDhkcklRR2hmcUc0N2pHZmJqV3JzczhWUQ==\",\"avg_rating\":\"4.9375\",\"caption_audio_tracks\":\"v=0\\u0026i=0\",\"fmt_list\":\"22\\/1280x720\\/9\\/0\\/115,43\\/640x360\\/99\\/0\\/0,18\\/640x360\\/9\\/0\\/115,5\\/426x240\\/7\\/0\\/0,36\\/320x180\\/99\\/1\\/0,17\\/176x144\\/99\\/1\\/0\",\"user_age\":\"25\",\"eventid\":\"GrWIVsnfL4ypoAP5_KygCw\",\"dashmpd\":\"https:\\/\\/manifest.googlevideo.com\\/api\\/manifest\\/dash\\/sver\\/3\\/signature\\/0127BD688CB38F5CCD7505ABB9174CB733884016.D6B0CB76AB839AC3E071B7EF7292E9D502752F84\\/ipbits\\/0\\/hfr\\/1\\/source\\/youtube\\/pl\\/23\\/itag\\/0\\/expire\\/1451821434\\/as\\/fmp4_audio_clear%2Cwebm_audio_clear%2Cfmp4_sd_hd_clear%2Cwebm_sd_hd_clear%2Cwebm2_sd_hd_clear\\/playback_host\\/r2---sn-h557snes.googlevideo.com\\/requiressl\\/yes\\/ms\\/au\\/fexp\\/3300132%2C3300164%2C9407187%2C9408534%2C9415515%2C9416126%2C9418203%2C9420452%2C9421341%2C9421977%2C9422342%2C9422596%2C9423662%2C9425078%2C9425382%2C9426523\\/mt\\/1451799775\\/mv\\/m\\/mm\\/31\\/key\\/yt6\\/ip\\/1.39.51.185\\/upn\\/3CP8OqXELUg\\/id\\/o-AIQXAH1tTFenn-qlJsHCadGKjRT6AY46kG46ZW4oDxY-\\/sparams\\/as%2Chfr%2Cid%2Cip%2Cipbits%2Citag%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cplayback_host%2Crequiressl%2Csource%2Cexpire\\/mn\\/sn-h557snes\",\"referrer\":\"https:\\/\\/www.youtube.com\\/channel\\/UC24HmeK-G-ElWkdGziH-VeA\",\"adaptive_fmts\":\"type=video%2Fmp4%3B+codecs%3D%22avc1.4d401f%22\\u0026projection_type=1\\u0026lmt=1439800144855301\\u0026init=0-708\\u0026quality_label=720p\\u0026bitrate=1103237\\u0026size=1280x720\\u0026clen=79769551\\u0026index=709-2336\\u0026itag=136\\u0026fps=24\\u0026url=https%3A%2F%2Fr2---sn-h557snes.googlevideo.com%2Fvideoplayback%3Fsver%3D3%26signature%3D14D903A537C3F8972532BE0D0355BF2859B2BB37.C6EDB038B550192373545F31A1B28B9D7C6A465B%26initcwndbps%3D795000%26ipbits%3D0%26clen%3D79769551%26keepalive%3Dyes%26source%3Dyoutube%26dur%3D710.167%26pl%3D23%26itag%3D136%26mime%3Dvideo%252Fmp4%26expire%3D1451821434%26lmt%3D1439800144855301%26requiressl%3Dyes%26ms%3Dau%26fexp%3D3300132%252C3300164%252C9407187%252C9408534%252C9415515%252C9416126%252C9418203%252C9420452%252C9421341%252C9421977%252C9422342%252C9422596%252C9423662%252C9425078%252C9425382%252C9426523%26mt%3D1451799775%26mv%3Dm%26sparams%3Dclen%252Cdur%252Cgir%252Cid%252Cinitcwndbps%252Cip%252Cipbits%252Citag%252Ckeepalive%252Clmt%252Cmime%252Cmm%252Cmn%252Cms%252Cmv%252Cpl%252Crequiressl%252Csource%252Cupn%252Cexpire%26gir%3Dyes%26key%3Dyt6%26ip%3D1.39.51.185%26upn%3D3CP8OqXELUg%26id%3Do-AIQXAH1tTFenn-qlJsHCadGKjRT6AY46kG46ZW4oDxY-%26mm%3D31%26mn%3Dsn-h557snes,type=video%2Fwebm%3B+codecs%3D%22vp9%22\\u0026projection_type=1\\u0026lmt=1438885538145219\\u0026init=0-242\\u0026quality_label=720p\\u0026bitrate=692265\\u0026size=1280x720\\u0026clen=38742985\\u0026index=243-2568\\u0026itag=247\\u0026fps=24\\u0026url=https%3A%2F%2Fr2---sn-h557snes.googlevideo.com%2Fvideoplayback%3Fsver%3D3%26signature%3D405B9C672B10C789F8C9BCD5D708CD403A9A347F.B0C0DDB98BF7641A42A6B79CEAB7A4D50630E36B%26initcwndbps%3D795000%26ipbits%3D0%26clen%3D38742985%26keepalive%3Dyes%26source%3Dyoutube%26dur%3D710.126%26pl%3D23%26itag%3D247%26mime%3Dvideo%252Fwebm%26expire%3D1451821434%26lmt%3D1438885538145219%26requiressl%3Dyes%26ms%3Dau%26fexp%3D3300132%252C3300164%252C9407187%252C9408534%252C9415515%252C9416126%252C9418203%252C9420452%252C9421341%252C9421977%252C9422342%252C9422596%252C9423662%252C9425078%252C9425382%252C9426523%26mt%3D1451799775%26mv%3Dm%26sparams%3Dclen%252Cdur%252Cgir%252Cid%252Cinitcwndbps%252Cip%252Cipbits%252Citag%252Ckeepalive%252Clmt%252Cmime%252Cmm%252Cmn%252Cms%252Cmv%252Cpl%252Crequiressl%252Csource%252Cupn%252Cexpire%26gir%3Dyes%26key%3Dyt6%26ip%3D1.39.51.185%26upn%3D3CP8OqXELUg%26id%3Do-AIQXAH1tTFenn-qlJsHCadGKjRT6AY46kG46ZW4oDxY-%26mm%3D31%26mn%3Dsn-h557snes,type=video%2Fmp4%3B+codecs%3D%22avc1.4d401e%22\\u0026projection_type=1\\u0026lmt=1439800140365820\\u0026init=0-708\\u0026quality_label=480p\\u0026bitrate=548953\\u0026size=854x480\\u0026clen=38520299\\u0026index=709-2336\\u0026itag=135\\u0026fps=24\\u0026url=https%3A%2F%2Fr2---sn-h557snes.googlevideo.com%2Fvideoplayback%3Fsver%3D3%26signature%3D7526A79FDA1EFAAEFD122AACE64BD8FDA08129B8.41501A92E045B599C2405DF13693C133E0F4971E%26initcwndbps%3D795000%26ipbits%3D0%26clen%3D38520299%26keepalive%3Dyes%26source%3Dyoutube%26dur%3D710.167%26pl%3D23%26itag%3D135%26mime%3Dvideo%252Fmp4%26expire%3D1451821434%26lmt%3D1439800140365820%26requiressl%3Dyes%26ms%3Dau%26fexp%3D3300132%252C3300164%252C9407187%252C9408534%252C9415515%252C9416126%252C9418203%252C9420452%252C9421341%252C9421977%252C9422342%252C9422596%252C9423662%252C9425078%252C9425382%252C9426523%26mt%3D1451799775%26mv%3Dm%26sparams%3Dclen%252Cdur%252Cgir%252Cid%252Cinitcwndbps%252Cip%252Cipbits%252Citag%252Ckeepalive%252Clmt%252Cmime%252Cmm%252Cmn%252Cms%252Cmv%252Cpl%252Crequiressl%252Csource%252Cupn%252Cexpire%26gir%3Dyes%26key%3Dyt6%26ip%3D1.39.51.185%26upn%3D3CP8OqXELUg%26id%3Do-AIQXAH1tTFenn-qlJsHCadGKjRT6AY46kG46ZW4oDxY-%26mm%3D31%26mn%3Dsn-h557snes,type=video%2Fwebm%3B+codecs%3D%22vp9%22\\u0026projection_type=1\\u0026lmt=1438885484882485\\u0026init=0-242\\u0026quality_label=480p\\u0026bitrate=295676\\u0026size=854x480\\u0026clen=18093070\\u0026index=243-2504\\u0026itag=244\\u0026fps=24\\u0026url=https%3A%2F%2Fr2---sn-h557snes.googlevideo.com%2Fvideoplayback%3Fsver%3D3%26signature%3D5E2229F6605B18F66C25F6BB22E4BAB1ECC1789F.A023101C0BE507A602A27966002A7B706D392C99%26initcwndbps%3D795000%26ipbits%3D0%26clen%3D18093070%26keepalive%3Dyes%26source%3Dyoutube%26dur%3D710.126%26pl%3D23%26itag%3D244%26mime%3Dvideo%252Fwebm%26expire%3D1451821434%26lmt%3D1438885484882485%26requiressl%3Dyes%26ms%3Dau%26fexp%3D3300132%252C3300164%252C9407187%252C9408534%252C9415515%252C9416126%252C9418203%252C9420452%252C9421341%252C9421977%252C9422342%252C9422596%252C9423662%252C9425078%252C9425382%252C9426523%26mt%3D1451799775%26mv%3Dm%26sparams%3Dclen%252Cdur%252Cgir%252Cid%252Cinitcwndbps%252Cip%252Cipbits%252Citag%252Ckeepalive%252Clmt%252Cmime%252Cmm%252Cmn%252Cms%252Cmv%252Cpl%252Crequiressl%252Csource%252Cupn%252Cexpire%26gir%3Dyes%26key%3Dyt6%26ip%3D1.39.51.185%26upn%3D3CP8OqXELUg%26id%3Do-AIQXAH1tTFenn-qlJsHCadGKjRT6AY46kG46ZW4oDxY-%26mm%3D31%26mn%3Dsn-h557snes,type=video%2Fmp4%3B+codecs%3D%22avc1.4d401e%22\\u0026projection_type=1\\u0026lmt=1439800138403404\\u0026init=0-708\\u0026quality_label=360p\\u0026bitrate=261496\\u0026size=640x360\\u0026clen=19344683\\u0026index=709-2336\\u0026itag=134\\u0026fps=24\\u0026url=https%3A%2F%2Fr2---sn-h557snes.googlevideo.com%2Fvideoplayback%3Fsver%3D3%26signature%3D6999AB1706E4098D16A8001E05A37BC282F60098.1563839AF9CA3944C74C301BA07D7CE12578534C%26initcwndbps%3D795000%26ipbits%3D0%26clen%3D19344683%26keepalive%3Dyes%26source%3Dyoutube%26dur%3D710.167%26pl%3D23%26itag%3D134%26mime%3Dvideo%252Fmp4%26expire%3D1451821434%26lmt%3D1439800138403404%26requiressl%3Dyes%26ms%3Dau%26fexp%3D3300132%252C3300164%252C9407187%252C9408534%252C9415515%252C9416126%252C9418203%252C9420452%252C9421341%252C9421977%252C9422342%252C9422596%252C9423662%252C9425078%252C9425382%252C9426523%26mt%3D1451799775%26mv%3Dm%26sparams%3Dclen%252Cdur%252Cgir%252Cid%252Cinitcwndbps%252Cip%252Cipbits%252Citag%252Ckeepalive%252Clmt%252Cmime%252Cmm%252Cmn%252Cms%252Cmv%252Cpl%252Crequiressl%252Csource%252Cupn%252Cexpire%26gir%3Dyes%26key%3Dyt6%26ip%3D1.39.51.185%26upn%3D3CP8OqXELUg%26id%3Do-AIQXAH1tTFenn-qlJsHCadGKjRT6AY46kG46ZW4oDxY-%26mm%3D31%26mn%3Dsn-h557snes,type=video%2Fwebm%3B+codecs%3D%22vp9%22\\u0026projection_type=1\\u0026lmt=1438885368311125\\u0026init=0-242\\u0026quality_label=360p\\u0026bitrate=167052\\u0026size=640x360\\u0026clen=11068820\\u0026index=243-2494\\u0026itag=243\\u0026fps=24\\u0026url=https%3A%2F%2Fr2---sn-h557snes.googlevideo.com%2Fvideoplayback%3Fsver%3D3%26signature%3DAE347F1D04B23BEE8E117503942A8E5A6FDD9B5D.2A2100DA7BF4F89F4077497CAD6A8541086CE496%26initcwndbps%3D795000%26ipbits%3D0%26clen%3D11068820%26keepalive%3Dyes%26source%3Dyoutube%26dur%3D710.126%26pl%3D23%26itag%3D243%26mime%3Dvideo%252Fwebm%26expire%3D1451821434%26lmt%3D1438885368311125%26requiressl%3Dyes%26ms%3Dau%26fexp%3D3300132%252C3300164%252C9407187%252C9408534%252C9415515%252C9416126%252C9418203%252C9420452%252C9421341%252C9421977%252C9422342%252C9422596%252C9423662%252C9425078%252C9425382%252C9426523%26mt%3D1451799775%26mv%3Dm%26sparams%3Dclen%252Cdur%252Cgir%252Cid%252Cinitcwndbps%252Cip%252Cipbits%252Citag%252Ckeepalive%252Clmt%252Cmime%252Cmm%252Cmn%252Cms%252Cmv%252Cpl%252Crequiressl%252Csource%252Cupn%252Cexpire%26gir%3Dyes%26key%3Dyt6%26ip%3D1.39.51.185%26upn%3D3CP8OqXELUg%26id%3Do-AIQXAH1tTFenn-qlJsHCadGKjRT6AY46kG46ZW4oDxY-%26mm%3D31%26mn%3Dsn-h557snes,type=video%2Fmp4%3B+codecs%3D%22avc1.4d4015%22\\u0026projection_type=1\\u0026lmt=1439800138797861\\u0026init=0-672\\u0026quality_label=240p\\u0026bitrate=248274\\u0026size=426x240\\u0026clen=21751666\\u0026index=673-2300\\u0026itag=133\\u0026fps=24\\u0026url=https%3A%2F%2Fr2---sn-h557snes.googlevideo.com%2Fvideoplayback%3Fsver%3D3%26signature%3D7A8A960C0C970140039F6F1E1B0E34591DF7D04F.12CDCB8678E58909FE3FF3CD678938E46B09C7BF%26initcwndbps%3D795000%26ipbits%3D0%26clen%3D21751666%26keepalive%3Dyes%26source%3Dyoutube%26dur%3D710.167%26pl%3D23%26itag%3D133%26mime%3Dvideo%252Fmp4%26expire%3D1451821434%26lmt%3D1439800138797861%26requiressl%3Dyes%26ms%3Dau%26fexp%3D3300132%252C3300164%252C9407187%252C9408534%252C9415515%252C9416126%252C9418203%252C9420452%252C9421341%252C9421977%252C9422342%252C9422596%252C9423662%252C9425078%252C9425382%252C9426523%26mt%3D1451799775%26mv%3Dm%26sparams%3Dclen%252Cdur%252Cgir%252Cid%252Cinitcwndbps%252Cip%252Cipbits%252Citag%252Ckeepalive%252Clmt%252Cmime%252Cmm%252Cmn%252Cms%252Cmv%252Cpl%252Crequiressl%252Csource%252Cupn%252Cexpire%26gir%3Dyes%26key%3Dyt6%26ip%3D1.…</script>");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return new String[0];
            }

            @Override
            protected void onPostExecute(String[] strings) {
                for (String str : strings)
                    Log.i("$link", str);
            }
        }.execute();*/
    }

    protected String[] extractLinks(String result) {
        //The title of the downloaded file
        String title = "bla bla bla";
        // An array of strings that will hold the links (in case of error, it will hold 1 string)
        String[] temper = new String[1];

        try{
            // Extract the "url_encoded_fmt_stream_map" attr from the flash object
            Pattern p = Pattern.compile("url_encoded_fmt_stream_map(.*?);");
            Matcher m = p.matcher(result);
            List<String> matches = new ArrayList<String>();
            while(m.find()){
                matches.add(m.group().replace("url_encoded_fmt_stream_map=", "").replace(";", ""));
            }
            String[] streamMap = null;
            List<String> links = new ArrayList<String>();

            if(matches.get(0) != null &&  matches.get(0) != ""){
                // Split the "url_encoded_fmt_stream_map" into an array of strings that hold the link values
                streamMap = matches.get(0).split("%2C");
                for (int i = 0; i < streamMap.length; i++){
                    String url = "";
                    String sig = "";

                    //Using regex, we will get the video url.
                    Pattern p2 = Pattern.compile("url%3D(.*?)%26");
                    Matcher m2 = p2.matcher(streamMap[i]);
                    List<String> matches2 = new ArrayList<String>();
                    while(m2.find()){
                        // We don't need the "url=...&" part.
                        matches2.add(m2.group().substring(6, m2.group().length() - 3));
                    }
                    url = matches2.get(0);

                    //Using regex again, we will get the video signature.
                    p2 = Pattern.compile("sig%3D(.*?)%26");
                    m2 = p2.matcher(streamMap[i]);
                    matches2 = new ArrayList<String>();
                    while(m2.find()){
                        // We don't need the "sig=...&" part.
                        matches2.add(m2.group().substring(6, m2.group().length() - 3));
                    }
                    sig = matches2.get(0);

                    //Now lets add a link to our list. Link = url + sig + title.
                    links.add(URLDecoder.decode(URLDecoder.decode(url + "&signature=", "UTF-8") + sig + "%26title%3D" + URLDecoder.decode(title, "UTF-8"), "UTF-8"));
                }
            }
            else{
                links.add("No download Links");
            }
            //Now lets make temper point to a array that holds the list items (aka links).
            temper = links.toArray(new String[links.size()]);
        }
        catch(Exception ex){
            temper[0] = ex.getMessage();
        }
        // That's it! temper has direct download links from youtube!
        return temper;
    }
}
