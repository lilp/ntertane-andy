package phantlab.ntertain.app.video;

import android.content.Context;
import android.media.MediaCodec;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.MediaController;

import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.PlayerControl;
import com.google.android.exoplayer.util.Util;

import phantlab.ntertain.app.music.NtertainPlayer;

/**
 * Created by kartikeyasingh on 01/01/16.
 */
public class ExoPlayerView extends SurfaceView implements ExoPlayer.Listener {
    private static final int BUFFER_SEGMENT_SIZE = 256 * 1024;
    private static final int BUFFER_SEGMENTS = 64;
    Uri uri;
    ExoPlayer mExoPlayer;
    private MediaController.MediaPlayerControl control;

    public ExoPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void play(Uri uri) {
        this.uri = uri;
        play();
    }

    public void pause() {
        control.pause();
    }

    public void resume() {
        control.start();
    }

    public void seekTo(int ms) {
        control.seekTo(ms);
    }

    public boolean isPlaying() {
        return control.isPlaying();
    }

    public void release() {
        mExoPlayer.release();
    }

    private void play() {
        mExoPlayer = ExoPlayer.Factory.newInstance(2);
        DataSource source = new DefaultUriDataSource(getContext(), Util.getUserAgent(getContext(), "Ntertane"));
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(uri, source, new DefaultAllocator(BUFFER_SEGMENTS), BUFFER_SEGMENT_SIZE);
        TrackRenderer videoRenderer = new MediaCodecVideoTrackRenderer(getContext(), sampleSource, MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        TrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource);
        mExoPlayer.prepare(videoRenderer, audioRenderer);
        mExoPlayer.addListener(this);
        mExoPlayer.sendMessage(videoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, getHolder().getSurface());
        mExoPlayer.setPlayWhenReady(true);
        control = new PlayerControl(mExoPlayer);
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        Log.i("$state", String.valueOf(playbackState));
    }

    @Override
    public void onPlayWhenReadyCommitted() {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.i(ExoPlayerView.class.getSimpleName(), error.getMessage());
    }
}
