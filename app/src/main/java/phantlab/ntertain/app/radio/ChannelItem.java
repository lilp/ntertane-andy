package phantlab.ntertain.app.radio;

import android.net.Uri;

/**
 * Created by kartikeyasingh on 24/12/15.
 */
public class ChannelItem {
    String channel, thumbnail;
    Uri uri;
    Channels radio;

    public ChannelItem(String channel, String thumbnail, Uri uri, Channels radio) {
        this.channel = channel;
        this.thumbnail = thumbnail;
        this.uri = uri;
        this.radio = radio;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public Channels getRadio() {
        return radio;
    }

    public Uri getUri() {
        return uri;
    }

    public String getChannel() {
        return channel;
    }
}
