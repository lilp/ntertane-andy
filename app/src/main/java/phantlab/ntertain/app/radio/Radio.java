package phantlab.ntertain.app.radio;

import android.net.Uri;

/**
 * Created by kartikeyasingh on 24/12/15.
 */
public class Radio {

    public static Uri getLiveStreamUri(Channels channel) {
        String uri = null;
        switch (channel) {
            case NAIJAFM:
                uri = "http://icy2.abacast.com/megalectrics-naijafmmp3-32";
                break;
            case RADIOTWOTHIRTYFOUR:
                uri = "http://174.37.16.73:8080/Live";
                break;
            case CLASSICFM:
                uri = "http://icy2.abacast.com/megalectrics-classicfm-32";
                break;
            case COOLFM:
                uri = "http://icestream.coolwazobiainfo.com:8000/coolfm-lagos";
                break;
            case INSPIRATION:
                uri = "http://67.228.150.175:8550/;";
                break;
            case RADIOCONTINENTAL:
                uri = "http://67.228.150.175:8890/;";
                break;
            case RAINBOW:
                uri = "http://icy3.abacast.com/rainbow-rainbowaac-48";
                break;
            case SMOOTH:
                uri = "http://ss1.metrong.net:8000/smoothlivefm128";
                break;
            case TOPRADIO:
                uri = "http://67.228.150.175:8460/;";
                break;
            case THEBEAT:
                uri = "http://icy2.abacast.com/megalectrics-thebeat-32";
                break;
            case WAZOBIALAGOS:
                uri = "http://icestream.coolwazobiainfo.com:8000/wazobia-lagos";
                break;
            case HOTFM:
                uri = "http://97.74.73.104:9974/;";
                break;
        }
        return Uri.parse(uri);
    }
}
