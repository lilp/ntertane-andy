package phantlab.ntertain.app.radio;

/**
 * Created by kartikeyasingh on 24/12/15.
 */
public enum Channels {
    NAIJAFM,
    RADIOTWOTHIRTYFOUR,
    CLASSICFM,
    COOLFM,
    INSPIRATION,
    RADIOCONTINENTAL,
    RAINBOW,
    SMOOTH,
    TOPRADIO,
    THEBEAT,
    WAZOBIALAGOS,
    HOTFM
}


// http://174.37.16.73:8080/Live - 234Radio
// http://icy2.abacast.com/megalectrics-naijafmmp3-32
// http://icy2.abacast.com/megalectrics-classicfm-32 => CLASSIC FM 97.3
// http://icestream.coolwazobiainfo.com:8000/coolfm-lagos =>Cool FM 96.9 Lagos
// "http://67.228.150.175:8550/;" => Inspiration 92.3 FM
// http://67.228.150.175:8890/; RADIO CONTINENTAL
// http://icy3.abacast.com/rainbow-rainbowaac-48 =>RainBow 94.1 fm
// http://ss1.metrong.net:8000/smoothlivefm128 => SMOOTH 98.1
// http://67.228.150.175:8460/; => TOP RADIO
// http://icy2.abacast.com/megalectrics-thebeat-32 => THE BEAT