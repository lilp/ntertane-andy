package phantlab.ntertain.app.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.PlaylistAdapter;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.databases.MyMusicDatabase;
import phantlab.ntertain.app.models.Playlist;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class AddPlaylistDialog extends DialogFragment {
    @Bind(R.id.playlist)
    RecyclerView playlist;

    @Bind(R.id.no_playlist)
    TextView no_playlist;

    PlaylistAdapter adapter;
    Tracks track;

    public AddPlaylistDialog setTrack(Tracks track) {
        this.track = track;
        return this;
    }

    private MyMusicDatabase mMyMusic;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mMyMusic = new MyMusicDatabase(getActivity());
        View dialog = inflater.inflate(R.layout.add_to_playlist, container, false);
        ButterKnife.bind(this, dialog);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        adapter = new PlaylistAdapter(getActivity(), null);
        adapter.setDialog();
        playlist.setLayoutManager(new LinearLayoutManager(getActivity()));
        playlist.setAdapter(adapter);
        final List<Playlist> data = mMyMusic.getPlaylist();
        adapter.setCallback(new ClickCallbacks.ClickCallback() {
            @Override
            public void onClick(int position) {
                int id = data.get(position).getId();
                mMyMusic.addPlaylistItem(track, id);
                 Toast.makeText(getActivity(), String.format(Locale.ENGLISH, "%s added to playlist",
                        track.getTrack()), Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });

        if (data == null || data.isEmpty()) {
            playlist.setVisibility(View.GONE);
            no_playlist.setVisibility(View.VISIBLE);
        } else {
            if (data.size() > 1)
                playlist.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        Utils.convertDpToPixel(96)));
            adapter.setData(data);
        }

    }

    @OnClick(R.id.cancel)
    public void onCancel() {
        dismiss();
    }
}
