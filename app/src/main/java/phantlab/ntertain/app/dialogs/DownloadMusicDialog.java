package phantlab.ntertain.app.dialogs;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.ui.ThumbnailController;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class DownloadMusicDialog extends DialogFragment {
    @Bind({R.id.thumb, R.id.blur})
    List<SimpleDraweeView> thumbs;
    @Bind({android.R.id.title, android.R.id.summary})
    List<TextView> texts;
    Tracks track;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View dialog = inflater.inflate(R.layout.download_music_dialog, container, false);
        ButterKnife.bind(this, dialog);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
        texts.get(0).setText(track.getTrack());
        texts.get(1).setText(track.getAlbum());
        int image_width = Utils.convertDpToPixel(56);
        ThumbnailController.with(thumbs.get(0)).dimens(image_width, image_width).uri(Uri.parse(track.getThumbnail())).load();
        ThumbnailController.with(thumbs.get(1)).from(getActivity()).uri(Uri.parse(track.getThumbnail())).blur(25);
    }


    @OnClick(R.id.cancel)
    public void onClose(){
        dismiss();
    }

    public DownloadMusicDialog setTrack(Tracks track) {
        this.track = track;
        return this;
    }
}
