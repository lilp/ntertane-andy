package phantlab.ntertain.app.dialogs;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public interface DialogCallback {
    void onPositiveClick();
}
