package phantlab.ntertain.app.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;

import phantlab.ntertain.app.R;

/**
 * Created by kartikeyasingh on 18/12/15.
 */
public class NoMusicDialog extends DialogFragment {

    DialogInterface.OnClickListener listener;

    String message;

    public NoMusicDialog setMessage(String message) {
        this.message = message;
        return this;
    }

    public NoMusicDialog setListener(DialogInterface.OnClickListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public void dismiss() {/***/}

    public void hide() {
        super.dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.ntertain_dialog);
        builder.setTitle("Sorry!");
        builder.setMessage(message == null ? "Music not available, maybe try another one (dj khaled's voice)." : message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, listener);
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface di, int key, KeyEvent event) {
                return key == KeyEvent.KEYCODE_BACK;
            }
        });
        return builder.create();
    }
}
