package phantlab.ntertain.app.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.databases.MyMusicDatabase;
import phantlab.ntertain.app.models.Playlist;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class CreatePlaylistDialog extends DialogFragment {
    @Bind(R.id.playlist_name)
    EditText playlist;
    private MyMusicDatabase mMyMusic;
    private DialogCallback callback;

    public CreatePlaylistDialog setCallback(DialogCallback callback) {
        this.callback = callback;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mMyMusic = new MyMusicDatabase(getActivity());
        View dialog = inflater.inflate(R.layout.play_dialog, container, false);
        ButterKnife.bind(this, dialog);
        return dialog;
    }

    @OnClick(R.id.ok)
    public void onOk() {
        mMyMusic.createPlaylist(new Playlist(getPlaylistTitle(), 0, 0));
        callback.onPositiveClick();
    }

    @OnClick(R.id.cancel)
    public void onCancel() {
        dismiss();
    }

    public String getPlaylistTitle() {
        return playlist.getText().toString();
    }
}
