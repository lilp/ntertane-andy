package phantlab.ntertain.app.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import butterknife.ButterKnife;
import butterknife.OnClick;
import phantlab.ntertain.app.R;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class WelcomeDialog extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View dialog = inflater.inflate(R.layout.welcome_dialog, container, false);
        ButterKnife.bind(this, dialog);
        return dialog;
    }

    @OnClick(R.id.close)
    public void onClose(){
        dismiss();
    }

}
