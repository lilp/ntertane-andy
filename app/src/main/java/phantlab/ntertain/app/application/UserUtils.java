package phantlab.ntertain.app.application;

import android.content.Context;
import android.content.SharedPreferences;

import phantlab.ntertain.app.api.model.User;

/**
 * Created by kartikeyasingh on 31/12/15.
 */
public class UserUtils {
    public final static String NTERTANE_PREFS = "ntertane_prefs";
    private final static String KEY_TOKEN = "token";
    private final static String KEY_EMAIL = "email";
    private final static String KEY_NAME = "name";
    private final static String KEY_USERNAME = "username";
    private final static String KEY_SIGNED_IN = "signed_in";

    public static void saveUser(Ntertain app, User user) {
        SharedPreferences preferences = app.getSharedPreferences(NTERTANE_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_TOKEN, user.getToken());
        editor.putString(KEY_USERNAME, user.getLogin());
        editor.putString(KEY_NAME, user.getName());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putBoolean(KEY_SIGNED_IN, true);
        editor.apply();
    }

    public static User getUser(Ntertain app) {
        User user = new User();
        SharedPreferences preferences = app.getSharedPreferences(NTERTANE_PREFS, Context.MODE_PRIVATE);
        user.setName(preferences.getString(KEY_NAME, ""));
        user.setLogin(preferences.getString(KEY_USERNAME, ""));
        user.setEmail(preferences.getString(KEY_EMAIL, ""));
        user.setToken(preferences.getString(KEY_TOKEN, ""));
        return user;
    }

    public static boolean isSignedIn(Ntertain app) {
        SharedPreferences preferences = app.getSharedPreferences(NTERTANE_PREFS, Context.MODE_PRIVATE);
        return preferences.getBoolean(KEY_SIGNED_IN, false);
    }
}
