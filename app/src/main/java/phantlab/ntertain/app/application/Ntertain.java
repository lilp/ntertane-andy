package phantlab.ntertain.app.application;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;

import phantlab.ntertain.app.api.model.User;
import phantlab.ntertain.app.foundation.Cache;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/20/2015.
 */
public class Ntertain extends Application {
    private String token;
    private static User user;
    private boolean isSignedIn = false;


    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }

    private void initialize() {
        FacebookSdk.sdkInitialize(this);
        Fresco.initialize(this);
        Utils.setContext(this);
        user = UserUtils.getUser(this);
        isSignedIn = UserUtils.isSignedIn(this);
        new Cache(this).getCss();
    }

    public static User getUser() {
        return user;
    }

    public boolean isSignedIn() {
        return isSignedIn;
    }

    public String getToken() {
        return "8c4397f0-0994-4acf-83f9-07345f7a7373";
    }
}
