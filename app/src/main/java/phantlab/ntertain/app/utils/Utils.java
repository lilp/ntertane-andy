package phantlab.ntertain.app.utils;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import phantlab.ntertain.app.activities.AlbumDetails;
import phantlab.ntertain.app.activities.ArtistActivity;
import phantlab.ntertain.app.activities.MovieDetails;
import phantlab.ntertain.app.activities.NewsDetails;
import phantlab.ntertain.app.activities.PlayMusicActivity;
import phantlab.ntertain.app.activities.TopTracksActivity;
import phantlab.ntertain.app.api.Artists;
import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.api.model.Artist;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.news.NewsItem;


public class Utils {
    public static final int item_width = 200;
    private static Typeface primary = null, secondary = null, semibold;
    private static Context context;
    private static File smartNewsDir;
    private static PeriodFormatter formatter = null;

    public static void setContext(Context context) {
        Utils.context = context;
    }

    public static int getColor(Activity activity, int resId) {
        return activity.getResources().getColor(resId);
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static void ellipsis(TextView view) {
        if (view.getLineCount() > 1) {
            int lineEnd = view.getLayout().getLineEnd(0);
            view.setText(view.getText().subSequence(0, lineEnd - 3) + "...");
        }
    }

    public static Typeface getFont(Context sender, boolean _) {
        if (_) {
            if (primary == null)
                primary = Typeface.createFromAsset(sender.getAssets(), "fonts/ProximaNova-Light.otf");
        } else {
            if (secondary == null)
                secondary = Typeface.createFromAsset(sender.getAssets(), "fonts/ProximaNova-Regular.otf");
            return secondary;
        }
        return primary;
    }

    public static Typeface getSemiBoldProxima(Context sender) {
        if (semibold == null)
            semibold = Typeface.createFromAsset(sender.getAssets(), "fonts/ProximaNova-Semibold.otf");
        return semibold;
    }

    public static int convertDpToPixel(float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static void StartActivity(Activity sender, Class t) {
        Intent intent = new Intent(sender, t);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sender.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(sender).toBundle());
            return;
        } else sender.startActivity(intent);
    }

    public static void StartActivity(Activity sender, Class t, Bundle data) {
        Intent intent = new Intent(sender, t);
        intent.putExtra("#data", data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bundle _ = ActivityOptions.makeSceneTransitionAnimation(sender).toBundle();
            sender.startActivity(intent, _);
            return;
        } else sender.startActivity(intent);
    }


    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Activity context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static void setGridLayoutManager(Activity sender, RecyclerView list) {
        list.setLayoutManager(new GridLayoutManager(sender, getCount(sender, item_width)));
    }

    public static int getCount(Activity activity, int width) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float density = activity.getResources().getDisplayMetrics().density;
        float dpWidth = outMetrics.widthPixels / density;
        return Math.round(dpWidth / width);
    }


    public static boolean isExternalStorageRemovable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD)
            return Environment.isExternalStorageRemovable();
        return true;
    }

    public static File getExternalCacheDir(Context context) {
        if (hasExternalCacheDir()) {
            return context.getExternalCacheDir();
        }
        final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
        return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
    }

    public static boolean hasExternalCacheDir() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static String getTitle(String title) {
        String val = "";
        if (title.length() > 20)
            val = title.substring(0, 16).concat("...");
        else val = title;
        return val;
    }

    public static String getTitleBigger(String title) {
        String val = "";
        if (title.length() > 37)
            val = title.substring(0, 34).concat("...");
        else val = title;
        return val;
    }

    public static File getNewsDataDir() {
        try {
            return new File(context.getPackageManager().getPackageInfo(context.getPackageName(), 0)
                    .applicationInfo.dataDir, "News");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void changeProgressColor(int color, ProgressBar progress) {
        progress.getIndeterminateDrawable()
                .setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    public static void setUpTransitions(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            window.setEnterTransition(new Explode());
            window.setExitTransition(new Explode());
        }
    }

    public static Bundle getTrackData(Tracks model) {
        Bundle data = new Bundle();
        data.putString(PlayMusicActivity.KEY_ALBUM, model.getAlbum());
        data.putString(PlayMusicActivity.KEY_NAME, model.getTrack());
        data.putString(PlayMusicActivity.KEY_THUMBNAIL, model.getThumbnail());
        data.putString(PlayMusicActivity.KEY_ARTIST, model.getArtists()[0]);
        data.putString(PlayMusicActivity.KEY_URL, model.getUrl());
        return data;
    }

    public static Bundle getArtistData(Artist artist, boolean isFromTrack) {
        Bundle data = new Bundle();
        data.putString(ArtistActivity.KEY_ARTIST, artist.getName());
        data.putString(ArtistActivity.KEY_THUMBNAIL, artist.getThumbnail());
        data.putBoolean(ArtistActivity.KEY_IS_FROM_TRACK, isFromTrack);
        data.putInt(ArtistActivity.KEY_ALBUM_COUNT, artist.getAlbumCount());
        data.putInt(ArtistActivity.KEY_ARTIST_ID, artist.getId());
        data.putInt(ArtistActivity.KEY_SONG_COUNT, artist.getTrackCount());
        return data;
    }

    public static Bundle getAlbumData(Albums album, String artist, boolean isFromTrack) {
        Bundle data = new Bundle();
        data.putString(AlbumDetails.KEY_ALBUM, album.getName());
        data.putString(AlbumDetails.KEY_ARTIST, artist);
        data.putString(AlbumDetails.KEY_THUMBNAIL, album.getThumbnail());
        data.putBoolean(ArtistActivity.KEY_IS_FROM_TRACK, isFromTrack);
        data.putInt(AlbumDetails.KEY_ALBUM_ID, album.getId());
        data.putInt(AlbumDetails.KEY_TRACK_COUNT, album.getTrackCount());
        return data;
    }


    public static String format(long ms) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(ms),
                TimeUnit.MILLISECONDS.toSeconds(ms) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(ms)));
    }

    public static List<Tracks> getFilteredFeaturedMusic(List<Tracks> data) {
        String unique_artist = "";
        List<Tracks> filtered_data = new ArrayList<>();

        for (Tracks item : data) {
            String artist = item.getArtists()[0];
            if (!artist.equals(unique_artist))
                filtered_data.add(item);
            unique_artist = artist;
        }

        return filtered_data;
    }

    public static void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static String getApprox(String timestamp) {
        String approx = "";

        try {
            Period period = new Period(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(timestamp).getTime(),
                    Calendar.getInstance().getTime().getTime());
            if (formatter == null)
                formatter = new PeriodFormatterBuilder()
                        .appendSeconds().appendSuffix(" just now")
                        .appendMinutes().appendSuffix(" mins ago")
                        .appendHours().appendSuffix(" H ago")
                        .appendDays().appendSuffix(" D ago")
                        .appendWeeks().appendSuffix(" W ago")
                        .appendMonths().appendSuffix(" M ago")
                        .appendYears().appendSuffix(" Y ago")
                        .printZeroNever()
                        .toFormatter();
            if (period.getSeconds() < 60)
                approx = formatter.print(period.toStandardSeconds());
            else if (period.getMinutes() >= 1)
                approx = formatter.print(period.toStandardMinutes());
            if (period.getDays() > 0)
                approx = formatter.print(period.toStandardDays());
            else if (period.getDays() >= 7)
                approx = formatter.print(period.toStandardWeeks());
            else if (period.getMonths() > 0)
                approx = formatter.print(period.toStandardDays());
            else
                approx = formatter.print(period.toStandardHours());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return approx;
    }

    public static Bundle getFeaturedMusicData(boolean is_recent) {
        Bundle data = new Bundle();
        data.putBoolean(TopTracksActivity.KEY_RECENT, is_recent);
        return data;
    }

    public static Bundle getMovieBundle(MovieMeta model) {
        Bundle data = new Bundle();
        data.putString(MovieDetails.TITLE, model.getData().get(0));
        data.putString(MovieDetails.PLOT, model.getData().get(1));
        data.putString(MovieDetails.GENRE, model.getData().get(2));
        data.putString(MovieDetails.THUMB, model.getData().get(5));
        data.putString(MovieDetails.LINK, model.getData().get(6));
        data.putString(MovieDetails.PLAY, model.getData().get(4));
        data.putString(MovieDetails.VIDEO, model.getData().get(7));
        data.putString(MovieDetails.RATING, model.getData().get(3));
        return data;
    }

    public static Bundle getNewsBundle(NewsItem model) {
        Bundle data = new Bundle();
        data.putString(NewsDetails.TITLE, model.getTitle());
        data.putString(NewsDetails.LINK, model.getLink());
        data.putString(NewsDetails.TIME, model.getTimestamp());
        data.putString(NewsDetails.ENCODED, model.getThumbnail());
        data.putSerializable(NewsDetails.TYPE, model.getSource());
        return data;
    }

    public static String md5(String txt) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(txt.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++)
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));

        return sb.toString();
    }
}
