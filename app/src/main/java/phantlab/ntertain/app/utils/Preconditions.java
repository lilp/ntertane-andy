package phantlab.ntertain.app.utils;

import phantlab.ntertain.app.api.model.Tracks;

/**
 * Created by kartikeyasingh on 29/12/15.
 */
public class Preconditions {
    public static void checkTrack(Tracks track) {
        if (track.getUrl() == null)
            throw new IllegalArgumentException();
    }
}
