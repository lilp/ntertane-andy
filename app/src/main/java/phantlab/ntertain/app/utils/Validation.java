package phantlab.ntertain.app.utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kartikeyasingh on 01/01/16.
 */
public class Validation {
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    Context context;
    private List<ValidationHolder> validations = new ArrayList<>();

    private Validation() {/**/}

    public static Validation getInstance() {
        return ValidationInstanceHolder.instance;
    }

    public void release() {
        validations.clear();
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void add(EditText field, String name, Type type) {
        validations.add(new ValidationHolder(field, name, type));
    }

    public boolean validate() {
        boolean result = true;
        for (ValidationHolder holder : validations)
            result = result && holder.validate();
        return result;
    }

    public enum Type {
        EMAIL,
        PASSWORD,
        EMPTY
    }

    private static class ValidationInstanceHolder {
        public static Validation instance = new Validation();
    }

    class ValidationHolder implements TextWatcher {
        EditText field;
        Type type;
        String field_name;
        Pattern pattern;

        public ValidationHolder(EditText field, String field_name, Type type) {
            this.field = field;
            this.type = type;
            this.field_name = field_name;
            setUpPattern();
            field.addTextChangedListener(this);
        }

        private void setUpPattern() {
            if (type == Type.EMAIL)
                pattern = Pattern.compile(EMAIL_PATTERN);
        }

        public boolean validate() {
            if (getString().trim().length() == 0) {
                field.setError(field_name.concat(" can't be empty"));
                return false;
            }
            if (type.equals(Type.EMAIL)) {
                Matcher matcher = pattern.matcher(getString());
                if (!matcher.matches()) {
                    field.setError(field_name.concat(" is invalid"));
                    return false;
                }
            }
            return true;
        }

        public String getString() {
            return field.getText().toString();
        }

        public Type getType() {
            return type;
        }

        @Override
        public void beforeTextChanged(CharSequence charseq, int i, int i1, int i2) {/***/}

        @Override
        public void onTextChanged(CharSequence charseq, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}