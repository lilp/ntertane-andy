package phantlab.ntertain.app.ui;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.MoviesCallback;
import phantlab.ntertain.app.movies.MoviesEngine;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/28/2015.
 */
public class CastView extends LinearLayout {
    List<String> actors;

    @Bind(R.id.thumbnail)
    SimpleDraweeView thumbnail;

    @Bind(R.id.name)
    TextView mActorTextView;

    public void setActors(List<String> actors) {
        this.actors = actors;
        init();
    }

    private void init() {
        final List<SimpleDraweeView> actors_thumb = new ArrayList<>();

        for (String actor : actors) {
            View layout = LayoutInflater.from(getContext()).inflate(R.layout.cast_item, null);
            ButterKnife.bind(this, layout);
            mActorTextView.setText(actor);
            actors_thumb.add(thumbnail);
            addView(layout);
        }

        for (int i = 0; i < actors_thumb.size(); i++) {
            final SimpleDraweeView thumb = actors_thumb.get(i);
            new MoviesEngine().getThumb(actors.get(i).replace(" ", "_"), new MoviesCallback.ThumbResult() {
                @Override
                public void onSuccess(String result) {
                    if (result != null) {
                        int width = Utils.convertDpToPixel(48);
                        new ThumbnailController()
                                .with(thumb).uri(Uri.parse(result.replace("//", "https://")))
                                .dimens(width, width)
                                .load();
                    }
                }
            });
        }

    }

    public CastView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
    }
}
