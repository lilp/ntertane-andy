package phantlab.ntertain.app.ui;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.adapters.HomePagerAdapter;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 12/12/15.
 */
public class FeaturedMusicView extends LinearLayout {
    @Bind(R.id.music_image)
    SimpleDraweeView image;

    public FeaturedMusicView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
        setData();
    }

    private void setData() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        List<HomePagerAdapter.Items> items = new ArrayList<>();
        items.add(new HomePagerAdapter.Items("No Ceilings", "Lilwayne", "http://imi.ulximg.com/image/300x300/cover/1447866891_338161a5a514a9233eec89eaf02ad364.png/bb2d344ae147aae6ad2bf00954a91841/1447866891_a3b3f59746ad822a895a1c43938d1d61.png"));
        items.add(new HomePagerAdapter.Items("Not Afraid", "Eminem", "http://img12.deviantart.net/40ff/i/2010/196/5/f/not_afraid_by_kcbonx.jpg"));
        items.add(new HomePagerAdapter.Items("All eyes on you", "Meek Mill", "https://upload.wikimedia.org/wikipedia/en/thumb/2/2e/MeekMillDWMTM.jpg/220px-MeekMillDWMTM.jpg"));
        int image_width = Utils.convertDpToPixel(100);

        for (HomePagerAdapter.Items item : items) {
            View layout = inflater.inflate(R.layout.music_featured_items, null);
            LinearLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.setLayoutParams(params);
            ButterKnife.bind(this, layout);
            ThumbnailController.with(image).dimens(image_width, image_width).
                    uri(Uri.parse(item.getSongThumb())).load();
            addView(layout);
        }
    }
}
