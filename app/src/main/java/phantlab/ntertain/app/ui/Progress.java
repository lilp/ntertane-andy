package phantlab.ntertain.app.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import phantlab.ntertain.app.R;


public class Progress extends View {

    private CircularProgressDrawable mDrawable;


    public Progress(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Progress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray data = context.obtainStyledAttributes(attrs, R.styleable.Progress);
        mDrawable = new CircularProgressDrawable(Color.parseColor("#FFA300"), data.getInt(1, 13), data.getBoolean(0, false));
        mDrawable.setCallback(this);
        data.recycle();
    }

    public void start() {
        mDrawable.start();
    }

    public void stop() {
        mDrawable.stop();
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (mDrawable != null) {
            if (visibility == VISIBLE) {
                mDrawable.start();
            } else {
                if (mDrawable.isRunning()) {
                    mDrawable.stop();
                }
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mDrawable.setBounds(0, 0, w, h);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        mDrawable.draw(canvas);
    }

    @Override
    protected boolean verifyDrawable(Drawable who) {
        return who == mDrawable || super.verifyDrawable(who);
    }
}