package phantlab.ntertain.app.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

/**
 * Created by kartikeyasingh on 12/12/15.
 */
public class NoScrollbarsScrollView extends HorizontalScrollView{
    public NoScrollbarsScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setHorizontalScrollBarEnabled(false);
    }
}
