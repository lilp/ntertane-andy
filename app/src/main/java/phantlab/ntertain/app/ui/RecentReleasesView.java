package phantlab.ntertain.app.ui;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.music.MusicService;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 08/12/15.
 */
public class RecentReleasesView extends LinearLayout implements View.OnClickListener {
    List<Tracks> data;

    @Bind({android.R.id.title, android.R.id.summary, R.id.playback})
    List<TextView> texts;

    @Bind(R.id.thumbnail)
    SimpleDraweeView thumbnail;

    Progress progress;
    MusicService mStreamingService;

    public RecentReleasesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
    }

    public void setProgress(Progress progress, MusicService mStreamingService) {
        this.progress = progress;
        this.mStreamingService = mStreamingService;
    }

    public void setData(List<Tracks> data) {
        this.data = data;
        progress.setVisibility(GONE);
        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        if (data != null)
            for (int i = 0; i < Math.min(data.size(), 10); i++) {
                Tracks item = data.get(i);
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View layout = inflater.inflate(R.layout.song_list_item, null);
                ButterKnife.bind(this, layout);

                layout.setOnClickListener(this);
                texts.get(0).setText(item.getTrack());
                texts.get(1).setText(item.getArtists()[0]);
                texts.get(2).setText(Utils.format(item.getDuration()));

                int width = Utils.convertDpToPixel(48);
                ThumbnailController.with(thumbnail).uri(Uri.parse(item.getThumbnail()))
                        .dimens(width, width).load();
                layout.setTag(i);
                addView(layout);
            }
    }

    @Override
    public void onClick(View view) {
        mStreamingService.playFromList(data, (Integer) view.getTag());
    }
}
