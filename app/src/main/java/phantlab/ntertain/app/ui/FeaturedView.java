package phantlab.ntertain.app.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 12/12/15.
 */
public class FeaturedView extends LinearLayout {
    public FeaturedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        int width = Utils.convertDpToPixel(100),
                margin = Utils.convertDpToPixel(2);

        for (int i = 0; i < 3; i++) {
            View layout = inflater.inflate(R.layout.stack_items, null);
            ButterKnife.bind(this, layout);
            LinearLayout.LayoutParams params = new LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.setLayoutParams(params);
            layout.setPadding(margin, margin, margin, margin);
            addView(layout);
        }
    }
}
