package phantlab.ntertain.app.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 27/12/15.
 */
public class FontTextView extends TextView {
    private boolean is_created = false;
    boolean isRegular = false, isSemiBold = false;

    public FontTextView(Context context) {
        super(context);
        is_created = true;
        isSemiBold = true;
        init(context, null);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FontTextView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        if (!is_created) {
            TypedArray data = context.obtainStyledAttributes(attrs, R.styleable.fontTextView);
            String font = data.getString(0);
            isRegular = font.equals("Regular");
            isSemiBold = font.equals("SemiBold");
            data.recycle();
        }

        if (isSemiBold)
            setTypeface(Utils.getSemiBoldProxima(getContext()));
        else setTypeface(Utils.getFont(getContext(), isRegular));
    }
}
