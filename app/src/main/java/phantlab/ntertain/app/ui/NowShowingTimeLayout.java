package phantlab.ntertain.app.ui;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.movies.Shows;
import phantlab.ntertain.app.ui.models.MoviesTime;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/23/2015.
 */
public class NowShowingTimeLayout extends LinearLayout {
    List<String> mTimeList;


    public void setData(List<String> mTimeList) {
        this.mTimeList = mTimeList;
        init();
    }

    public NowShowingTimeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
    }

    private void init() {
        for (String time : mTimeList) {
            FontTextView mTimeView = new FontTextView(getContext());
            mTimeView.setText(time);
            mTimeView.setTextColor(Color.WHITE);
            mTimeView.setTextSize(17);
            LinearLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            int margin = Utils.convertDpToPixel(4f), padding = Utils.convertDpToPixel(5f);
            params.setMargins(margin, margin, margin, margin);
            mTimeView.setLayoutParams(params);
            mTimeView.setPadding(padding, margin, padding, margin);
            mTimeView.setBackgroundResource(R.drawable.time_bg);
            addView(mTimeView);
        }
    }
}
