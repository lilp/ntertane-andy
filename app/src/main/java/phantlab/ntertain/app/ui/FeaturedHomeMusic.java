package phantlab.ntertain.app.ui;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.BaseActivity;
import phantlab.ntertain.app.api.TopTracks;
import phantlab.ntertain.app.api.model.TrackCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.music.MusicService;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 12/12/15.
 */
public class FeaturedHomeMusic extends LinearLayout {

    @Bind(android.R.id.title)
    TextView title;

    @Bind(R.id.thumbnail)
    SimpleDraweeView thumbnail;
    int i;
    private Progress loading;
    private Activity sender;


    public FeaturedHomeMusic(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
    }

    public void setProgress(Progress loading, Activity sender) {
        this.loading = loading;
        this.sender = sender;
        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final int width = Utils.convertDpToPixel(110),
                thumb_height = Utils.convertDpToPixel(124),
                margin = Utils.convertDpToPixel(4);

        final MusicService service = ((BaseActivity) sender).getStreamingService();

        new TopTracks().getTracks(20, 1, new Callback<TrackCallback>() {
            @Override
            public void success(TrackCallback track, Response response) {
                loading.stop();
                loading.setVisibility(GONE);

                final List<Tracks> filtered = Utils.getFilteredFeaturedMusic(track.getData());
                final List<Tracks> result = new ArrayList<>();
                for (Tracks item : filtered)
                    if (item.getThumbnail() != null && item.getUrl() != null)
                        result.add(item);
                for (i = 0; i < Math.min(result.size(), 3); i++) {
                    final Tracks song = result.get(i);
                    View layout = inflater.inflate(R.layout.featured_home_music, null);
                    layout.setTag(i);
                    ButterKnife.bind(FeaturedHomeMusic.this, layout);
                    LayoutParams params = new LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layout.setLayoutParams(params);
                    layout.setPadding(margin, margin, margin, margin);
                    title.setText(song.getTrack());
                    ThumbnailController.with(thumbnail)
                            .dimens(width, thumb_height)
                            .uri(Uri.parse(song.getThumbnail())).load();
                    layout.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                Utils.StartActivity(sender, PlayMusicActivity.class, Utils.getTrackData(song));
                            service.playFromList(result, (Integer) view.getTag());
                        }
                    });
                    addView(layout);

                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
