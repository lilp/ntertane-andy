package phantlab.ntertain.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer.util.Util;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.AlbumDetails;
import phantlab.ntertain.app.activities.ArtistActivity;
import phantlab.ntertain.app.activities.PlayerActivity;
import phantlab.ntertain.app.api.Album;
import phantlab.ntertain.app.api.Artists;
import phantlab.ntertain.app.api.Playlist;
import phantlab.ntertain.app.api.model.Albums;
import phantlab.ntertain.app.api.model.Artist;
import phantlab.ntertain.app.api.model.TopPlaylistCallback;
import phantlab.ntertain.app.api.model.Tracks;
import phantlab.ntertain.app.bus.BusProvider;
import phantlab.ntertain.app.bus.PlayBackStateChangedEvent;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.dialogs.AddPlaylistDialog;
import phantlab.ntertain.app.dialogs.DownloadMusicDialog;
import phantlab.ntertain.app.music.MusicService;
import phantlab.ntertain.app.ui.models.Menu;
import phantlab.ntertain.app.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class NtertainMenu extends LinearLayout {
    ClickCallbacks.ClickCallback callback;
    List<Menu> data;
    View overlay;
    private Tracks track;
    private AppCompatActivity sender;
    private Animation slide_down, slide;
    private ImageButton toggle;
    Playlist playlist = new Playlist();
    MusicService service;

    public void setService(MusicService service) {
        this.service = service;
    }

    public void setSender(AppCompatActivity sender) {
        this.sender = sender;
    }

    public void setTrack(Tracks track) {
        this.track = track;
    }

    public void setOverlay(View overlay) {
        this.overlay = overlay;
    }

    public NtertainMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        isInEditMode();
        setOrientation(VERTICAL);
        setup();
    }

    public NtertainMenu setCallback(ClickCallbacks.ClickCallback callback) {
        this.callback = callback;
        return this;
    }

    public NtertainMenu setData(List<Menu> data) {
        this.data = data;
        return this;
    }

    public void setup() {
        toggle = new ImageButton(getContext());
        toggle.setBackgroundDrawable(null);
        toggle.setImageResource(R.drawable.ic_option);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        toggle.setLayoutParams(params);
        addView(toggle);

        final LinearLayout menu_container = new LinearLayout(getContext());
        menu_container.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        menu_container.setOrientation(VERTICAL);
        addView(menu_container);

        slide = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
        slide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                overlay.setVisibility(VISIBLE);
                menu_container.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {/**/}

            @Override
            public void onAnimationRepeat(Animation animation) {/****/}
        });
        slide_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {/***/}

            @Override
            public void onAnimationEnd(Animation animation) {
                overlay.setVisibility(GONE);
                menu_container.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {/****/}
        });

        toggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                TranslateAnimation animation = new TranslateAnimation(getContext());

                int[] location = new int[2];
                menu_container.getLocationOnScreen(location);
                Log.i("#location", "location = " + location[0] + "," + location[1]);
                Log.i("#location_2", "height = " + menu_container.getHeight());
                Log.i("#location_2", "bottom = " + menu_container.getBottom());

                if (menu_container.getVisibility() == GONE) {
                    startAnimation(slide);
                    toggle.setImageResource(R.drawable.ic_option_close);
                } else {
                    close();
                }
            }
        });


        // test
        data = new ArrayList<>();

        data.add(new Menu("Go to Artist", R.drawable.ic_artists));
        data.add(new Menu("Go to Album", R.drawable.ic_album));
        data.add(new Menu("Watch Video", R.drawable.ic_video));
//        data.add(new Menu("Get Callertunez", R.drawable.ic_callertune));
        data.add(new Menu("Start Song Radio", R.drawable.ic_radio));
        data.add(new Menu("Add to playlist", R.drawable.ic_offline_playlist));
        data.add(new Menu("Download Music", R.drawable.ic_downloads));

        MenuUtils.setMenu(getContext(), menu_container, data, new ClickCallbacks.ClickCallback() {
            @Override
            public void onClick(int position) {
                switch (position) {
                    case 0:
                        goToArtist();
                        break;
                    case 1:
                        goToAlbum();
                        break;
                    case 2:
                        watchVideo();
                        break;
                    case 3:
                        startSongRadio();
                        break;
                    case 4:
                        addToPlaylist();
                        break;
                    case 5:
                        downloadMusic();
                        break;
                }

                close();
            }
        });

        menu_container.setVisibility(GONE);
    }

    private void close() {
        toggle.setImageResource(R.drawable.ic_option);
        startAnimation(slide_down);
    }

    private void startSongRadio() {
        final ProgressDialog dialog = new ProgressDialog(sender);
        dialog.setMessage("Starting song radio..");
        dialog.show();
        playlist.startRadio(track.getArtists()[0], new Callback<TopPlaylistCallback>() {
            @Override
            public void success(TopPlaylistCallback cb, Response response) {
                service.playFromList(cb.getPlaylist(), 0);
                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(sender, "Something went wrong.\nPlease try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void downloadMusic() {
        new DownloadMusicDialog()
                .setTrack(track)
                .show(sender.getSupportFragmentManager(), "$download");
    }

    private void addToPlaylist() {
        new AddPlaylistDialog().setTrack(track).show(sender.getSupportFragmentManager(), "$add");
    }

    private void watchVideo() {
        service.pause();
        String yt_id = track.getYoutubeId();
        Intent intent = new Intent(sender, PlayerActivity.class);
        intent.putExtra(PlayerActivity.VIDEO_ID, yt_id);
        sender.startActivity(intent);
    }

    private void goToAlbum() {
        Albums album = new Albums(-1, -1, -1, track.getAlbum(), track.getThumbnail(), track.getArtists()[0]);
        Utils.StartActivity(sender, AlbumDetails.class, Utils.getAlbumData(album, track.getArtists()[0], true));
    }

    private void goToArtist() {
        Artist artist = new Artist();
        artist.setName(track.getArtists()[0]);
        Utils.StartActivity(sender, ArtistActivity.class, Utils.getArtistData(artist, true));
    }
}
