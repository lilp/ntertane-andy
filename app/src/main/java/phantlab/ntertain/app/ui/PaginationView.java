package phantlab.ntertain.app.ui;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/20/2015.
 */
public class PaginationView extends LinearLayout {
    public PaginationView(Context context) {
        super(context);
    }

    private ViewPager pager;

    public void setViewPager(final ViewPager pager) {
        this.pager = pager;

        init();

        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i("#item", pager.getCurrentItem() + "/" + pager.getChildCount());
                int pos = (pager.getCurrentItem() == pager.getChildCount()) ? 0 : (pager.getCurrentItem() + 1);
                pager.setCurrentItem(pos, true);
                handler.postDelayed(this, 6000);
            }
        }, 6000);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {/***/}

            @Override
            public void onPageSelected(int position) {
                sync(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {/***/}
        });
    }

    private OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (Integer) v.getTag();
            pager.setCurrentItem(position, true);
            sync(position);
        }
    };

    public PaginationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
    }

    private void init() {
        Context context = getContext();
        Log.i("$min", "min = " + Math.min(4, pager.getChildCount()));
        for (int i = 0; i < 3; i++) {
            Button indicator = new Button(context);
            int width = (int) Utils.convertDpToPixel(8f, context),
                    margin = (int) Utils.convertDpToPixel(4f, context);
            indicator.setBackgroundResource(R.drawable.oval);
            indicator.setDuplicateParentStateEnabled(true);
            indicator.setOnClickListener(listener);
            LayoutParams params = new LayoutParams(width, width);
            params.setMargins(margin, margin, margin, margin);
            indicator.setLayoutParams(params);
            indicator.setTag(i);
            addView(indicator);
        }
    }

    public void sync(int position) {
        Log.i("$position", "position = " + position);
        getChildAt(position).setBackgroundResource(R.drawable.oval_selected);

        for (int i = 0; i < getChildCount(); i++)
            if (i != position)
                getChildAt(i).setBackgroundResource(R.drawable.oval);
    }

    public PaginationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
