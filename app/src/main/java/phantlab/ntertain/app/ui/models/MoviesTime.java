package phantlab.ntertain.app.ui.models;

/**
 * Created by Ddev on 11/23/2015.
 */
public class MoviesTime {
    String time;

    public MoviesTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }
}
