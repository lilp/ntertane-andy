package phantlab.ntertain.app.ui;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.List;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.NewsDetails;
import phantlab.ntertain.app.news.NewsFeed;
import phantlab.ntertain.app.news.NewsItem;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 12/12/15.
 */
public class FeaturedNewsView extends LinearLayout {
    private Progress progress = null;

    public FeaturedNewsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
    }

    public void setData(final Progress progress, final Activity sender) {
        this.progress = progress;
        progress.start();
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final int height = Utils.convertDpToPixel(76),
                padding = Utils.convertDpToPixel(10);

        new NewsFeed().getFeaturedNews(new NewsFeed.FeaturedResult() {
            @Override
            public void onSuccess(List<NewsItem> result) {
                Log.i("$featured-news", String.valueOf(result.size()));
                for (final NewsItem item : result) {
                    final View layout = inflater.inflate(R.layout.featured_news, null);
                    layout.setPadding(padding, padding, padding, padding);
                    LinearLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            height);
                    layout.setLayoutParams(params);
                    ((TextView) layout.findViewById(android.R.id.title)).setText(item.getTitle());
                    ((TextView) layout.findViewById(android.R.id.summary)).setText(item.getSubtitle());

                    layout.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Utils.StartActivity(sender, NewsDetails.class, Utils.getNewsBundle(item));
                        }
                    });

                    addView(layout);
                }
                progress.stop();
                progress.setVisibility(GONE);
            }
        });
    }
}
