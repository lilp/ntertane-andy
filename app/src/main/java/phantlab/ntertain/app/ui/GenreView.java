package phantlab.ntertain.app.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 09/12/15.
 */
public class GenreView extends LinearLayout implements View.OnClickListener {

    List<LinearLayout> rows;

    @Bind({R.id.genre_icon, R.id.thumbnail})
    List<ImageView> thumbs;

    @Bind(android.R.id.title)
    TextView title;


    private String[] titles = new String[]{"Party", "Focus", "Study", "EDM/Dance",
            "Dinner", "Sleep", "Hip Hop", "Travel", "decades", "Reggae"};

    private int[] icon = new int[]{R.drawable.icon_party, R.drawable.icon_focus, R.drawable.icon_study,
            R.drawable.icon_edm, R.drawable.icon_dinner, R.drawable.icon_sleep, R.drawable.icon_hiphop,
            R.drawable.icon_travel, R.drawable.ic_decades, R.drawable.icon_reggae};

    private int[] bg = new int[]{R.drawable.party_background, R.drawable.focus_background,
            R.drawable.study_background, R.drawable.edm_background, R.drawable.dinner_background,
            R.drawable.sleep_background, R.drawable.hiphop_background, R.drawable.travel_background,
            R.drawable.decades_background, R.drawable.reggae_background};

    public GenreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
        rows = new ArrayList<>();
        for (int i = 0; i < 2; i++)
            rows.add(new LinearLayout(context));
        setData();
    }

    private void setData() {

        int counter = -1, margin = Utils.convertDpToPixel(2);

        for (LinearLayout row : rows) {
            row.setOrientation(VERTICAL);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            params.setMargins(margin, margin, margin, margin);
            row.setLayoutParams(params);

            addView(row);

            for (int i = 0; i < 5; i++) {
                counter++;
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View item = inflater.inflate(R.layout.music_genre_mood_items, null);
                int width = Utils.convertDpToPixel(168);
                FrameLayout.LayoutParams mLayoutParams = new FrameLayout.LayoutParams(width, width);
                item.setLayoutParams(mLayoutParams);
                item.setPadding(margin, margin, margin, margin);
                ButterKnife.bind(this, item);
                thumbs.get(0).setImageResource(icon[counter]);
                thumbs.get(1).setImageResource(bg[counter]);
                title.setText(titles[counter]);
                item.setOnClickListener(this);
                row.addView(item);
            }
        }
    }

    @Override
    public void onClick(View view) {

    }
}
