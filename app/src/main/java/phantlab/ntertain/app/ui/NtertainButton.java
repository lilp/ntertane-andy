package phantlab.ntertain.app.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 27/12/15.
 */
public class NtertainButton extends Button {

    public NtertainButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public NtertainButton(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray data = context.obtainStyledAttributes(attrs, R.styleable.fontTextView);
        String font = data.getString(0);
        boolean isRegular = font.equals("Regular"),
                isSemiBold = font.equals("SemiBold");
        data.recycle();
        if (isSemiBold)
            setTypeface(Utils.getSemiBoldProxima(getContext()));
        else setTypeface(Utils.getFont(getContext(), isRegular));
    }
}
