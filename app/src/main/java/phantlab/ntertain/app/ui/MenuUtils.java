package phantlab.ntertain.app.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import phantlab.ntertain.app.R;
import phantlab.ntertain.app.callbacks.ClickCallbacks;
import phantlab.ntertain.app.ui.models.Menu;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 21/12/15.
 */
public class MenuUtils {
    public static void setMenu(final Context context, LinearLayout menu_container, List<Menu> data, final ClickCallbacks.ClickCallback cb) {
        for (int i = 0; i < data.size(); i++) {
            Menu item = data.get(i);
            LayoutInflater inflater = LayoutInflater.from(context);
            View child = inflater.inflate(R.layout.menu_item, null);

            ((TextView) child.findViewById(android.R.id.title)).setText(item.getTitle());
            ImageView icon = (ImageView) child.findViewById(R.id.thumbnail);
            icon.setImageResource(item.getIcon());

            if (item.getIcon() == R.drawable.ic_radio)
                icon.setColorFilter(context.getResources().getColor(R.color.buttons_yellow));

            int height = Utils.convertDpToPixel(56);
            RelativeLayout.LayoutParams mLParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
            child.setLayoutParams(mLParams);
            child.setTag(i);
            menu_container.addView(child);

            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cb.onClick((int) v.getTag());
                }
            });
        }

    }
}
