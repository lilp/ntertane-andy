package phantlab.ntertain.app.ui;

import android.content.pm.ActivityInfo;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.music.MusicService;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/7/2015.
 */
public abstract class Screen extends AppCompatActivity {
    public static MusicService mStreamingService;
    @Nullable
    @Bind(R.id.toolbar)
    public Toolbar toolbar;
    public TextView title;
    private boolean isBoundActivity = false;

   /* @Override
    protected void onPause() {
        super.onPause();

        if (mStreamingService != null)
            if (mStreamingService.isPlaying())
                mStreamingService.showNotification();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mStreamingService != null)
            if (mStreamingService.isNotificationShowing())
                mStreamingService.clearNotification();

    }*/

    public void setIsBoundActivity(boolean isBoundActivity) {
        this.isBoundActivity = isBoundActivity;
    }

    public void onCreate() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void setHorizontalOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public abstract void setup();

    public void disableIcon() {
        assert toolbar != null;
        toolbar.findViewById(R.id.ntertain_logo).setVisibility(View.GONE);
    }

    @Override
    public void setContentView(int mLayoutResId) {
        super.setContentView(mLayoutResId);
        ButterKnife.bind(this);
        if (toolbar != null) {
            toolbar.setTitle("");
            title = (TextView) toolbar.findViewById(R.id.title_ntertain);
            if (title != null)
                title.setTypeface(Utils.getFont(this, false));
        }
        setup();
    }

    public void setBackEnabled() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
