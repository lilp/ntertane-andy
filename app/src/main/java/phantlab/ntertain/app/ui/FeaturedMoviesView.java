package phantlab.ntertain.app.ui;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import phantlab.ntertain.app.R;
import phantlab.ntertain.app.activities.MovieDetails;
import phantlab.ntertain.app.callbacks.MoviesCallback;
import phantlab.ntertain.app.movies.MovieMeta;
import phantlab.ntertain.app.movies.MoviesEngine;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by kartikeyasingh on 12/12/15.
 */
public class FeaturedMoviesView extends LinearLayout {
    Progress progress;

    @Bind({android.R.id.title, android.R.id.summary})
    List<TextView> texts;

    @Bind(R.id.thumbnail)
    SimpleDraweeView thumbnail;

    Activity sender;

    public FeaturedMoviesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);
    }

    public void setData(Progress progress, Activity sender) {
        this.progress = progress;
        this.sender = sender;
        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        progress.start();

        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final int width = Utils.convertDpToPixel(110),
                height = Utils.convertDpToPixel(200),
                margin = Utils.convertDpToPixel(5);
        new MoviesEngine().getNowShowing(new MoviesCallback.StringResult() {
            @Override
            public void onSuccess(List<MovieMeta> result) {
                if (result != null) {
                    for (int i = 0; i < Math.min(result.size(), 3); i++) {
                        View layout = inflater.inflate(R.layout.item, null);
                        ButterKnife.bind(FeaturedMoviesView.this, layout);

                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
                        layout.setLayoutParams(params);

                        final MovieMeta meta = result.get(i);
                        List<String> data = meta.getData();

                        Log.i("#meta", "data.size() = " + data.size() + " texts.size() = " + texts.size());

                        texts.get(0).setText(data.get(0));
                        texts.get(1).setText(data.get(2));
                        ThumbnailController.with(thumbnail).uri(Uri.parse(data.get(5)))
                                .dimens(105, 200).load();

                        layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.StartActivity(sender, MovieDetails.class, Utils.getMovieBundle(meta));
                            }
                        });

                        addView(layout);
                    }
                }

                progress.stop();
                progress.setVisibility(GONE);
            }
        });
    }

}
