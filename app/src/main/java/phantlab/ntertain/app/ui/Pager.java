package phantlab.ntertain.app.ui;

import android.os.Handler;
import android.support.v4.view.ViewPager;


/**
 * Created by kartikeyasingh on 03/12/15.
 */
public class Pager {

    public static void attach(final ViewPager pager, final int delay) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (pager != null) {
                        int pos = (pager.getCurrentItem() - 1 == pager.getChildCount()) ?
                                0 : (pager.getCurrentItem() + 1);
                        pager.setCurrentItem(pos, true);
                    }
                } catch (Exception e) {/**/}

                handler.postDelayed(this, delay != -1 ? delay : 2000);
            }
        }, delay != -1 ? delay : 2000);

    }
}
