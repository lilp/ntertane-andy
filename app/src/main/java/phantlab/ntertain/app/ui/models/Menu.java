package phantlab.ntertain.app.ui.models;

/**
 * Created by kartikeyasingh on 07/12/15.
 */
public class Menu {
    String title;
    int icon;

    public Menu(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public int getIcon() {
        return icon;
    }
}
