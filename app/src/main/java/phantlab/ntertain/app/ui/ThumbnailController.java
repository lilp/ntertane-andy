package phantlab.ntertain.app.ui;

import android.content.Context;
import android.net.Uri;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.datasource.DataSubscriber;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import phantlab.ntertain.app.ui.processors.BlurPostprocessor;
import phantlab.ntertain.app.utils.Utils;

/**
 * Created by Ddev on 11/28/2015.
 */
public class ThumbnailController {
    private static SimpleDraweeView thumbnail;
    private static ThumbnailController instance;
    private Uri uri;
    private int width, height;
    private Context context;
    private DataSubscriber subscriber;

    public static ThumbnailController with(SimpleDraweeView thumbnail) {
        ThumbnailController.thumbnail = thumbnail;
        instance = new ThumbnailController();
        return instance;
    }

    public ThumbnailController from(Context context) {
        this.context = context;
        return this;
    }

    public ThumbnailController uri(Uri uri) {
        this.uri = uri;
        return this;
    }

    public ThumbnailController dimens(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public ThumbnailController dataSource(DataSubscriber subscriber) {
        this.subscriber = subscriber;
        return this;
    }

    public void getBitmap() {
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        ImageRequestBuilder builder = ImageRequestBuilder.newBuilderWithSource(uri);
        if (width > 0 && height > 0)
            builder.setResizeOptions(new ResizeOptions(width, height));
        ImageRequest request = builder.build();
        DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(request, context);
        dataSource.subscribe(subscriber, CallerThreadExecutor.getInstance());
    }

    public void blur(int threshold) {
        ImageRequestBuilder request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setPostprocessor(new BlurPostprocessor(context, threshold));

        if (width > 0 && height > 0)
            request.setResizeOptions(new ResizeOptions(width, height));

        PipelineDraweeController controller = (PipelineDraweeController)
                Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request.build())
                        .setOldController(thumbnail.getController())
                        .build();
        thumbnail.setController(controller);
    }

    public void load() {
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions(Utils.convertDpToPixel(width), Utils.convertDpToPixel(height)))
                .build();
        AbstractDraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(thumbnail.getController())
                .setImageRequest(request)
                .build();
        thumbnail.setController(controller);
    }
}
