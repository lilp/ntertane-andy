package phantlab.ntertain.app.models;

/**
 * Created by kartikeyasingh on 03/12/15.
 */
public class FeaturedItems {
    String thumbnail, title , genre;

    public FeaturedItems(String thumbnail, String title, String genre){
        this.thumbnail = thumbnail;
        this.title = title;
        this.genre = genre;
    }

    public String getGenre() {
        return genre;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getTitle() {
        return title;
    }
}
