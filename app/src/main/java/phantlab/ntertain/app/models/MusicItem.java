package phantlab.ntertain.app.models;

/**
 * Created by kartikeyasingh on 08/12/15.
 */
public class MusicItem {
    String track, album, artist, thumbnail;
    long track_id;

    public MusicItem(long track_id, String track, String album, String artist, String thumbnail) {
        this.track = track;
        this.album = album;
        this.artist = artist;
        this.thumbnail = thumbnail;
        this.track_id = track_id;
    }

    public String getArtist() {
        return artist;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getAlbum() {
        return album;
    }

    public String getTrack() {
        return track;
    }
}
