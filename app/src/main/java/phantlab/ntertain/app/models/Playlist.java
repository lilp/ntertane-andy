package phantlab.ntertain.app.models;

/**
 * Created by kartikeyasingh on 19/12/15.
 */
public class Playlist {
    String name;
    int id, track_count;

    public Playlist(String name, int id, int track_count) {
        this.name = name;
        this.id = id;
        this.track_count = track_count;
    }

    public String getName() {
        return name;
    }

    public int getTrackCount() {
        return track_count;
    }

    public int getId() {
        return id;
    }
}
