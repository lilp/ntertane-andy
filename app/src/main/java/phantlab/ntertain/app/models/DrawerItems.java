package phantlab.ntertain.app.models;

/**
 * Created by Ddev on 11/22/2015.
 */
public class DrawerItems {
    private String title;
    private int icon;

    public DrawerItems(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }
}
