package phantlab.ntertain.app.models;

/**
 * Created by Ddev on 11/29/2015.
 */
public class Comments {
    String name, comment, thumbnail;

    public Comments(String name, String comment, String thumbnail){
        this.name = name;
        this.comment = comment;
        this.thumbnail = thumbnail;
    }

    public String getComment() {
        return comment;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

}
